<?php
class Admin_model extends CI_Model
{
	/////////////////////////////////////////////////// admin
	public function login($email, $password)
	{
		$this->db->where('email', $email);
		$result = $this->db->get('admin');
		if ($result->num_rows() == 1) {
			$hash = $result->row(0)->password;
			if (password_verify($password, $hash)) {
				return $result->row(0)->admin_id;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	public function forgetpassword($email, $rand)
	{
		$data = array(
			'admin_key' => $rand
		);
		$this->security->xss_clean($data);
		$this->db->where('email', $email);
		$this->db->update('admin', $data);
	}
	public function admindetail($admin_key)
	{
		$this->db->where('admin_key', $admin_key);
		$query = $this->db->get('admin');
		return $query->row_array();
	}
	public function updatepassword($enc_password, $email)
	{
		$data = array(
			'password' => $enc_password,
			'admin_key' => 'null'
		);
		$this->security->xss_clean($data);
		$this->db->where('email', $email);
		$this->db->update('admin', $data);
	}
	public function get_countries()
	{
		$query = $this->db->get('countries');
	}


	public function get_admin($adminid)
	{
		$this->db->join('countries', 'admin.admin_country = countries.country_id', 'left');
		$this->db->where('admin_id', $adminid);
		$query = $this->db->get('admin');
		return $query->row_array();
	}
	public function get_admins()
	{
		$query = $this->db->get('admin');
		return $query->result_array();
	}
	public function contact()
	{
		$data = array(

			'contact_us_name' => $this->input->post('contact_us_name'),
			'contact_us_email' => $this->input->post('contact_us_email'),
			'contact_us_phone' => $this->input->post('contact_us_phone'),
			'contact_us_subject' => $this->input->post('contact_us_subject'),
			'contact_us_massage' => $this->input->post('contact_us_massage'),
			'contact_us_status' => 'pending'

		);
		$this->security->xss_clean($data);
		$this->db->insert('contact_us', $data);
	}
	public function get_contacts()
	{
		$this->db->order_by('contact_us_status', 'ASC');
		$this->db->order_by('contact_us_id', 'DESC');

		$query = $this->db->get('contact_us');
		return $query->result_array();
	}
	public function resolve($contactid)
	{
		$data = array(
			'contact_us_status' => 'solved'
		);
		$this->security->xss_clean($data);
		$this->db->where('contact_us_id', $contactid);
		$this->db->update('contact_us', $data);
	}

	public function addadmin($enc_password, $imgname)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'mobile' => $this->input->post('mobile'),
			'country' => $this->input->post('country'),
			'city' => $this->input->post('city'),
			'state' => $this->input->post('state'),
			'zipcode_admin_id' => $this->input->post('zipcode'),
			'role' => $this->input->post('role'),
			'password' => $enc_password,
			'corporate_application' => $imgname,
			'profile_image' => 'no_image.jpg',
			'status' => 'deactivate',
			'registration_date' => date('d/m/Y')
		);
		$this->security->xss_clean($data);
		return $this->db->insert('admin', $data);
	}
	public function adminimage($imgname, $admin_id)
	{
		$data = array(
			'profile_image' => $imgname
		);
		$this->security->xss_clean($data);
		$this->db->where('admin_id', $admin_id);
		$this->db->update('admin', $data);
	}
	function update_admin($admin_id)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'country' => $this->input->post('country'),
			'city' => $this->input->post('city'),
			'state' => $this->input->post('state'),
			'zipcode_admin_id' => $this->input->post('zipcode'),
			'mobile' => $this->input->post('mobile'),
		);
		$this->security->xss_clean($data);
		$this->db->where('admin_id', $admin_id);
		$this->db->update('admin', $data);
	}
	public function deleteadmin($admin_id)
	{
		$data = array(
			'status' => 'delete'
		);
		$this->security->xss_clean($data);
		$this->db->where('admin_id', $admin_id);
		$this->db->update('admin', $data);
	}

	public function get_users()
	{
		$query = $this->db->get('users');
		return $query->result_array();
	}
	public function get_users_status()
	{
		$this->db->where('status', 'deactivate');
		$query = $this->db->get('users');
		return $query->result_array();
	}

	public function deleteuser($user_id)
	{
		$data = array(
			'status' => 'delete'
		);
		$this->security->xss_clean($data);
		$this->db->where('user_id', $user_id);
		$this->db->update('users', $data);
	}

	public function disablecourse($course_id)
	{
		$data = array(
			'course_status' => 'disable'
		);
		$this->security->xss_clean($data);
		$this->db->where('couse_id', $course_id);
		$this->db->update('courses', $data);
	}
	public function enablecourse($course_id)
	{
		$data = array(
			'course_status' => 'enable'
		);
		$this->security->xss_clean($data);
		$this->db->where('couse_id', $course_id);
		$this->db->update('courses', $data);
	}

	public function get_states_by_countryid($countryid)
	{
		$this->db->where('state_country_id', $countryid);
		$query = $this->db->get('states');
		return $query->result_array();
	}


	
	public function get_subcatbyid($catid)
	{
		$this->db->where('category_id_sub', $catid);
		$query = $this->db->get('sub_categories');
		return $query->result_array();
	}


	public function get_cities_by_stateid($state_id)
	{
		$this->db->where('city_state_id', $state_id);
		$query = $this->db->get('cities');
		return $query->result_array();
	}
























	/////////////////////////////////////////////////// user


	/////////////////////////////////////////////////// orders
	public function get_orders()
	{
		$this->db->join('users', 'order_food.order_user_id = users.user_id', 'left');
		$query = $this->db->get('order_food');
		return $query->result_array();
	}
	public function get_orders_user($user_id)
	{
		$this->db->where('order_user_id', $user_id);
		$query = $this->db->get('order_food');
		return $query->result_array();
	}
	public function approved($orderid)
	{
		$data = array(
			'order_status' => 'approved'
		);
		$this->security->xss_clean($data);
		$this->db->where('order_id', $orderid);
		return $this->db->update('order_food', $data);
	}
	public function rejected($orderid)
	{
		$data = array(
			'order_status' => 'rejected'
		);
		$this->security->xss_clean($data);
		$this->db->where('order_id', $orderid);
		return $this->db->update('order_food', $data);
	}
	public function cancelled($orderid)
	{
		$data = array(
			'order_status' => 'cancelled'
		);
		$this->security->xss_clean($data);
		$this->db->where('order_id', $orderid);
		return $this->db->update('order_food', $data);
	}
	public function readyfordelivery($orderid)
	{
		$data = array(
			'order_status' => 'readyfordelivery'
		);
		$this->security->xss_clean($data);
		$this->db->where('order_id', $orderid);
		return $this->db->update('order_food', $data);
	}
	public function delivered($orderid)
	{
		$data = array(
			'order_status' => 'delivered'
		);
		$this->security->xss_clean($data);
		$this->db->where('order_id', $orderid);
		return $this->db->update('order_food', $data);
	}
	public function get_delivery()
	{
		$this->db->join('users', 'order_food.order_user_id = users.user_id', 'left');
		$this->db->where('order_food.order_status', 'delivered');
		$query = $this->db->get('order_food');
		return $query->result_array();
	}
	public function get_Pendingapproval()
	{
		$this->db->join('users', 'order_food.order_user_id = users.user_id', 'left');
		$this->db->where('order_food.order_status', 'pendingapproval');
		$query = $this->db->get('order_food');
		return $query->result_array();
	}
	public function get_readyfordelivery()
	{
		$this->db->join('users', 'order_food.order_user_id = users.user_id', 'left');
		$this->db->where('order_food.order_status', 'readyfordelivery');
		$query = $this->db->get('order_food');
		return $query->result_array();
	}
	public function get_orders_userid($user_id)
	{
		$this->db->join('users', 'order_food.order_user_id = users.user_id', 'left');
		$this->db->where('order_user_id', $user_id);
		$query = $this->db->get('order_food');
		return $query->result_array();
	}
	public function get_order($order_id)
	{
		$this->db->join('users', 'order_food.order_user_id = users.user_id', 'left');
		$this->db->where('order_id', $order_id);
		$query = $this->db->get('order_food');
		return $query->row_array();
	}
	public function updateorderfood($orderid, $user_id)
	{
		if (!empty($this->input->post('oldaddress'))) {
			$address = $this->input->post('oldaddress');
			$zipcode = $this->input->post('oldzipcode');
		} else {
			$address = $this->input->post('newaddress');
			$zipcode = $this->input->post('newzipcode');
			$data = array(
				'address' => 	$address,
				'zipcode' => $zipcode,
				'address_user_id' => $user_id
			);
			$this->security->xss_clean($data);
			$this->db->insert('address', $data);
		}
		$data = array(
			'date' => $this->input->post('date'),
			'time' => $this->input->post('time'),
			'guest' => $this->input->post('guest'),
			'delivery_instruction' => $this->input->post('delivery_instruction'),
			'address' => $address,
			'zipcode' => $zipcode,
			'order_status' => 'pendingapproval',
		);
		$this->security->xss_clean($data);
		$this->db->where('order_id', $orderid);
		$this->db->update('order_food', $data);
	}
	public function get_order_food_item($order_id)
	{
		$this->db->where('order_id_food', $order_id);
		$query = $this->db->get('order_food_item');
		return $query->result_array();
	}
	public function get_order_desert_item($order_id)
	{
		$this->db->where('order_id_desert', $order_id);
		$query = $this->db->get('order_desert_item');
		return $query->result_array();
	}
	public function get_order_item_diet($order_id)
	{
		$this->db->join('diet', 'order_item_diet.diet_id = diet.diet_id', 'left');
		$this->db->where('order_id_diet', $order_id);
		$query = $this->db->get('order_item_diet');
		return $query->result_array();
	}
	/////////////////////////////////////////////////// user end

	/////////////////////////////////////////////////// notification start
	public function pendingnotification($orderid, $userid)
	{
		$data = array(
			'notification_title' => 'Your order status set to pending, your Order Id: #' . $orderid,
			'notification' => 'Your order status set to pending Order Id: #' . $orderid,
			'notification_user_id' => $userid,
			'admin_user' => 'user',
			'date_time' => date('d/m/Y h:m:s'),
			'status' => 'unread'
		);
		$this->security->xss_clean($data);
		return $this->db->insert('notification', $data);
	}
	public function approvednotification($userid, $orderid)
	{
		$data = array(
			'notification_title' => 'Your order has been approved, your Order Id: #' . $orderid,
			'notification' => 'Your order has been approved, your Order Id: #' . $orderid,
			'notification_user_id' => $userid,
			'admin_user' => 'user',
			'date_time' => date('d/m/Y h:m:s'),
			'status' => 'unread'
		);
		$this->security->xss_clean($data);
		return $this->db->insert('notification', $data);
	}
	public function rejectednotification($userid, $orderid)
	{
		$data = array(
			'notification_title' => 'Your Order Has Been Rejected Order ID' . $orderid,
			'notification' => 'Your Order Has Been Rejected Order ID : ' . $orderid,
			'notification_user_id' => $userid,
			'admin_user' => 'user',
			'date_time' => date('d/m/Y h:m:s'),
			'status' => 'unread'
		);
		$this->security->xss_clean($data);
		return $this->db->insert('notification', $data);
	}
	public function cancellednotification($userid, $orderid)
	{
		$data = array(
			'notification_title' => 'Your Order Has Been Cancelled Order Id' . $orderid,
			'notification' => 'Your Order Has Been Cancelled Order Id' . $orderid,
			'notification_user_id' => $userid,
			'admin_user' => 'user',
			'date_time' => date('d/m/Y h:m:s'),
			'status' => 'unread'
		);
		$this->security->xss_clean($data);
		return $this->db->insert('notification', $data);
	}
	public function readyfordeliverynotification($userid, $orderid)
	{
		$data = array(
			'notification_title' => 'Your Order Has Been Ready For Delivery order ID' . $orderid,
			'notification' => 'Your Order Has Been Ready For Delivery order ID' . $orderid,
			'notification_user_id' => $userid,
			'admin_user' => 'user',
			'date_time' => date('d/m/Y h:m:s'),
			'status' => 'unread'
		);
		$this->security->xss_clean($data);
		return $this->db->insert('notification', $data);
	}
	public function deliverednotification($userid, $orderid)
	{
		$data = array(
			'notification_title' => 'Your Order Has Been Delivered order ID' . $orderid,
			'notification' => 'Your Order Has Been Delivered order ID' . $orderid,
			'notification_user_id' => $userid,
			'admin_user' => 'user',
			'date_time' => date('d/m/Y h:m:s'),
			'status' => 'unread'
		);
		$this->security->xss_clean($data);
		return $this->db->insert('notification', $data);
	}
	public function updateorder($orderid, $userid)
	{
		$data = array(
			'notification_title' => 'Your Order Has Been Updated order ID' . $orderid,
			'notification' => 'Your Order Has Been successfully Updated order ID' . $orderid,
			'notification_user_id' => $userid,
			'admin_user' => 'user',
			'date_time' => date('d/m/Y h:m:s'),
			'status' => 'unread'
		);
		$this->security->xss_clean($data);
		return $this->db->insert('notification', $data);
	}
	public function order_arrived($orderid)
	{
		$data = array(
			'notification_title' => 'Please check new arder is arrived orderid' . $orderid,
			'notification' => 'Please check new arder is arrived orderid' . $orderid,
			'admin_user' => 'admin',
			'date_time' => date('d/m/Y h:m:s'),
			'status' => 'unread'
		);
		$this->security->xss_clean($data);
		return $this->db->insert('notification', $data);
	}
	public function user_registered_notify($userid)
	{
		$data = array(
			'notification_title' => 'New user is registered user id' . $userid,
			'notification' => 'New user is registered user id' . $userid,
			'admin_user' => 'admin',
			'date_time' => date('d/m/Y h:m:s'),
			'status' => 'unread'
		);
		$this->security->xss_clean($data);
		return $this->db->insert('notification', $data);
	}
	public function admin_registered_notify($adminid)
	{
		$data = array(
			'notification_title' => 'New admin is registered admin id ' . $adminid,
			'notification' => 'New admin is registered admin id ' . $adminid,
			'admin_user' => 'admin',
			'date_time' => date('d/m/Y h:m:s'),
			'status' => 'unread'
		);
		$this->security->xss_clean($data);
		return $this->db->insert('notification', $data);
	}
	public function get_notifications()
	{
		$this->db->where('admin_user', 'admin');
		$query = $this->db->get('notification');
		return $query->result_array();
	}
	public function get_notification_count()
	{
		$this->db->where('admin_user', 'admin');
		$this->db->where('status', 'unread');
		$query = $this->db->get('notification');
		return $query->num_rows();
	}
	public function get_notification()
	{
		$this->db->order_by('notification.notification_id', 'desc');
		$this->db->limit(10);
		$this->db->where('admin_user', 'admin');
		$this->db->where('status', 'unread');
		$query = $this->db->get('notification');
		return $query->result_array();
	}
	public function get_notification_count_user($userid)
	{
		$this->db->where('notification_user_id', $userid);
		$this->db->where('admin_user', 'user');
		$this->db->where('status', 'unread');
		$query = $this->db->get('notification');
		return $query->num_rows();
	}
	public function get_notification_user($userid)
	{
		$this->db->order_by('notification.notification_id', 'desc');
		$this->db->limit(10);
		$this->db->where('notification_user_id', $userid);
		$this->db->where('admin_user', 'user');
		$this->db->where('status', 'unread');
		$query = $this->db->get('notification');
		return $query->result_array();
	}
	public function get_notifications_user($userid)
	{
		$this->db->where('notification_user_id', $userid);
		$this->db->where('admin_user', 'user');
		$query = $this->db->get('notification');
		return $query->result_array();
	}
	/////////////////////////////////////////////////// notification end

	/////////////////////////////////////////////////// diet
	public function adddiet()
	{
		$data = array(
			'diet_name' => $this->input->post('diet_name'),
			'diet_price' => $this->input->post('diet_price'),
		);
		$this->security->xss_clean($data);
		return $this->db->insert('diet', $data);
	}
	public function updatediet($dietid)
	{
		$data = array(
			'diet_name' => $this->input->post('diet_name'),
			'diet_price' => $this->input->post('diet_price'),
		);
		$this->security->xss_clean($data);
		$this->db->where('diet_id', $dietid);
		return $this->db->update('diet', $data);
	}
	public function deletediet($dietid)
	{
		$this->db->where('diet_id', $dietid);
		$this->db->delete('diet');
	}
	public function get_diet()
	{
		$query = $this->db->get('diet');
		return $query->result_array();
	}
	/////////////////////////////////////////////////// diet end

	/////////////////////////////////////////////////// country

	public function get_country($countryid)
	{
		$this->db->where('country_id', $countryid);
		$query = $this->db->get('countries');
		return $query->row_array();
	}
	public function get_active_country()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('countries');
		return $query->result_array();
	}
	public function get_countries_deactivate()
	{
		$this->db->where('status', 'deactivate');
		$query = $this->db->get('countries');
		return $query->result_array();
	}
	public function activecountry($countryid)
	{
		$data = array(
			'status' => 'active'
		);
		$this->security->xss_clean($data);
		$this->db->where('country_id', $countryid);
		return $this->db->update('countries', $data);
	}
	public function deactivatecountry($countryid)
	{
		$data = array(
			'status' => 'deactivate'
		);
		$this->security->xss_clean($data);
		$this->db->where('country_id', $countryid);
		return $this->db->update('countries', $data);
	}
	/////////////////////////////////////////////////// country end

	/////////////////////////////////////////////////// city
	public function get_cities()
	{
		$query = $this->db->get('cities');
		return $query->result_array();
	}
	public function get_cities_deactivate()
	{
		$this->db->where('status', 'deactivate');
		$query = $this->db->get('cities');
		return $query->result_array();
	}
	public function get_cities_by_country($country)
	{
		$this->db->where('country_name', $country);
		$query = $this->db->get('cities');
		return $query->result_array();
	}
	public function get_cities_active()
	{
		$this->db->where('status', 'active');
		$query = $this->db->get('cities');
		return $query->result_array();
	}
	public function get_city($cityid)
	{
		$this->db->where('city_id', $cityid);
		$query = $this->db->get('cities');
		return $query->row_array();
	}
	/////////////////////////////////////////////////// city end

	/////////////////////////////////////////////////// category
	public function addcategory()
	{
		$data = array(
			'category_name' => $this->input->post('category_name'),
			'category_price' => $this->input->post('category_price'),
		);
		$this->security->xss_clean($data);
		return $this->db->insert('category', $data);
	}
	public function updatecategory($categoryid)
	{
		$data = array(
			'category_name' => $this->input->post('category_name'),
			'category_price' => $this->input->post('category_price'),
		);
		$this->security->xss_clean($data);
		$this->db->where('category_id', $categoryid);
		return $this->db->update('category', $data);
	}
	public function deletecategory($categoryid)
	{
		$this->db->where('category_id', $categoryid);
		$this->db->delete('category');
	}
	public function get_category()
	{
		$query = $this->db->get('category');
		return $query->result_array();
	}
	/////////////////////////////////////////////////// category end

	/////////////////////////////////////////////////// zipcode
	public function addzipcode()
	{
		$data = array(
			'zipcode' => $this->input->post('zipcode'),
		);
		$this->security->xss_clean($data);
		return $this->db->insert('zipcode', $data);
	}
	public function updatezipcode($zipcodeid)
	{
		$data = array(
			'zipcode' => $this->input->post('zipcode'),
		);
		$this->security->xss_clean($data);
		$this->db->where('zipcode_id', $zipcodeid);
		return $this->db->update('zipcode', $data);
	}
	public function deletezipcode($zipcodeid)
	{
		$this->db->where('zipcode_id', $zipcodeid);
		$this->db->delete('zipcode');
	}
	public function get_zipcode($zipcodeid)
	{
		$this->db->where('zipcode_id', $zipcodeid);
		$query = $this->db->get('zipcode');
		return $query->row_array();
	}
	public function get_zipcodes()
	{
		$query = $this->db->get('zipcode');
		return $query->result_array();
	}
	/////////////////////////////////////////////////// zipcode end

	/////////////////////////////////////////////////// settings
	public function guest()
	{
		$this->db->where('id', '1');
		$query = $this->db->get('settings');
		return $query->row_array();
	}
	public function updateguest()
	{
		$data = array(
			'guest' => $this->input->post('guest'),
		);
		$this->security->xss_clean($data);
		$this->db->where('id', '1');
		return $this->db->update('settings', $data);
	}
	/////////////////////////////////////////////////// settings end

	/////////////////////////////////////////////////// ajax
	public function ajax_edit_category($categoryid)
	{
		$this->db->where('category_id', $categoryid);
		$query = $this->db->get('category');
		return $query->row_array();
	}
	public function ajax_edit_diet($dietid)
	{
		$this->db->where('diet_id', $dietid);
		$query = $this->db->get('diet');
		return $query->row_array();
	}
	/////////////////////////////////////////////////// ajax end
}

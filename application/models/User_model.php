<?php
class User_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function login($email, $password)
	{
		$this->db->where('user_email', $email);
		$result = $this->db->get('users');
		if ($result->num_rows() == 1) {
			$hash = $result->row(0)->user_password;

			if (password_verify($password, $hash)) {

				return $result->row(0)->user_id;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function generatecode($user_email, $randnum)
	{

		$data = array(

			'user_key' => $randnum,

		);
		$this->security->xss_clean($data);
		$this->db->where('user_email', $user_email);
		$this->db->update('users', $data);
	}

	public function changepassword($user_email, $password)
	{
		$data = array(

			'user_password' => $password,

		);
		$this->security->xss_clean($data);
		$this->db->where('user_email', $user_email);
		$this->db->update('users', $data);
	}

	public function userdetail($conformationcode, $useremail)
	{
		$this->db->where('user_key', $conformationcode);
		$this->db->where('user_email', $useremail);

		$query = $this->db->get('users');
		return $query->row_array();
	}

	public function updatepassword($enc_password, $email)
	{
		$data = array(
			'password' => $enc_password,
			'user_key' => 'null'
		);
		$this->security->xss_clean($data);
		$this->db->where('email', $email);
		$this->db->update('admin', $data);
	}

	public function registeration($enc_password)
	{
		$data = array(

			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'user_email' => $this->input->post('user_email'),
			'user_password' => $enc_password,
			'user_gender' => $this->input->post('user_gender'),
			'user_country_id' => $this->input->post('user_country_id'),
			'user_state_id' => $this->input->post('user_state_id'),
			'user_city_id' => $this->input->post('user_city_id'),
			'user_street_num_name' => $this->input->post('user_street_num_name'),
			'user_postal_code' => $this->input->post('user_postal_code'),
			'user_address' => $this->input->post('user_address'),
			'user_registration_date' => date('dd/mm/Y'),
			'user_status' => 'active',
			'user_add_courses_count' => '0'

		);
		$this->security->xss_clean($data);
		$this->db->insert('users', $data);
	}
	public function get_user($user_id)
	{
		$this->db->join('countries', 'users.user_country_id = countries.country_id', 'left');
		$this->db->where('user_id', $user_id);
		$query = $this->db->get('users');
		return $query->row_array();
	}

	public function get_languages()
	{
		$query = $this->db->get('languages');
		return $query->result_array();
	}
























	public function get_categories()
	{
		$query = $this->db->get('categories');
		return $query->result_array();
	}

	public function get_subcategories()

	{
		$query = $this->db->get('sub_categories');
		return $query->result_array();
	}
	public function get_countries()
	{
		$query = $this->db->get('countries');
		return $query->result_array();
	}


	public function get_cities($countryid)
	{
		$this->db->join('states', 'states.state_id = cities.city_state_id', 'left');

		$this->db->where('state_country_id', $countryid);


		$query = $this->db->get('cities');
		return $query->result_array();
	}

	/////////////////////////////////////////////////// user start
	public function get_search_courses()
	{
		$search_courses = $this->input->post('search_courses');

		$this->db->like('course_name', $search_courses);
		$this->db->join('categories', 'courses.course_category = categories.category_id', 'left');
		$this->db->join('sub_categories', 'courses.course_subcat_id = sub_categories.sub_category_id', 'left');

		$this->db->where('course_status', 'enable');
		$query = $this->db->get('courses');


		return $query->result_array();
	}
	public function get_searchdetail_courses()
	{
		$what = $this->input->post('what');
		$where = $this->input->post('where');
		$when = $this->input->post('when');
		$this->db->join('categories', 'courses.course_category = categories.category_id', 'left');
		$this->db->join('sub_categories', 'courses.course_subcat_id = sub_categories.sub_category_id', 'left');
		$this->db->like('course_name', $what);
		$this->db->like('category_name', $what);
		$this->db->like('sub_category_name', $what);
		$this->db->like('course_country_id', $where);
		$this->db->like('course_city_name', $where);
		$this->db->like('course_start_date', $when);


		$this->db->where('course_status', 'enable');
		$query = $this->db->get('courses');


		return $query->result_array();
	}





	public function get_user_courses($userid)
	{
		$this->db->join('categories', 'courses.course_category = categories.category_id', 'left');
		$this->db->join('sub_categories', 'courses.course_subcat_id = sub_categories.sub_category_id', 'left');

		$this->db->where('course_user_id', $userid);

		$query = $this->db->get('courses');
		return $query->result_array();
	}
	public function get_course($courseid)
	{
		$this->db->join('categories', 'courses.course_category = categories.category_id', 'left');
		$this->db->join('sub_categories', 'courses.course_subcat_id = sub_categories.sub_category_id', 'left');

		$this->db->where('couse_id', $courseid);

		$query = $this->db->get('courses');
		return $query->row_array();
	}
	public function get_course_fors($courseid)
	{
		$this->db->where('courseid_course_for', $courseid);
		$query = $this->db->get('course_for');
		return $query->result_array();
	}

	public function get_course_requirements($courseid)
	{
		$this->db->where('courseid_requirement', $courseid);
		$query = $this->db->get('course_requirement');
		return $query->result_array();
	}







	public function get_coursesbycat($cat_id)
	{
		$this->db->join('categories', 'courses.course_category = categories.category_id', 'left');
		$this->db->join('sub_categories', 'courses.course_subcat_id = sub_categories.sub_category_id', 'left');

		$this->db->where('course_category', $cat_id);
		$this->db->where('course_status', 'enable');
		$query = $this->db->get('courses');
		return $query->result_array();
	}
	public function get_recent_courses()
	{
		$this->db->join('categories', 'courses.course_category = categories.category_id', 'left');
		$this->db->join('sub_categories', 'courses.course_subcat_id = sub_categories.sub_category_id', 'left');

		$this->db->where('course_status', 'enable');
		$this->db->order_by("couse_id", "Desc");
		$this->db->limit(10);
		$query = $this->db->get('courses');
		return $query->result_array();
	}

	public function get_all_courses()
	{
		$this->db->join('categories', 'courses.course_category = categories.category_id', 'left');
		$this->db->join('sub_categories', 'courses.course_subcat_id = sub_categories.sub_category_id', 'left');

		$query = $this->db->get('courses');
		return $query->result_array();
	}
	public function get_courses()
	{

		$this->db->join('categories', 'courses.course_category = categories.category_id', 'left');
		$this->db->join('sub_categories', 'courses.course_subcat_id = sub_categories.sub_category_id', 'left');

		$this->db->where('course_status', 'enable');
		$query = $this->db->get('courses');
		return $query->result_array();
	}
	public function get_active_courses()
	{
		$this->db->join('categories', 'courses.course_category = categories.category_id', 'left');
		$this->db->join('sub_categories', 'courses.course_subcat_id = sub_categories.sub_category_id', 'left');

		$this->db->where('course_status', 'enable');
		$query = $this->db->get('courses');
		return $query->result_array();
	}


	public function add_course()
	{
		$data = array(

			'course_name' => $this->input->post('course_name'),
			'course_full_desc' => $this->input->post('course_full_desc'),
			'course_category' => $this->input->post('course_category'),
			'course_price' => $this->input->post('course_price'),
			'course_type' => $this->input->post('course_type'),
			'course_total_students' => $this->input->post('course_total_students'),
			'course_instructor_name' => $this->input->post('course_instructor_name'),
			'course_language' => $this->input->post('course_language'),
			'course_user_id' => $this->session->userdata('qvoot_user_id'),
			'course_level' => $this->input->post('course_level'),
			'course_certificate' => $this->input->post('course_certificate'),
			'course_country_id' => $this->input->post('course_country_id'),
			'course_city_name' => $this->input->post('course_city_name'),
			'course_start_date' => $this->input->post('course_start_date'),
			'course_subcat_id' => $this->input->post('course_subcat_id'),
			'course_status' => 'disable',
			'course_currency' => $this->input->post('course_currency'),
			'course_submit_date' => date('d/m/Y')
		);
		$this->security->xss_clean($data);
		$this->db->insert('courses', $data);
		return $this->db->insert_id();
	}




	public function register($enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'country' => $this->input->post('country'),
			'city' => $this->input->post('city'),
			'address' => $this->input->post('address'),
			'password' => $enc_password,
			'status' => 'deactivate',
			'complete' => 'no',
			'registration_date' => date('d/m/Y')
		);
		$this->security->xss_clean($data);
		$this->db->insert('users', $data);
		return $this->db->insert_id();
	}


	public function profileimage($imgname)
	{
		$data = array(
			'profile_image' => $imgname
		);
		$this->security->xss_clean($data);
		$this->db->where('user_id', $this->input->post('user_id'));
		$this->db->update('users', $data);
	}


	public function add_user_course($addone, $user_id)
	{
		$data = array(
			'user_add_courses_count' => $addone
		);
		$this->security->xss_clean($data);
		$this->db->where('user_id', $user_id);
		$this->db->update('users', $data);
	}

	public function get_profiles()
	{


		$query = $this->db->get('users');
		return $query->result_array();
	}
	public function update_profile($user_id)
	{
		$data = array(
			'name' => $this->input->post('name'),
			/////////////////////////////////////////////////// 'email' => $this->input->post('email'),
			'country' => $this->input->post('country'),
			'city' => $this->input->post('city'),
			'state' => $this->input->post('state'),
			'zipcode_user_id' => $this->input->post('zipcode_user_id'),
			'credit_card' => $this->input->post('credit_card'),
			'name_of_organization' => $this->input->post('name_of_organization'),
			'cantact_name' => $this->input->post('cantact_name'),
			'phone_num' => $this->input->post('phone_num'),
			'mobile' => $this->input->post('mobile'),
			'delivery_address' => $this->input->post('delivery_address'),
			'account_note' => $this->input->post('account_note'),
			'delivery_instruction' => $this->input->post('delivery_instruction'),
			/////////////////////////////////////////////////// 'diet_preference' => $this->input->post('diet_preference'),
			'allergies' => $this->input->post('allergies'),
			'budget_preference' => $this->input->post('budget_preference'),
			'day_Schedule' => $this->input->post('day_Schedule'),
			'complete' => 'yes'
		);
		$this->security->xss_clean($data);
		$this->db->where('user_id', $user_id);
		$this->db->update('users', $data);
	}

	public function forgetpassword($email, $rand)
	{
		$data = array(
			'users_key' => $rand
		);
		$this->security->xss_clean($data);
		$this->db->where('email', $email);
		$this->db->update('users', $data);
	}
	public function usersdetail($users_key)
	{
		$this->db->where('users_key', $users_key);
		$query = $this->db->get('users');
		return $query->row_array();
	}

	public function get_profile($user_id)
	{
		$this->db->join('countries', 'users.user_country_id = countries.country_id', 'left');
		$this->db->where('user_id', $user_id);
		$query = $this->db->get('users');
		return $query->row_array();
	}
	public function updateaddress($addressid)
	{
		$data = array(
			'address' => $this->input->post('address'),
			'zipcode' => $this->input->post('zipcode')
		);
		$this->security->xss_clean($data);
		$this->db->where('address_id', $addressid);
		$this->db->update('address', $data);
	}
	public function deleteaddress($addressid)
	{
		$this->db->where('address_id', $addressid);
		$this->db->delete('address');
	}
	public function addaddress($user_id)
	{
		$data = array(
			'address' => $this->input->post('address'),
			'zipcode' => $this->input->post('zipcode'),
			'address_user_id' => $user_id
		);
		$this->security->xss_clean($data);
		$this->db->insert('address', $data);
	}
	public function get_address($user_id)
	{
		$this->db->join('zipcode', 'address.zipcode = zipcode.zipcode_id', 'left');
		$this->db->where('address_user_id', $user_id);
		$query = $this->db->get('address');
		return $query->result_array();
	}
	public function get_addresses()
	{
		$this->db->join('zipcode', 'address.zipcode = zipcode.zipcode_id', 'left');
		$query = $this->db->get('address');
		return $query->result_array();
	}
	/////////////////////////////////////////////////// user end
	/////////////////////////////////////////////////// order start
	public function get_order_favorites($user_id)
	{
		$this->db->join('users', 'order_food.order_user_id = users.user_id', 'left');
		$this->db->where('order_food.favorites', 'yes');
		$this->db->where('order_user_id', $user_id);
		$query = $this->db->get('order_food');
		return $query->result_array();
	}

	public function get_pictures()
	{

		$query = $this->db->get('course_pictures');
		return $query->result_array();
	}


	public function get_course_pictures($courseid)
	{
		$this->db->where('course_id', $courseid);

		$query = $this->db->get('course_pictures');
		return $query->result_array();
	}

	public function favorites($orderid)
	{
		$data = array(
			'favorites' => 'yes',
		);
		$this->security->xss_clean($data);
		$this->db->where('order_id', $orderid);
		$this->db->update('order_food', $data);
	}
	public function removefavorites($orderid)
	{
		$data = array(
			'favorites' => 'no',
		);
		$this->security->xss_clean($data);
		$this->db->where('order_id', $orderid);
		$this->db->update('order_food', $data);
	}
	public function delete_diet_preference($user_id)
	{
		$this->db->where('diet_preference_user_id', $user_id);
		$this->db->delete('diet_preference');
	}
	public function get_diet_preference($user_id)
	{
		$this->db->join('diet', 'diet_preference.diet_preference = diet.diet_id', 'left');
		$this->db->where('diet_preference_user_id', $user_id);
		$query = $this->db->get('diet_preference');
		return $query->result_array();
	}
	public function orderfood($user_id)
	{
		if (!empty($this->input->post('oldaddress'))) {
			$address = $this->input->post('oldaddress');
			$zipcode = $this->input->post('oldzipcode');
		} else {
			$address = $this->input->post('newaddress');
			$zipcode = $this->input->post('newzipcode');
			$data = array(
				'address' => 	$address,
				'zipcode' => $zipcode,
				'address_user_id' => $user_id
			);
			$this->security->xss_clean($data);
			$this->db->insert('address', $data);
		}
		$data = array(
			'date' => $this->input->post('date'),
			'time' => $this->input->post('time'),
			'admin_id' => $this->input->post('admin_id'),
			///////////////////////////////////////////////////'food' => $this->input->post('food'),
			'guest' => $this->input->post('guest'),
			///////////////////////////////////////////////////'desert' => $this->input->post('desert'),
			'delivery_instruction' => $this->input->post('delivery_instruction'),
			'address' => $address,
			'zipcode' => $zipcode,
			'order_status' => 'pendingapproval',
			'order_user_id' => $user_id
		);
		$this->security->xss_clean($data);
		$this->db->insert('order_food', $data);
		return $this->db->insert_id();
	}
	public function get_food()
	{
		$query = $this->db->get('food');
		return $query->result_array();
	}
	public function get_desert()
	{
		$query = $this->db->get('desert');
		return $query->result_array();
	}
	public function get_diet()
	{
		$query = $this->db->get('diet');
		return $query->result_array();
	}
	/////////////////////////////////////////////////// order end
	/////////////////////////////////////////////////// notification start
	public function get_notification($user_id)
	{
		$this->db->where('notification_user_id', $user_id);
		$this->db->where('admin_user', '0');
		$query = $this->db->get('notification');
		return $query->result_array();
	}
	public function notificationread($notificationid)
	{
		$data = array(
			'status' => 'read'
		);
		$this->security->xss_clean($data);
		$this->db->where('notification_id', $notificationid);
		$this->db->update('notification', $data);
	}
	public function notificationunread($notificationid)
	{
		$data = array(
			'status' => 'unread'
		);
		$this->security->xss_clean($data);
		$this->db->where('notification_id', $notificationid);
		$this->db->update('notification', $data);
	}
	public function get_ajax_notification($notification_id)
	{
		$this->db->where('notification_id', $notification_id);
		$query = $this->db->get('notification');
		return $query->row_array();
	}
	/////////////////////////////////////////////////// notification end
	/////////////////////////////////////////////////// ajax
	public function get_ajax_address($addressid)
	{
		$this->db->where('address_id', $addressid);
		$query = $this->db->get('address');
		return $query->row_array();
	}
	/////////////////////////////////////////////////// ajax end
}
?>
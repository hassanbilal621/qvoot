	<div class="row">

<a onclick="reloadPage();" class="modal-close right"><i class="material-icons">close</i></a>
		<?php echo form_open('admin/states'); ?>
		<h4 class="card-title">Add State</h4>
		<div class="row">
			<div class="input-field col s4">
				<select class="browser-default" name="" id="" required>
					<?php foreach ($countries as $country) : ?>
						<option value="<?php echo $country['country_id']; ?>"><?php echo $country['country_name']; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="input-field col s4">
				<select class="browser-default" name="city_state_id" id="" required>
					<?php foreach ($cities as $city) : ?>
						<option value="<?php echo $city['city_id']; ?>"><?php echo $city['city_name']; ?></option>
					<?php endforeach; ?>
				</select>
			</div>

			<div class="input-field col s4">
				<input type="text" name="state_name" placeholder="Select Country & city First Then Type state" required>
			</div>
			<button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Save State
				<i class="material-icons right">send</i>
			</button>
		</div>
		<?php echo form_close(); ?>
	</div>
<?php echo form_open('users/addaddress') ?>
<div class="row">
<a onclick="reloadPage();" class="modal-close right"><i class="material-icons">close</i></a>
    <div class="input-field col s8">
        <textarea name="address" class="discrip" placeholder="Type Your New Address" required></textarea>
        <input type="hidden" name="addressid" value="<?php echo $profile['user_id']; ?>"> 
    </div>
    <div class="input-field col s4">
        <select class="select2 browser-default" name="zipcode" required>
        <?php foreach ($zipcodes as $zipcode) : ?>
               
                    <option value="<?php echo $zipcode['zipcode_id']; ?>"><?php echo $zipcode['zipcode']; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>

<button type="submit" class="waves-effect waves-light  btn submit box-shadow-none mb-2 border-round right">Save</button>


<?php echo form_close(); ?>
<div class="row">
    <div class="col s12">
    <a onclick="reloadPage();" class="modal-close right"><i class="material-icons">close</i></a>
        <h6><?php echo $notification['notification_title']; ?></h6>
        <span><?php echo $notification['notification']; ?></span>
    </div>
</div>
<script>
    function reloadPage(){
        location.reload(true);
    }
</script>
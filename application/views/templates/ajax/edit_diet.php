<div class="row">
    <div class="col s12">
        <h4 class="card-title">Update Diet</h4>
        <a onclick="reloadPage();" class="modal-close right"><i class="material-icons">close</i></a>
    </div>
    <?php echo form_open('admin/updatediet'); ?>
    <div class="row">

        <div class="input-field col s6">
            <input type="text" name="diet_name" placeholder="Add diet" value="<?php echo $diet['diet_name']; ?>" required>
            <input type="hidden" name="diet_id" value="<?php echo $diet['diet_id']; ?>">
        </div>
        <div class="input-field col s6">
            <input type="text" name="diet_price" placeholder="Add diet Price" value="<?php echo $diet['diet_price']; ?>" required>
        </div>
        <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Submit
            <i class="material-icons right">send</i>
        </button>
    </div>
    <?php echo form_close(); ?>
</div>
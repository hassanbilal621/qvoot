<?php echo form_open('users/updateaddress') ?>
<div class="row">
    <a onclick="reloadPage();" class="modal-close right"><i class="material-icons">close</i></a>
    <div class="input-field col s8">
        <textarea name="address" class="discrip" placeholder="Type Your New Address"><?php echo $address['address']; ?></textarea>
        <input type="hidden" name="addressid" value="<?php echo $address['address_id']; ?>">
    </div>
    <div class="input-field col s4">
        <select class="select2 browser-default" name="zipcode">
            <?php foreach ($zipcodes as $zipcode) : ?>
                <?php if ($profile['zipcode'] == $zipcode['zipcode_id']) {
                ?>
                    <option selected value="<?php echo $zipcode['zipcode_id']; ?>"><?php echo $zipcode['zipcode']; ?></option>
                <?php
                } else {
                ?>
                    <option value="<?php echo $zipcode['zipcode_id']; ?>"><?php echo $zipcode['zipcode']; ?></option>
                <?php } ?>
            <?php endforeach; ?>
        </select>
    </div>
</div>

<button type="submit" class="waves-effect waves-light  btn submit box-shadow-none mb-2 border-round right">Save</button>


<?php echo form_close(); ?>
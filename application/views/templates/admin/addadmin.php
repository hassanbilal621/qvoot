<div id="main">
   <div class="row">
   <div id="breadcrumbs-wrapper">
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0">
                     <span<span style="font-weight: bold;">Add Admin</span>
                  </h5>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12">
         <div class="container">
            <div class="users-list-table">
               <div class="card">
                  <div class="card-content">
                  <?php if ($this->session->flashdata('admin_registered')) :?>
                  <div class="card-alert card green">
                     <div class="card-content white-text">
                        <span class="card-title white-text darken-1">
                           <i class="material-icons">notifications</i> admin Registered</span>
                        <p style=" margin: 0 0 0 30px; " >Your admin Has been Successfully Registered.</p>
                        </div>
                        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                            <span id="closeicon" aria-hidden="true">×</span>
                        </button>
                     </div>
                     <p> <?php echo $this->session->flashdata('admin_registered'); ?></p>
                     <?php endif; 
                     ?>
                     <?php echo form_open_multipart('admin/addadmin'); ?>
                     <div class="row margin">
                        <div class="input-field col s12  m12 l6">
                           <input type="text" name="name" required>
                           <label for="">First & Last Name</label>
                        </div>

                        <div class="input-field col s12 m12 l6">
                           <input type="number" class="validate" id="phone-code" name="mobile" required>
                           <label for="">Mobile No</label>
                        </div>
                     </div>
                     <div class="row margin">
                        <div class="input-field col s12  m12 l6">
                           <input type="email" class="validate"  name="email" required>
                           <label for="">Email</label>
                        </div>

                        <div class="input-field col s12  m12 l6">
                           <input type="password" name="password" required>
                           <label for="">Password</label>
                        </div>
                     </div>
                     <!-- <div class="row margin">
                        <div class="input-field col s12 m12 l12">
                           <input type="text" name="address" required>
                           <label for="">Address</label>
                        </div>
                     </div> -->
                     <div class="row margin">
                        <div class="input-field col s12 m12 l6">
                           <label for="" class="active">Select Country</label>
                           <select name="country" class="select2  browser-default" onchange="getcity(this.value)">
                              <?php foreach ($countries as $country) : ?>
                                 <option value="<?php echo $country['country_name']; ?>"><?php echo $country['country_name']; ?></option>
                              <?php endforeach; ?>

                           </select>
                        </div>

                        <div class="input-field col s12 m12 l6">
                           <label for="" class="active">Select City</label>
                           <select name="city" class="select2  browser-default" id="city">
                              <option disabled selected value="">Select Country First</option>
                            
                           </select>
                        </div>
                     </div>
                     <div class="row margin">
                        <div class="input-field col s12 m12 l6">
                           <label for="" class="active">Select Zip Code</label>
                           <select name="zipcode" class="select2  browser-default">
                           <?php foreach ($zipcodes as $zipcode) : ?>
                                 <option value="<?php echo $zipcode['zipcode_id']; ?>"><?php echo $zipcode['zipcode']; ?></option>
                              <?php endforeach; ?>
                           </select>
                        </div>
                        <div class="input-field col s12 m12 l6">
                           <label for="" class="active">Select Role</label>
                           <select name="role" class="select2  browser-default">
                              <option value="1">Super admin</option>
                              <option value="2">Admin</option>

                           </select>
                        </div>
                     </div>
                     <div class="row margin">
                        <div class="input-field col s12 m12 l12">
                           
                           <div class="row">
                              <div class=" col s12">
                                 <button class="btn waves-effect waves-light border-round submit right">Register Now</button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <?php echo form_close(); ?>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<script>
    function getcity(country) {
     $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/get_cities_by_countryid/" + country,
            data: 'country_name=' + country,
            success: function(data) {
                $("#city").html(data);
            }
        });
    }
</script>

<script>
   
    setInterval(function() {
        document.getElementById("closeicon").click();
        
    }, 5000);
</script>
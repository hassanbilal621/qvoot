<div id="main">
   <div class="row">
      <div id="breadcrumbs-wrapper">
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0">
                     <span<span style="font-weight: bold;">Profile</span>
                  </h5>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12">
         <div class="container">
            <!-- users view start -->
            <div class="section users-view">
               <!-- users view media object start -->
               <div class="card-panel">
                  <div class="row">
                     <div class="col s12 m7">
                        <div class="display-flex media">
                           <a href="#" class="avatar">
                              <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $profile['profile_image']; ?>" alt="users view avatar" class=" circle" height="64" width="64">
                           </a>
                           <div class="media-body">
                              <h6 class="media-heading">
                                 <span class="capitalize"><?php echo $profile['name']; ?></span> <br>
                                 <span class="capitalize grey-text"><?php echo $profile['email']; ?></span>
                              </h6>
                           </div>
                        </div>
                     </div>
                     <div class="col s12 m5 quick-action-btns display-flex justify-content-end align-items-center pt-2">
                     


                      </div>
                  </div>
                  <div class="row mt-2">
                     <ul class="tabs mb-2 row">
                        <li class="tab">
                           <a class="display-flex align-items-center" id="information-tab" href="#information" style="width: 102%;">
                              <i class="material-icons mr-2">error_outline</i><span>Information</span>
                           </a>
                        </li>
                        <li class="tab">
                           <a class="display-flex align-items-center" id="address-tab" href="#address" style="width: 102%;">
                              <i class="material-icons mr-2">place</i><span>Delivery Address</span>
                           </a>
                        </li>
                     </ul>
                     <div id="information">
                        <div class="row">
                           <div class="col s6">
                              <table class="striped">
                                 <tbody>
                                    <tr>
                                       <td class="black-text">Contact Person</td>
                                       <td class="capitalize"><?php echo $profile['cantact_name']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Phone</td>
                                       <td class="capitalize"><?php echo $profile['phone_num']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Mobile</td>
                                       <td class="capitalize"><?php echo $profile['mobile']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Credit Card</td>
                                       <td class="capitalize"><?php echo $profile['credit_card']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Name OF Organisation:</td>
                                       <td class="capitalize"><?php echo $profile['name_of_organization']; ?></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                           <div class="col s6">
                              <table class="striped">
                                 <tbody>
                                    <tr>
                                       <td class="black-text">Country</td>
                                       <td class="capitalize"><?php echo $profile['country_name']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">City</td>
                                       <td class="capitalize"><?php echo $profile['city_name']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">State</td>
                                       <td class="capitalize"><?php echo $profile['state']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Zip Code</td>
                                       <td class="capitalize"><?php echo $profile['zipcode']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Registration Date:</td>
                                       <td class="capitalize"><?php echo $profile['registration_date']; ?></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        <div class="row mt-2">
                           <div class="col s12">
                              <table class="striped">
                                 <tbody>
                                    <tr>
                                       <td class="black-text">delivery instruction:</td>
                                       <td class="capitalize"><?php echo $profile['delivery_instruction']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Account Note:</td>
                                       <td class="capitalize"><?php echo $profile['account_note']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Diet preference:</td>
                                       <td class="capitalize">
                                          <?php foreach ($diet_preferences as $diet_preference) : ?>
                                             <?php echo $diet_preference['diet_name']; ?>,
                                          <?php endforeach; ?>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Allergies:</td>
                                       <td class="capitalize"><?php echo $profile['allergies']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Budget preference:</td>
                                       <td class="capitalize"><?php echo $profile['budget_preference']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Day Schedule:</td>
                                       <td class="capitalize"><?php echo $profile['day_Schedule']; ?></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col s12">
                              <div class="col s12 input-field">
                                 <h5 class="">corporate Application</h5>

                                 <?php

                                 $file_parts = pathinfo($profile['corporate_application']);

                                 switch ($file_parts['extension']) {
                                    case "pdf": ?>
                                       <embed src="<?php echo base_url(); ?>assets/uploads/<?php echo $profile['corporate_application']; ?>" width="100%" height="400px" />
                                    <?php
                                       break;

                                    case "jpg":
                                    ?>
                                       <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $profile['corporate_application']; ?>" class="left" height="400px" />
                                    <?php

                                       break;
                                    case "jpeg":
                                    ?>
                                       <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $profile['corporate_application']; ?>" class="left" height="400px" />
                                    <?php

                                       break;
                                    case "img":
                                    ?>
                                       <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $profile['corporate_application']; ?>" class="left" height="400px" />
                                 <?php

                                       break;

                                    case "": // Handle file extension for files ending in '.'
                                    case NULL: // Handle no file extension
                                       break;
                                 }

                                 ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div id="address">
                        <a class="waves-effect waves-light  btn submit box-shadow-none mb-2 border-round right right modal-trigger" onclick="addaddress(this.id)" id="<?php echo $profile['user_id']; ?>">Add Address</a>

                        <div class="row">
                           <div class="col s12">
                              <table id="page-length-option" class="display" style="width: 100%;">
                                 <thead>
                                    <tr>
                                       <th></th>
                                       <th>id</th>
                                       <th>Address</th>
                                       <th>Zip Code</th>
                                       <th>View</th>
                                       <th>Delete</th>
                                       <th></th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php $s_no = '1';
                                    foreach ($addresss as $address) : ?>
                                       <tr>
                                          <td class="black-text"></td>
                                          <td class="black-text"><?php echo $address['address_id']; ?></td>
                                          <td class="black-text"><?php echo $address['address']; ?></td>
                                          <td class="black-text"><?php echo $address['zipcode']; ?></td>

                                          <td class="black-text"><a class="modal-trigger" onclick="loadaddress(this.id)" id="<?php echo $address['address_id']; ?>"><i class="material-icons">edit</i></a></td>
                                          <td class="black-text"><a onclick="del(this.id)" id="<?php echo $address['address_id']; ?>"><i class="material-icons">delete</i></a></td>
                                          <td class="black-text"></td>
                                       </tr>
                                    <?php $s_no++;
                                    endforeach; ?>

                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<div id="modal1" class="modal">
   <div class="modal-content">
   </div>
</div>
<script>
   function addaddress(user_id) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>users/ajax_add_address_details/" + user_id,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal1').modal('open');
         }
      });
   }
</script>

<div id="modal3" class="modal">
   <div class="modal-content">
   </div>
</div>
<script>
   function loadaddress(addressid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>users/ajax_edit_address_details/" + addressid,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal3').modal('open');
         }
      });
   }
</script>
<script>
   function del(id) {

      swal({
         title: "Are you sure?",
         text: "",
         icon: 'warning',
         dangerMode: true,
         buttons: {
            cancel: 'No!',
            delete: 'Yes'
         }
      }).then(function(willDelete) {
         if (willDelete) {
            var html = $.ajax({
               type: "GET",
               url: "<?php echo base_url(); ?>users/deleteaddress/" + id,
               // data: info,
               async: false
            }).responseText;

            if (html == "success") {
               $("#delete").html("delete success.");
               return true;

            }
            swal("", {
               icon: "success",
            });
            setTimeout(location.reload.bind(location), 1000);
         } else {
            swal("", {
               title: 'Cancelled',
               icon: "error",
            });
         }
      });


   }
</script>
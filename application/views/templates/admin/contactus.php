<div id="main">
   <div class="row">
      <div id="breadcrumbs-wrapper" data-image="<?php echo base_url(); ?>assets/breadcrumb-bg.jpg">

         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0">
                     <span<span style="font-weight: bold;color: white;">Manage contacts</span>
                  </h5>
               </div>
               <div class="col s12 m6 l6 right-align-md">
                  <ol class="breadcrumbs mt-0 mb-0">
                     <li class="breadcrumb-item"><a style="color: white;" href="<?php echo base_url(); ?>admin/index">Home</a>
                     </li>
                     <li style="color: white;" class="breadcrumb-item active">Manage contacts
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12">
         <div class="container">
            <?php if ($this->session->flashdata('delete')) { ?>
               <div class="card-alert card red lighten-5">
                  <div class="card-content red-text">
                     <p>Deleted : contacts Has Been deleted</p>
                  </div>
                  <button type="button" class="close red-text" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">×</span>
                  </button>
               </div>
            <?php } ?>
            <!-- users list start -->
            <section class="users-list-wrapper section">
               <div class="users-list-table">
                  <div class="card">
                     <div class="card-content">
                        <!-- datatable start -->
                        <!-- <a class="btn waves-effect submit border-round waves-light mb-4 right" href="<?php echo base_url() ?>admin/addadmin"><i class="material-icons">add</i> Add admin</a> -->
                        <div class="responsive-table">
                           <table id="users-list-datatable" class="table">
                              <thead>
                                 <tr>
                                    <th>id</th>
                                    <th>name</th>
                                    <th>email</th>
                                    <th>phone</th>
                                    <th>subject</th>
                                    <th>massage</th>
                                    <th>status</th>
                                    <th>Action</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php foreach ($contacts as $contact) : ?>
                                    <tr>
                                       <td><?php echo $contact['contact_us_id']; ?></td>
                                       <td><?php echo $contact['contact_us_name']; ?></td>
                                       <td style="text-transform: lowercase!important;"><?php echo $contact['contact_us_email']; ?></td>
                                       <td><?php echo $contact['contact_us_phone']; ?></td>
                                       <td><?php echo $contact['contact_us_subject']; ?></td>
                                       <td><?php echo $contact['contact_us_massage']; ?></td>
                                       <td><?php echo $contact['contact_us_status']; ?></td>
                                       <?php if ($contact['contact_us_status'] == 'solved') { ?>
                                          <td><a class="tooltipped" data-position="bottom" data-tooltip="Already Solved"><i class="material-icons">settings_applications</i></a></td>
                                       <?php } else { ?>
                                          <td><a class="tooltipped" data-position="bottom" data-tooltip="Resolve" style="cursor: pointer;" href="<?php echo base_url(); ?>admin/resolve/<?php echo $contact['contact_us_id']; ?>"><i class="material-icons">settings_applications</i></a></td>

                                       <?php } ?>
                                    </tr>
                                 <?php endforeach; ?>

                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
</div>

<script>
   function enableadmin(adminid) {
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/ajax_admin_enable/" + adminid,
         success: function(data) {
            location.reload();


         }
      });
   }
</script>
<script>
   function disableadmin(adminid) {
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/ajax_admin_disable/" + adminid,
         success: function(data) {
            location.reload();




         }
      });
   }
</script>

<script>
   function del(id) {

      swal({
         title: "Are you sure?",
         icon: 'warning',
         dangerMode: true,
         buttons: {
            cancel: 'No',
            delete: 'Yes'
         }
      }).then(function(willDelete) {
         if (willDelete) {
            var html = $.ajax({
               type: "GET",
               url: "<?php echo base_url(); ?>admin/deleteadmin/" + id,
               // data: info,
               async: false
            }).responseText;

            if (html == "success") {
               $("delete").html("delete success.");
               return true;

            }
            swal("Your record has been deleted!", {
               icon: "success",
            });
            setTimeout(location.reload.bind(location), 1000);
         } else {
            swal("", {
               title: 'Cancelled',
               icon: "error",
            });
         }
      });


   }
</script>
<div id="main">
    <div class="row">
        <div id="breadcrumbs-wrapper" data-image="<?php echo base_url(); ?>assets/breadcrumb-bg.jpg">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0">
                            <span<span style="font-weight: bold;color: white;">View User</span>
                        </h5>
                    </div>
                    <div class="col s12 m6 l6 right-align-md">
                        <ol class="breadcrumbs mt-0 mb-0">
                            <li class="breadcrumb-item"><a style="color: white;" href="<?php echo base_url(); ?>admin/index">Home</a>
                            </li>

                            <li class="breadcrumb-item"><a style="color: white;" href="<?php echo base_url(); ?>admin/users">Manage User</a>
                            </li>
                            <li style="color: white;" class="breadcrumb-item active">View User
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">

                <!-- users view start -->
                <div class="section users-view">
                    <!-- users view media object start -->
                    <div class="card-panel">
                        <?php if ($this->session->flashdata('registered')) : ?>
                            <div class="alert alert-success alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success!</strong> User Successfully Registered .
                            </div>
                        <?php endif; ?>
                        <?php if ($this->session->flashdata('updated')) : ?>
                            <div class="alert alert-success alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Updated!</strong> user has been Successfully updated .
                            </div>
                        <?php endif; ?>
                        <div class="row">
                            <div class="col s12 m7">
                                <div class="display-flex media">
                                    <a href="#" class="avatar">
                                    <img src="<?php echo base_url(); ?>assets/uploads/no_image.jpg" alt="avatar" width="100px">
                                    </a>
                                    <div class="media-body">
                                        <h6 class="media-heading">
                                            <span class="capitalize"><?php echo $user['first_name']; ?></span><br>
                                            <span class="grey-text"><?php echo $user['user_email']; ?></span>
                                        </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-content">
                            <div id="information">
                                <div class="row">
                                    <div class="col s6">
                                        <table class="striped">
                                            <tbody>
                                                <tr>
                                                    <td class="black-text">First Name</td>
                                                    <td class="capitalize"><?php echo $user['first_name']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="black-text">Email</td>
                                                    <td style="text-transform: lowercase!important;"><?php echo $user['user_email']; ?></td>
                                                </tr>

                                                <tr>
                                                    <td class="black-text">Country</td>
                                                    <td class="capitalize"><?php echo $user['country_name']; ?></td>
                                                </tr>


                                                <tr>
                                                    <td class="black-text">Registration Date:</td>
                                                    <td class="capitalize"><?php echo $user['user_registration_date']; ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col s6">
                                        <table class="striped">
                                            <tbody>
                                                <tr>
                                                    <td class="black-text">Last Name</td>
                                                    <td class="capitalize"><?php echo $user['last_name']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="black-text">genders</td>
                                                    <td class="capitalize"><?php echo $user['user_gender']; ?></td>
                                                </tr>


                                                <!-- <tr>
                                                    <td class="black-text">City</td>
                                                    <td class="capitalize"><?php echo $user['user_city_name']; ?></td>
                                                </tr> -->

                                                <tr>
                                                    <td class="black-text">Status</td>
                                                    <td class="capitalize"><?php echo $user['user_status']; ?></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12">
                                        <h6 class="mb-2 mt-2"><i class="material-icons">note_add</i>User Courses</h6>
                                        <table class="striped">
                                            <thead>
                                                <tr>
                                                    <th>Course Title</th>
                                                    <th>Course Time</th>
                                                    <th>Price :</th>
                                                    <th>Instructor Name</th>
                                                    <th>Total Classes</th>
                                                    <th>Language</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($courses as $course) : ?>
                                                    <tr>
                                                        <th><?php echo $course['course_name']; ?></th>
                                                        <th><?php echo $course['course_start_date']; ?></th>
                                                        <td><?php echo $course['course_price']; ?></td>
                                                        <td><?php echo $course['course_instructor_name']; ?></td>
                                                        <td><?php echo $course['course_total_students']; ?></td>
                                                        <td><?php echo $course['course_language']; ?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12">
                                        <h6 class="mb-2 mt-2"><i class="material-icons">note</i>Enroll Courses</h6>
                                        <table class="striped">
                                            <thead>
                                                <tr>
                                                    <th>Course Title</th>
                                                    <th>Course Start</th>
                                                    <th>Price :</th>
                                                    <th>Instructor Name</th>
                                                    <th>Total Classes</th>
                                                    <th>Language</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($courses as $course) : ?>
                                                    <tr>
                                                        <th><?php echo $course['course_name']; ?></th>
                                                        <th><?php echo $course['course_start_date']; ?></span></th>
                                                        <td><?php echo $course['course_price']; ?></td>
                                                        <td><?php echo $course['course_instructor_name']; ?></td>
                                                        <td><?php echo $course['course_total_students']; ?></td>
                                                        <td><?php echo $course['course_language']; ?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
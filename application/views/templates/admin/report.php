<div id="main">
    <div class="row">
        <div id="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0">
                            <span<span style="font-weight: bold;">Report</span>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <!-- users list start -->
                <section class="users-list-wrapper section">
                    <div class="users-list-table">
                        <div class="card">
                            <div class="card-content">
                                <!-- datatable start -->
                                <div class="responsive-table">
                                    <table id="page-length-option" class="display">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date & Time</th>
                                                <th>client name</th>
                                                <th>Order Status</th>
                                                <th>Action</th>
                                                <th>Edit</th>
                                                <th>View Order</th>
                                                <th>Reorder</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $s_no = '1';
                                            foreach ($orders as $order) : ?>
                                                <tr>
                                                    <td><?php echo $s_no ?></td>
                                                    <td><?php echo $order['date']; ?> & <?php echo $order['time']; ?></td>
                                                    <td><?php echo $order['order_user_id']; ?></td>
                                                    <td> <?php echo $order['order_status']; ?></td>
                                                    <td>
                                                        <a class='dropdown dropdown-trigger mt-2 mb-2 mr-1 mb-1' data-target='dropdown1<?php echo $order['order_id']; ?>'><i class="material-icons">settings</i></a>

                                                        <ul id='dropdown1<?php echo $order['order_id']; ?>' class='dropdown-content'>
                                                            <li>
                                                                <a href="<?php echo base_url(); ?>admin/approved/<?php echo $order['order_id']; ?>" type="submit" name="action">Approved
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="<?php echo base_url(); ?>admin/rejected/<?php echo $order['order_id']; ?>" name="action">Rejected
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="<?php echo base_url(); ?>admin/cancelled/<?php echo $order['order_id']; ?>">Cancelled
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="<?php echo base_url(); ?>admin/readyfordelivery/<?php echo $order['order_id']; ?>">Ready For Delivery
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="<?php echo base_url(); ?>admin/delivered/<?php echo $order['order_id']; ?>">Delivered
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </td>
                                                    <td class="center"><a href="<?php echo base_url(); ?>admin/edit/<?php echo $order['order_id']; ?>"><i class="material-icons">edit</i></a></td>
                                                    <td class="center"><a href="<?php echo base_url(); ?>admin/vieworder/<?php echo $order['order_id']; ?>"><i class="material-icons">remove_red_eye</i></a></td>
                                                    <td class="center"><a href="<?php echo base_url(); ?>admin/reorder/<?php echo $order['order_id']; ?>"><i class="material-icons">repeat</i></a></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
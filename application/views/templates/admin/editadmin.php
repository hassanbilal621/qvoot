<div id="main">
    <div class="row">
        <div id="breadcrumbs-wrapper">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0">
                            <span<span style="font-weight: bold;">Edit Admin</span>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12">
        <div class="container">
            <div class="section users-edit">
                <div class="card">
                    <div class="card-content">

                        <div class="row">
                            <div class="col s12">
                                <!-- users edit media object start -->
                                <div class="media display-flex align-items-center mb-2">
                                    <a class="mr-2" href="#">
                                        <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $admin['profile_image']; ?>" alt="admin avatar" class=" circle" height="64" width="64">
                                    </a>
                                    <div class="media-body">
                                        <h5 class="capitalize media-heading mt-0"><?php echo $admin['name']; ?></h5>
                                        <div class="user-edit-btns display-flex">
                                            <?php echo form_open_multipart('admin/adminimage'); ?>
                                            <div class="row">
                                                <input type="file" name="userfile" required accept="image/*">
                                                <input type="hidden" name="admin_id" value="<?php echo $admin['admin_id']; ?>">
                                                <button type="submit" class="btn submit">Change</button>
                                            </div>
                                            <?php echo form_close(); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php echo form_open('admin/updateadmin') ?>
                                <div id="information">
                                    <div class="row">
                                        <div class="col s12 m6 l6">
                                            <div class="row">
                                                <div class="col s12 input-field">
                                                    <input id="username" name="name" type="text" class="validate" value="<?php echo $admin['name']; ?>" data-error=".errorTxt1">
                                                    <input type="hidden" name="admin_id" value="<?php echo $admin['admin_id']; ?>">
                                                    <label for="username">Username</label>
                                                    <small class="errorTxt1"></small>
                                                </div>
                                                <div class="col s12 input-field">
                                                    <input id="mobile" name="mobile" type="number" class="validate" value="<?php echo $admin['mobile']; ?>" data-error=".errorTxt2">
                                                    <label for="mobile">Mobile</label>
                                                    <small class="errorTxt2"></small>
                                                </div>
                                                <!-- <div class="col s12 input-field">
                                                    <label for="" class="active">Select Role</label>
                                                    <select name="zipcode" class="select2  browser-default">
                                                        <?php if ($admin['role'] == '1') { ?>
                                                            <option value="2">Admin</option>
                                                            <option selected value="1">Super admin</option>
                                                        <?php   } elseif ($admin['role'] == '2') { ?>
                                                            <option value="1">Super admin</option>
                                                            <option selected value="2">Admin</option>
                                                        <?php } else { ?>
                                                            <option value="1">Super admin</option>
                                                            <option value="2">Admin</option>
                                                        <?php    } ?>
                                                    </select>
                                                </div> -->

                                                <div class="col s12 input-field">
                                                    <label for="" class="active">Select City</label>
                                                    <select name="city" class="select2  browser-default" id="city">
                                                        <?php foreach ($cities as $city) : ?>

                                                            <?php if ($admin['city'] === $city['city_id']) {
                                                            ?>
                                                                <option selected value="<?php echo $city['city_id']; ?>"><?php echo $city['city_name']; ?></option>
                                                            <?php
                                                            } else {
                                                            ?>
                                                                <option value="<?php echo $city['city_id']; ?>"><?php echo $city['city_name']; ?></option>
                                                            <?php } ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="col s12 input-field">
                                                    <input id="state" name="state" type="text" class="validate" value="<?php echo $admin['state']; ?>" data-error=".errorTxt3">
                                                    <label for="state">State</label>
                                                    <small class="errorTxt3"></small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col s12 m6 l6">
                                            <div class="row">
                                                <div class="col s12 input-field">
                                                    <input id="email" name="email" type="email" class="validate" value="<?php echo $admin['email']; ?>" data-error=".errorTxt2" readonly>
                                                    <label for="email">Email</label>
                                                    <small class="errorTxt2"></small>
                                                </div>
                                                <div class="col s12 input-field">
                                                    <label for="" class="active">Select Country</label>
                                                    <select name="country" class="select2 browser-default" onchange="getcity(this.value)">>
                                                        <?php foreach ($countries as $country) : ?>
                                                            <?php

                                                            if ($admin['country'] == $country['country_name']) {
                                                            ?>
                                                                <option selected value="<?php echo $country['country_name']; ?>"><?php echo $country['country_name']; ?></option>
                                                            <?php
                                                            } else {
                                                            ?>
                                                                <option value="<?php echo $country['country_name']; ?>"><?php echo $country['country_name']; ?></option>
                                                            <?php } ?>
                                                        <?php endforeach; ?>

                                                    </select>
                                                </div>
                                                <div class="col s12 input-field">
                                                    <label for="" class="active">Select Zip Code</label>
                                                    <select name="zipcode" class="select2  browser-default">
                                                        <?php foreach ($zipcodes as $zipcode) : ?>
                                                            <?php if ($admin['zipcode_admin_id'] == $zipcode['zipcode_id']) {
                                                            ?>
                                                                <option selected value="<?php echo $zipcode['zipcode_id']; ?>"><?php echo $zipcode['zipcode']; ?></option>
                                                            <?php
                                                            } else {
                                                            ?>
                                                                <option value="<?php echo $zipcode['zipcode_id']; ?>"><?php echo $zipcode['zipcode']; ?></option>
                                                            <?php } ?>
                                                        <?php endforeach; ?>

                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12 display-flex justify-content-end mt-3">
                                    <button type="submit" class="btn submit mr-2 ">Save changes</button>
                                    <a href="<?php echo base_url(); ?>admin/profile" class="btn delete btn-light">Cancel</a>
                                </div>
                               <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function getcity(country) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/get_cities_by_countryid/" + country,
            data: 'country_name=' + country,
            success: function(data) {
                $("#city").html(data);
            }
        });
    }
</script>
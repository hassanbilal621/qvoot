<div id="main">
    <div class="row">
        <div class="col s12">

            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                <div class="col s12 m6">
                                    <div class=" sidebar-feature">
                                        <h5><b>Course Dtails</b></h5>

                                        <table>
                                            <tbody>
                                                <tr>
                                                    <th>Course Name</th>
                                                    <td colspan="2"><input type="text" name="project_name" value="<?php echo $course['course_name']; ?>"></td>
                                                </tr>
                                                <tr>
                                                    <th>Instructor Name </th>
                                                    <td colspan="2"><input type="text" name="description" value="<?php echo $course['course_instructor_name']; ?>"></td>
                                                </tr>
                                                <tr>
                                                    <th>Capacity </th>
                                                    <td colspan="2"><input type="text" name="description" value="<?php echo $course['course_total_students']; ?>"></td>
                                                </tr>
                                                <tr>
                                                    <th>Category </th>
                                                    <td colspan="2"><input type="text" name="description" value="<?php echo $course['category_name']; ?>"></td>
                                                </tr>
                                                <tr>
                                                    <th>Sub Category </th>
                                                    <td colspan="2"><input type="text" name="description" value="<?php echo $course['category_name']; ?>"></td>
                                                </tr>
                                                <tr>
                                                    <th>Language </th>
                                                    <td colspan="2"><input type="text" name="description" value="<?php echo $course['course_language']; ?>"></td>
                                                </tr>
                                                <tr>
                                                    <th>Start Time </th>
                                                    <td colspan="2"><input type="date" name="description" value="<?php echo $course['course_start_date']; ?>"></td>
                                                </tr>



                                            </tbody>
                                        </table>

                                    </div>
                                </div>


                                <div class="col s12 m6">
                                    <center>
                                        <h5><b>Course Pictures</b></h5>
                                    </center>
                                    <div class="carousel">


                                        <?php foreach ($courses_pictures as $courses_picture) : ?>
                                            <div>
                                                <a class="carousel-item" href="#one!"><img data-u="image" src="<?php echo base_url(); ?>assets/uploads/<?php if (isset($courses_picture['course_picture'])) {
                                                                                                                                                            echo $courses_picture['course_picture'];
                                                                                                                                                        } else {
                                                                                                                                                            echo "no_image.jpg";
                                                                                                                                                        }  ?>" />
                                                </a>
                                            </div>
                                        <?php endforeach; ?>

                                    </div>

                                </div>


                                <div class="col s12">
                                    <div class="course-detail-content-wrap margin-top-100px">
                                        <?php if (isset($course_fors)) { ?>
                                            <div class="post-overview-card margin-bottom-50px">
                                                <h5 class="widget-title">Who this course is for:</h5>
                                                <ul class="list-items mt-3">
                                                    <?php foreach ($course_fors as $course_for) : ?>
                                                        <li><i class="la la-check-circle"></i> <?php echo $course_for['course_for']; ?></li>

                                                    <?php endforeach; ?>

                                                </ul>
                                            </div>
                                        <?php   } ?>
                                        <?php if (isset($course_requirements)) { ?>
                                            <div class="requirement-wrap margin-bottom-40px">
                                                <h5 class="widget-title">Requirements</h5>
                                                <ul class="list-items mt-3">
                                                    <?php foreach ($course_requirements as $course_requirement) : ?>
                                                        <li><?php echo $course_requirement['course_requirements']; ?></li>

                                                    <?php endforeach; ?>

                                                </ul>
                                            </div>
                                        <?php   } ?>
                                        <div class="description-wrap margin-bottom-40px">
                                            <h5 class="widget-title">Description</h5>
                                            <p class="mb-2 mt-3">
                                                <?php echo $course['course_full_desc']; ?></p>
                                        </div>
                                        <div class="section-block"></div>

                                    </div><!-- end course-detail-content-wrap -->
                                </div>


                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function() {
        $('.carousel').carousel();
    });
</script>
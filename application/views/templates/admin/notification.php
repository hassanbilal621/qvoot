<div id="main">
    <div class="row">
        <div id="breadcrumbs-wrapper" data-image="<?php echo base_url(); ?>/assets/app-assets/images/background/red.jpg" style="height: 64px;">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>notification</span></h5>
                    </div>
                    <div class="col s12 m6 l6 right-align-md">
                        <ol class="breadcrumbs mb-0">
                            <li class="breadcrumb-item"><a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item active">notification
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <div class="section users-view">
                    <div class="card">
                        <div class="card-content">
                            <h6 class="mb-2 mt-2"><i class="material-icons">notifications_none</i> Notification</h6>
                            <div class="row">
                                <div class="col s12">

                                    <table class="striped">
                                        <tbody>
                                            <?php if(isset($notifications)) {
                                            ?>
                                                <?php foreach ($notifications as $notification) : ?>
                                                    <tr onclick="loadnotification(this.id)" id="<?php echo $notification['notification_id']; ?>">

                                                        <td class="capitalize"><?php echo $notification['notificaton_title']; ?></td>
                                                        <td class="capitalize"><?php echo $notification['notificaton']; ?></td>

                                                    </tr>

                                                <?php endforeach; ?>
                                            <?php } else { ?>
                                                <h6>No Unread Notificaton</h6>
                                            <?php } ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal3" class="modal">
    <div class="modal-content">
    </div>
</div>
<script>
    function loadnotification(notificationid) {
        // var userid = this.id;
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/ajax_notification_details/" + notificationid,
            success: function(data) {
                $(".modal-content").html(data);
                $('#modal3').modal('open');



            }
        });
    }
</script>
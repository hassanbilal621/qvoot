<div id="main">
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen-->
        <div class="container">
            <div class="row">
                <div class="col s12 m6 l6">
                    <h5 class="breadcrumbs-title mt-0 mb-0">
                        <span<span style="font-weight: bold;">Edit Profile</span>
                    </h5>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col s12">
            <div class="container">
                <div class="section users-edit">
                    <div class="card">
                        <div class="card-content">
                            <?php if ($this->session->flashdata('update')) : ?>
                                <div class="card-alert card red">
                                    <div class="card-content white-text">
                                        <span class="card-title white-text darken-1">
                                            <span class="card-title white-text darken-1">fill your all field</span>
                                            <p>fill your all field.</p>
                                    </div>
                                    <button type=" button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                        <span id="closeicon" aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <p> <?php echo $this->session->flashdata('update'); ?></p>
                            <?php endif; ?>

                            <div class="row">
                                <div class="col s12">
                                    <!-- users edit media object start -->
                                    <div class="media display-flex align-items-center mb-2">
                                        <a class="mr-2" href="#">
                                            <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $profile['profile_image']; ?>" alt="users avatar" class="z-depth-4 circle" height="64" width="64">
                                        </a>
                                        <div class="media-body">
                                            <h5 class="capitalize media-heading mt-0"><?php echo $profile['name']; ?></h5>
                                            <div class="user-edit-btns display-flex">
                                                <?php echo form_open_multipart('users/profileimage'); ?>
                                                <div class="row">
                                                    <input required type="file" name="userfile" required>
                                                    <input required type="hidden" name="user_id" value="<?php echo $profile['user_id']; ?>">
                                                    <button type="submit" class="btn submit">Change</button>
                                                </div>
                                                <?php echo form_close(); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="tabs mb-2 row">
                                        <li class="tab">
                                            <a class="display-flex align-items-center" id="information-tab" href="#information">
                                                <i class="material-icons mr-2">error_outline</i><span>Information</span>
                                            </a>
                                        </li>
                                        <li class="tab">
                                            <a class="display-flex align-items-center" id="address-tab" href="#address">
                                                <i class="material-icons mr-2">place</i><span> Delivery Address</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <?php echo form_open('admin/updateprofile') ?>
                                    <div id="information">
                                        <div class="row">
                                            <div class="col s12 m6 l6">
                                                <div class="row">
                                                    <div class="col s12 input-field">
                                                        <input required id="username" name="name" type="text" class="validate" value="<?php echo $profile['name']; ?>" data-error=".errorTxt1">
                                                        <input required type="hidden" name="user_id" value="<?php echo $profile['user_id']; ?>">
                                                        <label for="username">Username</label>
                                                        <small class="errorTxt1"></small>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <input required id="credit-demo" name="credit_card" type="text" class="validate" value="<?php echo $profile['credit_card']; ?>" data-error=".errorTxt3">
                                                        <label for="credit_card">Credit Card</label>
                                                        <small class="errorTxt3"></small>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <input required id="mobile" name="mobile" type="number" class="validate" value="<?php echo $profile['mobile']; ?>" data-error=".errorTxt2">
                                                        <label for="mobile">Mobile</label>
                                                        <small class="errorTxt2"></small>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <input required id="name_of_organization" name="name_of_organization" type="text" class="validate" value="<?php echo $profile['name_of_organization']; ?>" data-error=".errorTxt1">
                                                        <label for="name_of_organization">Name Of Organization</label>
                                                        <small class="errorTxt1"></small>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <label for="desert" class="active">Diet preference<span>(Press ctrl Key to select multiple Desert)</span></label>
                                                        <select class="select2 browser-default mdb-select" name="diet_id[]" multiple="multiple" required>
                                                            <?php foreach ($diets as $diet) : ?>
                                                                <?php
                                                                $selected = 0;
                                                                foreach ($diet_preferences as $diet_preference) :
                                                                    if ($diet['diet_id'] == $diet_preference['diet_preference']) {
                                                                        $selected = 1;
                                                                    }


                                                                endforeach; ?>

                                                                <option <?php if ($selected == 1) {
                                                                            echo "selected";
                                                                        } ?> value="<?php echo $diet['diet_id']; ?>"><?php echo $diet['diet_name']; ?></option>


                                                            <?php $selected = 0;

                                                            endforeach; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <input required id="allergies" name="allergies" type="text" class="validate" value="<?php echo $profile['allergies']; ?>" data-error=".errorTxt3">
                                                        <label for="allergies">Allergies</label>
                                                        <small class="errorTxt3"></small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col s12 m6 l6">
                                                <div class="row">
                                                    <div class="col s12 input-field">
                                                        <input required id="email" name="email" type="email" class="validate" value="<?php echo $profile['email']; ?>" data-error=".errorTxt2" readonly>
                                                        <label for="email">Email</label>
                                                        <small class="errorTxt2"></small>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <input required id="cantact_name" name="cantact_name" type="text" class="validate" value="<?php echo $profile['cantact_name']; ?>" data-error=".errorTxt1">
                                                        <label for="cantact_name">cantact name</label>
                                                        <small class="errorTxt1"></small>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <input required id="phone_num" name="phone_num" type="number" class="validate" value="<?php echo $profile['phone_num']; ?>" data-error=".errorTxt3">
                                                        <label for="phone_num">Phone No</label>
                                                        <small class="errorTxt3"></small>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <input required id="account_note" name="account_note" type="text" class="validate" value="<?php echo $profile['account_note']; ?>" data-error=".errorTxt3">
                                                        <label for="account_note">account note</label>
                                                        <small class="errorTxt3"></small>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <input required id="budget_preference" name="budget_preference" type="number" class="validate" value="<?php echo $profile['budget_preference']; ?>" data-error=".errorTxt3">
                                                        <label for="budget_preference">budget preference</label>
                                                        <small class="errorTxt3"></small>
                                                    </div>
                                                    <div class="col s12 input-field">
                                                        <input required id="day_Schedule" name="day_Schedule" type="text" class="validate" value="<?php echo $profile['day_Schedule']; ?>" data-error=".errorTxt3">
                                                        <label for="day_Schedule">day Schedule</label>
                                                        <small class="errorTxt3"></small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="address">
                                        <div class="row">
                                            <div class="col s12 m6 l6">
                                                <div class="col s12 input-field">
                                                    <label for="" class="active">Select Country</label>
                                                    <select name="country" class="select2 browser-default" onchange="getcity(this.value)">>
                                                        <?php foreach ($countries as $country) : ?>
                                                            <?php

                                                            if ($profile['country'] == $country['country_name']) {
                                                            ?>
                                                                <option selected value="<?php echo $country['country_name']; ?>"><?php echo $country['country_name']; ?></option>
                                                            <?php
                                                            } else {
                                                            ?>
                                                                <option value="<?php echo $country['country_name']; ?>"><?php echo $country['country_name']; ?></option>
                                                            <?php } ?>
                                                        <?php endforeach; ?>

                                                    </select>
                                                </div>
                                                <div class="col s12 input-field">
                                                <label for="" class="active">Select City</label>
                                                    <select name="city" class="select2  browser-default" id="city">
                                                        <?php foreach ($cities as $city) : ?>

                                                            <?php if ($profile['city'] === $city['city_id']) {
                                                            ?>
                                                                <option selected value="<?php echo $city['city_id']; ?>"><?php echo $city['city_name']; ?></option>
                                                            <?php
                                                            } else {
                                                            ?>
                                                                <option value="<?php echo $city['city_id']; ?>"><?php echo $city['city_name']; ?></option>
                                                            <?php } ?>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col s12 m6 l6">
                                                <div class="col s12 input-field">
                                                <label for="" class="active">Select Zip Code</label>
                                                    <select name="zipcode_user_id" class="select2  browser-default">
                                                        <?php foreach ($zipcodes as $zipcode) : ?>
                                                            <?php if ($profile['zipcode_user_id'] == $zipcode['zipcode_id']) {
                                                            ?>
                                                                <option selected value="<?php echo $zipcode['zipcode_id']; ?>"><?php echo $zipcode['zipcode']; ?></option>
                                                            <?php
                                                            } else {
                                                            ?>
                                                                <option value="<?php echo $zipcode['zipcode_id']; ?>"><?php echo $zipcode['zipcode']; ?></option>
                                                            <?php } ?>
                                                        <?php endforeach; ?>

                                                    </select>
                                                </div>
                                                <div class="col s12 input-field">
                                                    <input required id="state" name="state" type="text" class="validate" value="<?php echo $profile['state']; ?>" data-error=".errorTxt3">
                                                    <label for="state">State</label>
                                                    <small class="errorTxt3"></small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col s12">
                                                <div class="col s12 input-field">
                                                    <input required id="delivery_instruction" name="delivery_instruction" type="text" class="validate" value="<?php echo $profile['delivery_instruction']; ?>" data-error=".errorTxt1">
                                                    <label for="delivery_instruction">delivery instruction</label>
                                                    <small class="errorTxt1"></small>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col s12 display-flex justify-content-end mt-3">
                                        <button type="submit" class="btn submit mr-2 ">Save changes</button>
                                        <a href="<?php echo base_url(); ?>users/profile" class="btn delete btn-light">Cancel</a>
                                    </div>
                                   <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function getcity(country) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>admin/get_cities_by_countryid/" + country,
            data: 'country_name=' + country,
            success: function(data) {
                $("#city").html(data);
            }
        });
    }
</script>
<script>
    setInterval(function() {
        document.getElementById("closeicon").click();

    }, 5000);
</script>
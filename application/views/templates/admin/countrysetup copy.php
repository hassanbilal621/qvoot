<div id="main">
	<div class="row">
		<div id="breadcrumbs-wrapper" data-image="<?php echo base_url(); ?>/assets/app-assets/images/background/red.jpg" style="height: 64px;">
			<!-- Search for small screen-->
			<div class="container">
				<div class="row">
					<div class="col s12 m6 l6">
						<h5 class="breadcrumbs-title mt-0 mb-0"><span>Zip Code Management</span></h5>
					</div>
					<div class="col s12 m6 l6 right-align-md">
						<ol class="breadcrumbs mb-0">
							<li class="breadcrumb-item"><a href="#">Home</a>
							</li>
							<li class="breadcrumb-item active">Zip Code
							</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
		<div class="col s12">
			<div class="section" id="materialize-sparkline">
				<div class="section">
					<div class="card">
						<div class="card-content">
							<?php echo form_open('admin/country'); ?>
							<h4 class="card-title">Add Country</h4>
							<div class="row">
								<div class="input-field col s12">
									<input type="text" name="country_name" placeholder="Type Country" required>
									<button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Save Country
										<i class="material-icons right">send</i>
									</button>
								</div>
							</div>
							<?php echo form_close(); ?>
							<?php echo form_open('admin/city'); ?>
							<h4 class="card-title">Add City</h4>
							<div class="row">
								<div class="input-field col s6">
									<select name="country_city_id" required>
										<?php foreach ($countries as $country) : ?>
											<option value="<?php echo $country['country_id']; ?>"><?php echo $country['country_name']; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="input-field col s6">
									<input type="text" name="city_name" placeholder="Select Country First Then Type city" required>
								</div>
								<button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">save City
									<i class="material-icons right">send</i>
								</button>
							</div>
							<?php echo form_close(); ?>
							<?php echo form_open('admin/states'); ?>
							<h4 class="card-title">Add State</h4>
							<div class="row">
								<div class="input-field col s4">
									<select name="" id="" required>
										<?php foreach ($countries as $country) : ?>
											<option value="<?php echo $country['country_id']; ?>"><?php echo $country['country_name']; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="input-field col s4">
									<select name="city_state_id" id="" required>
										<?php foreach ($cities as $city) : ?>
											<option value="<?php echo $city['city_id']; ?>"><?php echo $city['city_name']; ?></option>
										<?php endforeach; ?>
									</select>
								</div>

								<div class="input-field col s4">
									<input type="text" name="state_name" placeholder="Select Country & city First Then Type state" required>
								</div>
								<button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Save State
									<i class="material-icons right">send</i>
								</button>
							</div>
							<?php echo form_close(); ?>
							<?php echo form_open('admin/zipcode'); ?>
							<h4 class="card-title">Add Zipode</h4>
							<div class="row">
								<div class="input-field col s3">
									<select name="" id="" required>
										<?php foreach ($countries as $country) : ?>
											<option value="<?php echo $country['country_id']; ?>"><?php echo $country['country_name']; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="input-field col s3">
									<select name="city_state_id" id="" required>
										<?php foreach ($cities as $city) : ?>
											<option value="<?php echo $city['city_id']; ?>"><?php echo $city['city_name']; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="input-field col s3">
									<select name="city_state_id" id="" required>
										<?php foreach ($cities as $city) : ?>
											<option value="<?php echo $city['city_id']; ?>"><?php echo $city['city_name']; ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="input-field col s3">
									<input type="number" name="zipcode" placeholder="Type Zipode" required>
								</div>
								<button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Save Zipode
									<i class="material-icons right">send</i>
								</button>
							</div>
						</div>
						<?php echo form_close(); ?>
						<div class="row">
							<div class="col s12">

								<section class="users-list-wrapper section">
									<div class="users-list-filter">
										<div class="card-panel">
											<div class="row">
												<form>
													<div class="col s12 m6 l3">
														<label for="users-list-verified">Verified</label>
														<div class="input-field">
															<select class="form-control" id="users-list-verified">
																<option value="">Any</option>
																<option value="Yes">Yes</option>
																<option value="No">No</option>
															</select>
														</div>
													</div>
													<div class="col s12 m6 l3">
														<label for="users-list-role">Role</label>
														<div class="input-field">
															<select class="form-control" id="users-list-role">
																<option value="">Any</option>
																<option value="User">User</option>
																<option value="Staff">Staff</option>
															</select>
														</div>
													</div>
													<div class="col s12 m6 l3">
														<label for="users-list-status">Status</label>
														<div class="input-field">
															<select class="form-control" id="users-list-status">
																<option value="">Any</option>
																<option value="Active">Active</option>
																<option value="Close">Close</option>
																<option value="Banned">Banned</option>
															</select>
														</div>
													</div>
													<div class="col s12 m6 l3 display-flex align-items-center show-btn">
														<button type="submit" class="btn btn-block indigo waves-effect waves-light">Show</button>
													</div>
												</form>
											</div>
										</div>
									</div>
									<div class="users-list-table">
										<div class="card">
											<div class="card-content">
												<!-- datatable start -->
												<div class="responsive-table">
													<table id="users-list-datatable" class="table">
														<thead>
															<tr>
																<th></th>
																<th>id</th>
																<th>Country Name</th>
																<th>City Name</th>
																<th>Stats Name</th>
																<th>Zipcodes</th>
																<th>Edit</th>
																<th>view</th>
																<th>Delete</th>
															</tr>
														</thead>
														<tbody>
															<?php $s_no = '1';
															foreach ($countrysetups as $countrysetup) : ?>
																<tr>
																	<td></td>
																	<td><?php echo $s_no ?></td>
																	<td><?php echo $countrysetup['country_name']; ?></td>
																	<td><?php echo $countrysetup['city_name']; ?></td>
																	<td><?php echo $countrysetup['state_name']; ?></td>
																	<td>No</td>
																	<td>Staff</td>
																	<td><span class="chip green lighten-5">
																			<span class="green-text">Active</span>
																		</span>
																	</td>
																	<td><a href="page-users-edit.html"><i class="material-icons">edit</i></a></td>
																	<td><a href="page-users-view.html"><i class="material-icons">remove_red_eye</i></a></td>
																</tr>
															<?php $s_no++;
															endforeach; ?>
														</tbody>
													</table>
												</div> <!-- datatable ends -->
											</div>
										</div>
									</div>
								</section>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>


</div>
<div id="modal4" class="modal">
	<div class="modal-content">
	</div>
</div>
<script>
	function loadcountryinfo(country_id) {
		// var userid = this.id;
		$.ajax({
			type: "GET",
			url: "<?php echo base_url(); ?>admin/ajax_edit_country/" + country_id,
			success: function(data) {
				$(".modal-content").html(data);
				$('#modal4').modal('open');
			}
		});
	}
</script>
<div id="modal1" class="modal">
	<div class="modal-content">
	</div>
</div>
<script>
	function loadcityinfo(city_id) {
		// var userid = this.id;
		$.ajax({
			type: "GET",
			url: "<?php echo base_url(); ?>admin/ajax_edit_city/" + city_id,
			success: function(data) {
				$(".modal-content").html(data);
				$('#modal1').modal('open');
			}
		});
	}
</script>
<div id="modal2" class="modal">
	<div class="modal-content">
	</div>
</div>
<script>
	function loadstateinfo(state_id) {
		// var userid = this.id;
		$.ajax({
			type: "GET",
			url: "<?php echo base_url(); ?>admin/ajax_edit_state/" + state_id,
			success: function(data) {
				$(".modal-content").html(data);
				$('#modal2').modal('open');
			}
		});
	}
</script>
<div id="modal3" class="modal">
	<div class="modal-content">
	</div>
</div>
<script>
	function loadzipcodeinfo(zipcode_id) {
		// var userid = this.id;
		$.ajax({
			type: "GET",
			url: "<?php echo base_url(); ?>admin/ajax_edit_zipcode/" + zipcode_id,
			success: function(data) {
				$(".modal-content").html(data);
				$('#modal3').modal('open');
			}
		});
	}
</script>
<div id="main">
   <div class="row">
   <div id="breadcrumbs-wrapper" data-image="<?php echo base_url(); ?>assets/breadcrumb-bg.jpg">

         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0">
                     <span<span style="font-weight: bold;color: white;">Manage Admin</span>
                  </h5>
               </div>
               <div class="col s12 m6 l6 right-align-md">
                  <ol class="breadcrumbs mt-0 mb-0">
                     <li class="breadcrumb-item"><a style="color: white;" href="<?php echo base_url();?>admin/index">Home</a>
                     </li>
                     <li style="color: white;" class="breadcrumb-item active">Manage Admin
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12">
         <div class="container">
            <?php if ($this->session->flashdata('delete')) { ?>
               <div class="card-alert card red lighten-5">
                  <div class="card-content red-text">
                     <p>Deleted : admin Has Been deleted</p>
                  </div>
                  <button type="button" class="close red-text" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">×</span>
                  </button>
               </div>
            <?php } ?>
            <!-- users list start -->
            <section class="users-list-wrapper section">
               <div class="users-list-table">
                  <div class="card">
                     <div class="card-content">
                        <!-- datatable start -->
                        <!-- <a class="btn waves-effect submit border-round waves-light mb-4 right" href="<?php echo base_url() ?>admin/addadmin"><i class="material-icons">add</i> Add admin</a> -->
                        <div class="responsive-table">
                           <table id="users-list-datatable" class="table">
                              <thead>
                                 <tr>
                                    <th>admin id</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Country</th>
                                    <th>city</th>
                                    <th>Status</th>
                                    <!-- <th>View</th> -->
                                    <!-- <th>edit</th>
                                    <th>Delete</th> -->
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php foreach ($admins as $admin) : ?>
                                    <tr>
                                       <td><?php echo $admin['admin_id']; ?></td>
                                       <td><?php echo $admin['admin_first_name']; ?></td>
                                       <td><?php echo $admin['admin_last_name']; ?></td>
                                       <td style="text-transform: lowercase!important;"><?php echo $admin['admin_email']; ?></td>
                                       <td><?php echo $admin['admin_mobile']; ?></td>
                                       <td><?php echo $admin['admin_country']; ?></td>
                                       <td><?php echo $admin['admin_city']; ?></td>
                                       <td><?php echo $admin['admin_status']; ?></td>
                                       <!-- <td><a class="tooltipped" data-position="bottom" data-tooltip="View Admin" href="<?php echo base_url(); ?>admin/view_admin/<?php echo $admin['admin_id']; ?>"><i class="material-icons">remove_red_eye</i></a></td> -->
                                       <!-- <td><a class="tooltipped" data-position="bottom" data-tooltip="Edit Admin" href="<?php echo base_url(); ?>admin/edit_admin/<?php echo $admin['admin_id']; ?>"><i class="material-icons">edit</i></a></td> -->
                                       <!-- <td><a class="tooltipped" data-position="bottom" data-tooltip="Delete Admin" href="<?php echo base_url(); ?>admin/delete_admin/<?php echo $admin['admin_id']; ?>"><i class="material-icons">delete</i></a></td> -->
                                    </tr>
                                 <?php endforeach; ?>

                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
</div>

<script>
   function enableadmin(adminid) {
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/ajax_admin_enable/" + adminid,
         success: function(data) {
            location.reload();


         }
      });
   }
</script>
<script>
   function disableadmin(adminid) {
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/ajax_admin_disable/" + adminid,
         success: function(data) {
            location.reload();




         }
      });
   }
</script>

<script>
   function del(id) {

      swal({
         title: "Are you sure?",
         icon: 'warning',
         dangerMode: true,
         buttons: {
            cancel: 'No',
            delete: 'Yes'
         }
      }).then(function(willDelete) {
         if (willDelete) {
            var html = $.ajax({
               type: "GET",
               url: "<?php echo base_url(); ?>admin/deleteadmin/" + id,
               // data: info,
               async: false
            }).responseText;

            if (html == "success") {
               $("delete").html("delete success.");
               return true;

            }
            swal("Your record has been deleted!", {
               icon: "success",
            });
            setTimeout(location.reload.bind(location), 1000);
         } else {
            swal("", {
               title: 'Cancelled',
               icon: "error",
            });
         }
      });


   }
</script>
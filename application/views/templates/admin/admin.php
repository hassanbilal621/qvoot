<div id="main">
   <div class="row">
      <div id="breadcrumbs-wrapper">
         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0">
                     <span<span style="font-weight: bold;">Profile</span>
                  </h5>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12">
         <div class="container">
            <!-- users view start -->
            <div class="section users-view">
               <!-- users view media object start -->
               <div class="card-panel">
                  <div class="row">
                     <div class="col s12 m7">
                        <div class="display-flex media">
                           <a href="#" class="avatar">
                              <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $admin['profile_image']; ?>" alt="Admin- Profile" class="circle" height="64" width="64">
                           </a>
                           <div class="media-body">
                              <h6 class="media-heading">
                                 <span class="capitalize"><?php echo $admin['name']; ?></span><br>
                                 <span class="capitalize grey-text"><?php echo $admin['email']; ?></span>
                              </h6>
                           </div>
                        </div>
                     </div>
                     <div class="col s12 m5 quick-action-btns display-flex justify-content-end align-items-center pt-2">
                        <a href="<?php echo base_url(); ?>admin/editadmin/<?php echo $admin['admin_id']; ?>" class="btn-small submit">Edit</a>
                     </div>
                  </div>
               </div>
               <!-- admin view media object ends -->
               <!-- admin view card data start -->
               <div class="card">
                  <div class="card-content">

                     <div id="information">
                        <h6 class="mb-2 mt-2"><i class="material-icons">error_outline</i> Personal Info</h6>
                        <div class="row">
                           <div class="col s6">
                              <table class="striped">
                                 <tbody>
                                    <tr>
                                       <td class="black-text">Name</td>
                                       <td class="capitalize"><?php echo $admin['name']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Email</td>
                                       <td class="capitalize"><?php echo $admin['email']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Mobile</td>
                                       <td class="capitalize"><?php echo $admin['mobile']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Registration Date:</td>
                                       <td class="capitalize"><?php echo $admin['registration_date']; ?></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                           <div class="col s6">
                              <table class="striped">
                                 <tbody>
                                    <tr>
                                       <td class="black-text">Country</td>
                                       <td class="capitalize"><?php echo $admin['country_name']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">City</td>
                                       <td class="capitalize"><?php echo $admin['city_name']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Zip Code</td>
                                       <td class="capitalize"><?php echo $admin['zipcode']; ?></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>


                     </div>

                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<div id="modal1" class="modal">
   <div class="modal-content">
   </div>
</div>
<script>
   function addaddress(admin_id) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/ajax_add_address_details/" + admin_id,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal1').modal('open');
         }
      });
   }
</script>

<div id="modal3" class="modal">
   <div class="modal-content">
   </div>
</div>
<script>
   function loadaddress(addressid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/ajax_edit_address_details/" + addressid,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal3').modal('open');
         }
      });
   }
</script>
<script>
   function del(id) {

      swal({
         title: "Are you sure?",
         text: "You will not be able to recover this imaginary file!",
         icon: 'warning',
         dangerMode: true,
         buttons: {
            cancel: 'No, Please!',
            delete: 'Yes, Delete It'
         }
      }).then(function(willDelete) {
         if (willDelete) {
            var html = $.ajax({
               type: "GET",
               url: "<?php echo base_url(); ?>admin/deleteaddress/" + id,
               // data: info,
               async: false
            }).responseText;

            if (html == "success") {
               $("#delete").html("delete success.");
               return true;

            }
            swal("Poof! Your record has been deleted!", {
               icon: "success",
            });
            setTimeout(location.reload.bind(location), 1000);
         } else {
            swal("Your imaginary file is safe", {
               title: 'Cancelled',
               icon: "error",
            });
         }
      });


   }
</script>
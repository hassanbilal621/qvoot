<div id="main">
   <div class="row">
      <div id="breadcrumbs-wrapper" data-image="<?php echo base_url(); ?>/assets/app-assets/images/background/red.jpg" style="height: 64px;">
         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0"><span>Zip Code Managemanegt</span></h5>
               </div>
               <div class="col s12 m6 l6 right-align-md">
                  <ol class="breadcrumbs mb-0">
                     <li class="breadcrumb-item"><a href="#">Home</a>
                     </li>
                     <li class="breadcrumb-item active">Zip Code Managemenet
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12">
         <div class="section" id="materialize-sparkline">
            <div class="section">
               <div class="card">
                  <div class="card-content">
                     <div class="row">
                        <h4 class="card-title">Add Zip Code</h4>
                        <?php echo form_open('admin/addexpencecatagoty'); ?>
                        <div class="col s12">
                           <div class="input-field col s6">
                              <input type="text" name="exp_cat" placeholder="Add Zip Code" required>
                           </div>

                           <button class="waves-effect waves-light btn submit z-depth-2 mt   -1 ml-1 right" id="view" type="submit" name="action">Submit
                              <i class="material-icons right">send</i>
                           </button>
                        </div>
                        <?php echo form_close(); ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col s12">
               <div class="card">
                  <div class="card-content">
                     <h4 class="card-title">Manage Expence Zip Code</h4>
                     <div class="row">
                        <table id="page-length-option" class="display">
                           <thead>
                              <tr>
                                 <th>Zip Code id</th>
                                 <th>Zip Code</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>01</td>
                                 <td>74405</td>
                                 </a>
                                 <td><a class="waves-effect waves-light   mr-1 mb-1" id="01" onclick="del(this.id)" type="submit" name="action">
                                       <i class="material-icons left">delete_forever</i>
                                    </a></td>
                                 <!--<td> 
                                                   <a class="waves-effect waves-light  btn gradient-45deg-green-teal box-shadow-none border-round mr-1 mb-1" onclick="active()" href="<?php echo base_url(); ?>admin/activestatus/01" type="submit" name="action">Active
                                                      <i class="material-icons left">done</i>
                                                
                                                   <a class="waves-effect waves-light  btn red darken-4 box-shadow-none border-round mr-1 mb-1" onclick="deactive()" href="<?php echo base_url(); ?>admin/deactivestatus/01" type="submit" name="action">Deactive
                                                      <i class="material-icons left">cancel</i>
                                                   </a>

                                                <button class="waves-effect waves-light  btn edit box-shadow-none border-round mr-1 mb-1 modal-trigger" onclick="loadZip Codeinfo(this.id)" id="01" type="submit" href="#modal3" name="action">EDIT
                                                   <i class="material-icons left">edit</i>
                                                </button>
                                                <a class="waves-effect waves-light  btn delete box-shadow-none border-round mr-1 mb-1" id="01" onclick="del(this.id)" type="submit" name="action">DELETE
                                                   <i class="material-icons left">delete_forever</i>
                                                </a>

                                             </td> -->
                              </tr>
                              <tr>
                                 <td>02</td>
                                 <td>74406</td>
                                 </a>
                                 <td><a class="waves-effect waves-light   mr-1 mb-1" id="01" onclick="del(this.id)" type="submit" name="action">
                                       <i class="material-icons left">delete_forever</i>
                                    </a></td>
                              </tr>
                              <tr>
                                 <td>03</td>
                                 <td>74445</td>
                                 </a>
                                 <td><a class="waves-effect waves-light   mr-1 mb-1" id="01" onclick="del(this.id)" type="submit" name="action">
                                       <i class="material-icons left">delete_forever</i>
                                    </a></td>
                              </tr>
                              </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

   </div>
</div>
</div>
</div>
</div>
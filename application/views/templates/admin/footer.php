<footer class="page-footer footer footer-static footer-light navbar-border navbar-shadow">
    <div class="footer-copyright">
        <div class="container"><span>&copy; 2020 <a href="https://softologics.com/" target="_blank">softologics</a> All rights reserved.</span><span class="right hide-on-small-only">Design and Developed by <a href="https://softologics.com/">softologics</a></span></div>
    </div>
</footer>

<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/imagesloaded.pkgd.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/js/datatables.checkboxes.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/select2/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/search.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/page-users.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/media-gallery-page.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/form-select2.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-carousel.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/extra-components-range-slider.js"></script>



</body>

</html>
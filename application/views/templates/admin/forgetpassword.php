<div class="row login-bg">
    <div class="col s12">
        <div class="container">
            <div id="login-page" class="row">
                <div class="col s8 m6 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
                    <?php echo form_open('admin/forgetpassword'); ?>
                    <div class="login-form">
                        <div class="row">
                            <div class="input-field col s12 center">
                                <img src="<?php echo base_url(); ?>assets/app-assets/images/logo/TEXT COLOR(2).png" alt="" style="width: 75%;margin: 10px 0px 10px 0;" class="responsive-img valign">
                                <h5 class="ml-4">Forgot Password</h5>
                                <h6 class="center login-form-text">You can reset your password</h6>
                            </div>
                        </div>
                        <?php if ($this->session->flashdata('empty')) : ?>
                            <div id="card-alert" class="card red">
                                <div class="card-content white-text">
                                    <p> <?php echo $this->session->flashdata('empty'); ?></p>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if ($this->session->flashdata('change_failed')) : ?>
                            <div id="card-alert" class="card red">
                                <div class="card-content white-text">
                                    <p> <?php echo $this->session->flashdata('change_failed'); ?></p>
                                </div>

                            </div>
                        <?php endif; ?>
                        <?php if ($this->session->flashdata('password_changed')) : ?>
                            <div id="card-alert" class="card red">
                                <div class="card-content white-text">
                                    <p> <?php echo $this->session->flashdata('password_changed'); ?></p>
                                </div>

                            </div>
                        <?php endif; ?>
                        <?php if ($this->session->flashdata('error')) : ?>
                            <div id="card-alert" class="card red">
                                <div class="card-content white-text">
                                    <p> <?php echo $this->session->flashdata('error'); ?></p>
                                </div>

                            </div>
                        <?php endif; ?>
                        <div class="row margin">
                            <div class="input-field col s12">
                                <i style="color:#ed4242;" class="material-icons prefix pt-2">person_outline</i>
                                <input id="username" name="email" type="email" placeholder="Type Your Email" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <button type="submit" name="login" class="btn waves-effect submit border-round waves-light col s12">Send Email</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m6 l6">
                                <p class=" medium-small"><a style="font-size: large;" href="<?php echo base_url(); ?>admin/login">Login</a></p>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
  <div class="row">
    <div class="col s12">
      <div class="container">
        <div id="register-page" class="row">
          <div class="col s12 m6 l4 z-depth-4 card-panel border-radius-6 register-card bg-opacity-8">
            <form class="login-form">
              <div class="row">
                <div class="input-field col s12 center">
                  <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/app-assets/images/logo/TEXT COLOR 2.png" alt="" style="height: 130px;" class="responsive-img valign"></a>

                  <h5 class="ml-4">Register</h5>
                  <p class="ml-4">Join to our community now !</p>
                </div>
              </div>
              <div class="row">
                <div class="col s12">
                  <div class="row margin">
                    <div class="input-field col s12  m12 l6">
                      <i class="material-icons prefix pt-2">person_outline</i>
                      <input type="text" name="UserFirstName" placeholder="Type Your First Name" required style="width: 85%!important;">
                    </div>
                    <div class="input-field col s12  m12 l6">
                      <i class="material-icons prefix pt-2">remove</i>
                      <input type="text" name="UserLastName" placeholder="Type Your Last Name" required style="width: 85%!important;">
                    </div>
                  </div>
                  <div class="row margin">
                    <div class="input-field col s12  m12 l6">
                      <i class="material-icons prefix pt-2">phone_outline</i>
                      <input type="number" name="UserPhone" placeholder="Type Your Number" required style="width: 85%!important;">
                    </div>
                    <div class="input-field col s12  m12 l6">
                      <i class="material-icons prefix pt-2">lock_outline</i>
                      <input type="password" name="password" placeholder="Type Your Password" required style="width: 85%!important;">
                    </div>
                  </div>
                  <div class="row margin">
                    <div class="input-field col s12  m12 l6">
                      <i class="material-icons prefix pt-2">pin_drop</i>
                      <input type="text" name="UserAddress" placeholder="Type Your address" required style="width: 85%!important;">
                    </div>
                    <div class="input-field col s12  m12 l6">
                      <i class="material-icons prefix pt-2">mail_outline</i>
                      <input type="email" name="email" placeholder="Type Your Email" required style="width: 85%!important;">
                    </div>
                  </div>
                  <!-- <div class="row margin">
                    <div class="input-field col s12  m12 l12">
                      <i class="material-icons prefix pt-2">pin_drop</i>
                      <input type="text" name="UserAddress" placeholder="Type Your address" required style="width: 85%!important;">
                    </div>
                  </div> -->
                  <div class="row">
                    <div class="input-field col s12">
                      <button class="btn waves-effect waves-light border-round submit col s12">Register</button>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="input-field col s12">
                  <p class="margin medium-small"><a href="<?php echo base_url(); ?>admin/login">Already have an account? Login</a></p>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
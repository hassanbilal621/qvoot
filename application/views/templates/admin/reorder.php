<div id="main">
    <div class="row">
        <div id="breadcrumbs-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0">
                            <span<span style="font-weight: bold;">Reorder Food</span>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <div class="box box-block bg-white">
                        <?php

                        $attributes = ['id' => 'myform'];
                        echo form_open('users/speedorder', $attributes); ?>
                        <div class="row">
                            <h4 class="card-title">Select Date And Time</h4>
                            <div class="input-field col s6">
                                <input id="date" name="date" value="<?php echo $order['date']; ?>" class="datepicker" type="text" readonly required>
                                <label for="date">select Date</label>
                            </div>
                            <div class="input-field col s6">
                                <input id="time" class="timepicker" value="<?php echo $order['time']; ?>" name="time" type="text" readonly required>
                                <label for="time">select Time</label>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="card-title">Guest And Food</h4>
                            <div class="input-field col s6">
                                <input name="guest" type="number" value="<?php echo $order['guest']; ?>" placeholder="1-<?php echo $guests['guest']; ?>" min="1" max="<?php echo $guests['guest']; ?>" required>
                                <label for="date">select Guest 1-<?php echo $guests['guest']; ?></label>
                            </div>
                            <div class="input-field col s6">
                                <label for="food" class="active">Select Food <span>(Press ctrl Key to select multiple food)</span></label>
                                <select class="browser-default" name="food[]" multiple="multiple" required>
                                    <?php foreach ($foods as $food) : ?>
                                        <?php
                                        $selected = 0;
                                        foreach ($order_foods as $order_food) :
                                            if ($food['food_id'] == $order_food['order_food']) {
                                                $selected = 1;
                                            }


                                        endforeach; ?>

                                        <option <?php if ($selected == 1) {
                                                    echo "selected";
                                                } ?> value="<?php echo $food['food_id']; ?> "><?php echo $food['food_name']; ?></option>


                                    <?php $selected = 0;

                                    endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="card-title">Special Diet</h4>

                            <div class="input-field col s3">
                                <label for="diet" class="active">Select Special Diet</label>
                                <select name="specialdiet" class="select2 browser-default" id="diet">
                                    <?php foreach ($diets as $diet) : ?>
                                        <option value="<?php echo $diet['diet_id']; ?>"><?php echo $diet['diet_name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="input-field col s3">
                                <input id="dietnumber" id="dietnumber" name="dietnumber" type="number">
                                <label for="dietnumber">Diet Qty</label>
                            </div>
                            <div class="input-field col s4">
                                <input id="dietnote" name="dietnote" id="dietnote" type="text">
                                <label for="dietnote">Add Diet Note</label>
                            </div>

                            <div class="input-field col s2">
                                <a id="add_row" class="waves-effect waves-light  btn submit box-shadow-none border-round">Add More</a>
                            </div>
                            <table class="striped bordered">
                                <thead>
                                    <tr>
                                        <th>Special Diet</th>
                                        <th>Diet Qty</th>
                                        <th>Diet Note</th>
                                    </tr>
                                </thead>
                                <tbody id="dynamic_field">
                                    <?php
                                    $i = 1;
                                    foreach ($order_diets as $order_diet) :

                                    ?>
                                        <tr id="row<?php $i ?>">
                                            <td><?php echo $order_diet['diet_name']; ?> <input type="hidden" name="diet_id[]" value="<?php echo $order_diet['diet_id']; ?>" id="diet_id" placeholder="Diet Name" class="form-control diet_id" /></td>
                                            <td><input type="number" name="dietnumber[]" value="<?php echo $order_diet['diet_num']; ?>" readonly class="form-control diet_id" step="0.00" min="0" /></td>
                                            <td><input type="text" name="dietnote[]" value="<?php echo $order_diet['diet_note']; ?>" class="form-control dietnote" readonly step="0" min="0" /></td>
                                            <td><a name="remove" id="<?php $i ?>" class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1  btn submit box-shadow-none border-round mr-1 mb-1 right btn_remove">Delete Row</a> </td>
                                        </tr>
                                    <?php
                                        $i++;
                                    endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <h4 class="card-title">Desert</h4>
                            <div class="col s2" style=" margin: 30px 0 0 0; ">
                                <label>
                                    <input type="checkbox" id="adddesert" name="des" />
                                    <span>Add a Desert</span>
                                </label>
                            </div>
                            <div class="input-field col s6 wantdesert display-none">
                                <label for="desert" class="active">Select Desert<span>(Press ctrl Key to select multiple Desert)</span></label>
                                <select class="select2 browser-default mdb-select" name="desert[]" multiple="multiple">
                                    <?php foreach ($deserts as $desert) : ?>
                                        <?php
                                        $selected = 0;
                                        foreach ($Order_deserts as $Order_desert) :
                                            if ($desert['desert_id'] == $Order_desert['order_desert']) {
                                                $selected = 1;
                                            }


                                        endforeach; ?>

                                        <option <?php if ($selected == 1) {
                                                    echo "selected";
                                                } ?> value="<?php echo $desert['desert_id']; ?>"><?php echo $desert['desert_name']; ?></option>


                                    <?php $selected = 0;

                                    endforeach; ?>
                                    <?php foreach ($deserts as $desert) : ?>
                                    <?php endforeach; ?>
                                </select>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="card-title">Delivery Instruction</h4>

                            <div class="input-field col s12">
                                <textarea id="" class="discrip" name="delivery_instruction" value="<?php echo $order['delivery_instruction']; ?>" placeholder="Type Your Delivery Instruction" required> </textarea>
                            </div>
                        </div>
                        <div class="row">
                            <h4 class="card-title">Address</h4>
                            <div class="col s6">
                                <p>
                                    <label>
                                        <input type="radio" id="chkYes" checked name="chkPinNo" />
                                        <span>Add Old Address</span>
                                    </label>
                                </p>
                            </div>
                            <div class="col s6">
                                <p>
                                    <label>
                                        <input type="radio" id="chkNo" name="chkPinNo" />
                                        <span>Type New Address</span>
                                    </label>
                                </p>
                            </div>

                        </div>
                        <div class="row" id="newaddress" style="display: none">
                            <div class="input-field col s8">
                                <textarea name="newaddress" id="" class="discrip" placeholder="Type Your New Address"></textarea>
                            </div>
                            <div class="input-field col s4">
                                <label for="" class="active">Select Zip Code</label>
                                <select name="zipcode" class="select2  browser-default">
                                    <?php foreach ($zipcodes as $zipcode) : ?>
                                        <option value="<?php echo $zipcode['zipcode']; ?>"><?php echo $zipcode['zipcode']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="row" id="oldaddress">
                            <div class="row">
                                <div class="input-field col s4">
                                    <select class="select2 browser-default" name="selectaddress" onchange="myFunctions(this.value);">
                                        <option disabled selected value="">select Your Address</option>
                                        <?php foreach ($addresss as $address) : ?>
                                            <option value="<?php echo $address['address_id']; ?>"><?php echo $address['address']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s8">
                                    <textarea class="discrip" name="oldaddress" id="address" value="<?php echo $order['address']; ?>" placeholder="Select your address first" readonly></textarea>
                                </div>
                                <div class="input-field col s4">
                                    <input id="zipcode" name="oldzipcode" placeholder=" " type="number" value="<?php echo $order['zipcode']; ?>" readonly>
                                    <label for="zipcode">Zip Code</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="input-field col s12">
                                <h6 style="border: 1px #a01515 solid;padding: 10px;">I understand this food order is not confirmed until I get a call from Food Hawk. Call us if you have any questions 512-909-6161 or something to that effect.</h6>
                            </div>
                        </div>
                        <div class="row">
                            <a onclick="submit()" class="waves-effect waves-light  btn submit box-shadow-none border-round right">Place Order</a>
                            <button type="submit" style="display:none;" id="suBmitfrom">submit</button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function submit() {
        swal({
            title: "Are you sure you want to submit?",
            text: "",
            icon: 'warning',
            dangerMode: true,
            buttons: {
                cancel: 'No!',
                delete: 'Yes'
            }
        }).then(function(willDelete) {
            if (willDelete) {


                // if (document.forms["myForm"]["fname"].value == "" && ) {
                //     alert("Name must be filled out");
                //     return false;
                // }
                document.getElementById("suBmitfrom").click();
                //document.getElementById("myform").submit();

            } else {
                swal("", {
                    title: 'Cancelled',
                    icon: "error",
                });
            }
        });
    }
</script>

<script>
    function address(addressid) {

        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>users/ajax_get_address_details/" + addressid,
            success: function(data) {
                alert("sadsad");

                var obj = JSON.parse(data);
                document.getElementById("zipcode").value = obj.zipcode;
                document.getElementById("address").value = obj.address;
            }
        });
    }
</script>

<script>
    $('#adddesert').change(function() {
        if ($(this).is(":checked")) {
            $('.wantdesert').removeClass('display-none');
        } else {
            $('.wantdesert').addClass('display-none');
        }
    });
</script>



<script>
    $(function() {
        $("input[name='chkPinNo']").click(function() {
            if ($("#chkYes").is(":checked")) {
                $("#oldaddress").show();
                $("#newaddress").hide();
            } else {

                $("#newaddress").show();
                $("#oldaddress").hide();
            }
        });
    });
</script>

<script>
    $(document).ready(function() {
        var finaltotal = 0;
        var i = <?php echo  $i; ?>;
        $('#add_row').click(function() {
            var e = document.getElementById("diet");
            dietname = e.options[e.selectedIndex].text;
            var diet_id = document.getElementById("diet").value;
            var dietnumber = document.getElementById("dietnumber").value;
            var dietnote = document.getElementById("dietnote").value;
            i++;
            $('#dynamic_field').append('<tr id="row' + i + '"><td>' + dietname +
                '<input type="hidden" name="diet_id[]" value="' + diet_id +
                '" id="diet_id" placeholder="Diet Name" class="form-control diet_id" /></td><td><input type="number" name="dietnumber[]" value="' +
                dietnumber +
                '" readonly placeholder="diet_id" class="form-control diet_id" step="0.00" min="0" /></td><td><input type="text" name="dietnote[]" placeholder="Enter dietnote" value="' +
                dietnote +
                '" class="form-control dietnote" readonly step="0" min="0" /></td><td><a name="remove" id="' + i +
                '" class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1  btn submit box-shadow-none border-round mr-1 mb-1 right btn_remove">Delete Row</a> </td></tr>'
            );
            document.getElementById("diet").value = "";
            document.getElementById("dietnumber").value = "";
            dietnote = document.getElementById("dietnote").value = "";
        });
        $(document).on('click', '.btn_remove', function() {
            var button_id = $(this).attr("id");

            $('#row' + button_id + '').remove();
        });
    });
</script>

<script>
    var dataaray = '<?php print_r($order_diets); ?>';


    alert(dataaray);
</script>



<script src="<?php echo base_url(); ?>assets/app-assets/js/jquerynew.min.js" type="text/javascript"></script>

<script>
    function myFunctions(val) {
        // var userid = this.id;
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>users/ajax_get_address_details/" + val,
            success: function(data) {
                //    alert(data);
                var obj = JSON.parse(data);
                document.getElementById("zipcode").value = obj.zipcode;
                document.getElementById("address").value = obj.address;
            }
        });
    }
</script>

<script>
    $(document).ready(function() {
        $('.timepicker').timepicker();
    });
</script>
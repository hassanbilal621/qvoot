<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-rounded">
    <div class="brand-sidebar">
        <h1 class="logo-wrapper">
            <a class="brand-logo darken-1" href="<?php echo base_url(); ?>admin">
                <img class="hide-on-med-and-down " src="<?php echo base_url(); ?>assets/favicon.png" style="margin: -20px 0 0 -10px;width: 50px !important;height: 100%;" alt="qvoot">
                <img class="show-on-medium-and-down hide-on-med-and-up" src="<?php echo base_url(); ?>assets/qvoot.png" style="margin: 0px 0px 0px 40px;width: 100px !important;height: 100%;" alt="qvoot">
                <span class="logo-text hide-on-med-and-down"><img src="<?php echo base_url(); ?>assets/qvoottext.png" alt="" style="width: 150px;height: 100%;margin: -12px 8px 11px -19px;"></span>
            </a>
            <a class=" navbar-toggler" href="#">
                <i class="material-icons">radio_button_checked</i>
            </a>
        </h1>
    </div>


    <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="accordion">

        <!---/////////////////////////////////////////-------------Dashboard-----------///////////////////////////////////////// -->

        <li class="navigation-header">
            <a class="navigation-header-text black-text">Dashboard</a>
            <i class="navigation-header-icon material-icons black-text">more_horiz</i>
        </li>
        <li class="bold">
            <a class="waves-effect waves-cyan" href="<?php echo base_url(); ?>admin/">
                <i class="material-icons">computer</i>
                <span class="menu-title" data-i18n="">Dashboard</span>
            </a>
        </li>

        <!---/////////////////////////////////////////-------------admin Management-----------///////////////////////////////////////// -->

        <li class="navigation-header">
            <a class="navigation-header-text black-text"> admin Management</a>
            <i class="navigation-header-icon material-icons black-text">more_horiz</i>
        </li>
        <li class="bold  <?php if ($this->router->fetch_method() == 'admins' || $this->router->fetch_method() == 'addadmin') {
                                echo 'active';
                            } ?>">
            <a class="<?php if ($this->router->fetch_method() == 'admins' || $this->router->fetch_method() == 'addadmin') {
                            echo 'active';
                        } ?> waves-effect waves-cyan" href="<?php echo base_url(); ?>admin/admins" data-i18n="">
                <i class="material-icons">person</i><span>Admins</span>
            </a>
        </li>




        <!---/////////////////////////////////////////-------------users Management-----------///////////////////////////////////////// -->

        <li class="navigation-header">
            <a class="navigation-header-text black-text"> Users Management</a>
            <i class="navigation-header-icon material-icons black-text">more_horiz</i>
        </li>
        <li class="bold  <?php if ($this->router->fetch_method() == 'users' || $this->router->fetch_method() == 'adduser') {
                                echo 'active';
                            } ?>">
            <a class="<?php if ($this->router->fetch_method() == 'users' || $this->router->fetch_method() == 'adduser') {
                            echo 'active';
                        } ?> waves-effect waves-cyan" href="<?php echo base_url(); ?>admin/users" data-i18n="">
                <i class="material-icons">person_outline</i><span>Users</span>
            </a>
        </li>
        <!---/////////////////////////////////////////------------- corcess-----------///////////////////////////////////////// -->
        <li class="navigation-header">
            <a class="navigation-header-text black-text"> Course Management</a>
            <i class="navigation-header-icon material-icons black-text">more_horiz</i>
        </li>
        <li class="bold  <?php if ($this->router->fetch_method() == 'courses' || $this->router->fetch_method() == 'adduser') {
                                echo 'active';
                            } ?>">
            <a class="<?php if ($this->router->fetch_method() == 'courses' || $this->router->fetch_method() == 'adduser') {
                            echo 'active';
                        } ?> waves-effect waves-cyan" href="<?php echo base_url(); ?>admin/courses" data-i18n="">
                <i class="material-icons">event_note</i><span>Courses</span>
            </a>
        </li>
        <!---/////////////////////////////////////////------------- Setting-----------///////////////////////////////////////// -->
        <li class="navigation-header">
            <a class="navigation-header-text black-text"> Setting Management</a>
            <i class="navigation-header-icon material-icons black-text">more_horiz</i>
        </li>
        <li class="bold  <?php if ($this->router->fetch_method() == 'contactus') {
                                echo 'active';
                            } ?>">
            <a class="<?php if ($this->router->fetch_method() == 'contactus') {
                            echo 'active';
                        } ?> waves-effect waves-cyan" href="<?php echo base_url(); ?>admin/contactus" data-i18n="">
                <i class="material-icons">event_note</i><span>Support Tickets</span>
            </a>
        </li>


    </ul>
    <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside>
<div id="main">
   <div class="row">
      <div id="breadcrumbs-wrapper" data-image="<?php echo base_url(); ?>/assets/app-assets/images/background/red.jpg" style="height: 64px;">
         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0"><span>diets Management</span></h5>
               </div>
               <div class="col s12 m6 l6 right-align-md">
                  <ol class="breadcrumbs mb-0">
                     <li class="breadcrumb-item"><a href="#">Home</a>
                     </li>
                     <li class="breadcrumb-item active">diets
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12">
         <div class="section" id="materialize-sparkline">
            <div class="section">
               <div class="card">
                  <div class="card-content">
                     <div class="row">
                        <h4 class="card-title">Add diets</h4>
                        <?php echo form_open('admin/diet'); ?>
                        <div class="input-field col s6">
                           <input type="text" name="diet_name" placeholder="Add diet" required>
                        </div>
                        <div class="input-field col s6">
                           <input type="text" name="diet_price" placeholder="Add diet Price" required>
                        </div>
                        <button class="waves-effect waves-light btn submit z-depth-2 mb-1 ml-1 right" id="view" type="submit" name="action">Submit
                           <i class="material-icons right">send</i>
                        </button>
                     </div>
                     <?php echo form_close(); ?>

                     <h4 class="card-title">Manage Expence diet</h4>
                     <div class="row">
                        <table id="page-length-option" class="display">
                           <thead>
                              <tr>
                                 <th>diet id</th>
                                 <th>diet Name</th>
                                 <th>diet Price</th>
                                 <th>edit</th>
                                 <th>delete</th>
                              </tr>
                           </thead>
                           <tbody>
                           <?php $s_no = '1';	
                            foreach ($diets as $diet) : ?>
                                 <tr>
                                    <td><?php echo $s_no ?></td>
                                    <td><?php echo $diet['diet_name']; ?></td>
                                    <td><?php echo $diet['diet_price']; ?></td>
                                    <td>
                                       <a class="waves-effect waves-light mr-1 mb-1 modal-trigger" onclick="loaddietinfo(this.id)" id="<?php echo $diet['diet_id']; ?>" type="submit" href="#modal3" name="action">
                                          <i class="material-icons left">edit</i>
                                       </a>
                                    </td>
                                    <td>
                                       <a class="waves-effect waves-light  mr-1 mb-1" href="<?php echo base_url(); ?>admin/deletediet/<?php echo $diet['diet_id']; ?>" type="submit" name="action">
                                          <i class="material-icons left">delete</i>
                                       </a>
                                    </td>
                                 </tr>
                                 <?php $s_no++; endforeach; ?>
                              </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

   </div>
</div>
</div>
</div>
</div>
<div id="modal3" class="modal">
   <div class="modal-content">
   </div>
</div>
<script>
   function loaddietinfo(dietid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/ajax_edit_diet/" + dietid,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal3').modal('open');
         }
      });
   }
</script>
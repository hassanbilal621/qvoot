<div id="main">
   <div class="row">
   <div id="breadcrumbs-wrapper" data-image="<?php echo base_url(); ?>assets/breadcrumb-bg.jpg">

         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0">
                     <span<span style="font-weight: bold;color: white;">User Management</span>
                  </h5>
               </div>
               <div class="col s12 m6 l6 right-align-md">
                  <ol class="breadcrumbs mt-0 mb-0">
                     <li class="breadcrumb-item"><a style="color: white;" href="<?php echo base_url(); ?>admin/index">Home</a>
                     </li>

                     <li style="color: white;" class="breadcrumb-item active">User Management
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12">
         <div class="container">
            <?php if ($this->session->flashdata('delete')) { ?>
               <div class="card-alert card red lighten-5">
                  <div class="card-content red-text">
                     <p>Deleted : admin Has Been deleted</p>
                  </div>
                  <button type="button" class="close red-text" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">×</span>
                  </button>
               </div>
            <?php } ?>
            <section class="users-list-wrapper section">
               <div class="users-list-table">
                  <div class="card">
                     <div class="card-content">
                        <!-- <a class="btn waves-effect submit border-round waves-light mb-4 right" href="<?php echo base_url() ?>admin/adduser"><i class="material-icons">add</i> Add user</a> -->
                        <div class="responsive-table">
                           <table id="users-list-datatable" class="table">
                              <thead>
                                 <tr>
                                    <th></th>
                                    <th>id</th>
                                    <th>name</th>
                                    <th>category</th>
                                    <th>price</th>
                                    <th>Course Start</th>
                                    <th>total_classes</th>
                                    <th>instructor_name</th>
                                    <th>language</th>
                                    <th>status</th>
                                    <th>view</th>
                                    <th>action</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php foreach ($courses as $course) : ?>
                                    <tr>
                                       <td></td>
                                       <td><?php echo $course['couse_id']; ?></td>
                                       <td><?php echo $course['course_name']; ?></td>
                                       <td><?php echo $course['course_category']; ?></td>
                                       <td><?php echo $course['course_price']; ?></td>
                                       <td><?php echo $course['course_start_date']; ?></td>
                                       <td><?php echo $course['course_total_students']; ?></td>
                                       <td><?php echo $course['course_instructor_name']; ?></td>
                                       <td><?php echo $course['course_language']; ?></td>
                                       <td><?php echo $course['course_status']; ?></td>
                                       <?php if ($course['course_status'] == 'enable') { ?>
                                          <td class="switch">
                                             <label>
                                                Deactivate <input type="checkbox" checked onclick="disablecourse(this.id)" id="<?php echo $course['couse_id']; ?>">
                                                <span class="lever"></span>
                                                Activate </label>
                                          </td>
                                       <?php } else { ?><td class="switch">
                                             <label>
                                                Deactivate <input type="checkbox" onclick="enablecourse(this.id)" id="<?php echo $course['couse_id']; ?>">
                                                <span class="lever"></span>
                                                Activate </label>
                                          </td>
                                       <?php  } ?>
                                       <td><a class="tooltipped" data-position="bottom" data-tooltip="View Course" href="<?php echo base_url(); ?>admin/view_course/<?php echo $course['couse_id']; ?>"><i class="material-icons">remove_red_eye</i></a></td>

                                    </tr>
                                 <?php endforeach; ?>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
</div>

<script>
   function enablecourse(couse_id) {
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/enablecourse/" + couse_id,
         success: function(data) {
            location.reload();


         }
      });
   }
</script>
<script>
   function disablecourse(couse_id) {
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/disablecourse/" + couse_id,
         success: function(data) {
            location.reload();




         }
      });
   }
</script>
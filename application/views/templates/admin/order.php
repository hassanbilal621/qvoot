<div id="main">
   <div class="row">
   <div id="breadcrumbs-wrapper" data-image="<?php echo base_url(); ?>/assets/app-assets/images/background/red.jpg" style="height: 64px;">
         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0"><span>orders</span></h5>
               </div>
               <div class="col s12 m6 l6 right-align-md">
                  <ol class="breadcrumbs mb-0">
                     <li class="breadcrumb-item"><a href="#">Home</a>
                     </li>
                     <li class="breadcrumb-item active">orders
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12">
         <div class="container">
            <!-- users list start -->
            <section class="users-list-wrapper section">
               <div class="users-list-table">
                  <div class="card">
                     <div class="card-content">
                        <!-- datatable start -->
                        <div class="responsive-table">
                           <table id="users-list-datatable" class="table">
                              <thead>
                                 <tr>
                                    <th></th>
                                    <th>id</th>
                                    <th>Food Name</th>
                                    <th>Food</th>
                                    <th>Your Orders</th>
                                    <th>Last Order Date</th>
                                    <th>Status</th>
                                    <th>View</th>
                                    <th>Delete</th>
                                    <th></th>
                                 </tr>
                              </thead>
                              <tbody>
                                

                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
</div>
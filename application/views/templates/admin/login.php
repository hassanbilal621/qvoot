<div class="row login-bg">
    <div class="col s12">
        <div class="container">
            <div id="login-page" class="row">
                <div class="col s8 m6 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
                    <?php echo form_open('admin/login_admin'); ?>
                    <div class="login-form">
                        <div class="row">
                            <div class="input-field col s12 center">
                                <img src="<?php echo base_url(); ?>assets/app-assets/images/logo/TEXT COLOR(2).png" alt="" style="width: 75%;margin: 10px 0px 10px 0;" class="responsive-img valign">

                                <h6 class="center login-form-text">Welcome to the Study Clock Admin</h6>
                            </div>
                        </div>
                        <?php if ($this->session->flashdata('login_failed')) : ?>
                            <div id="card-alert" id="car" class="card red">
                                <div class="card-content center white-text" style="margin: 0px 0 0 0;padding: 5px;">
                                    <p> <?php echo $this->session->flashdata('login_failed'); ?></p>
                                </div>

                            </div>
                        <?php endif; ?>
                        <?php if ($this->session->flashdata('password_changed')) : ?>
                            <div id="card-alert" id="car" class="card green">
                                <div class="card-content center white-text" style="margin: 0px 0 0 0;padding: 5px;">
                                    <p> <?php echo $this->session->flashdata('password_changed'); ?></p>
                                </div>
                                s
                            </div>
                        <?php endif; ?>
                        <?php if ($this->session->flashdata('user_registered')) : ?>
                            <div id="card-alert" class="card green">
                                <div class="card-content center white-text" style="margin: 0px 0 0 0;padding: 5px;">
                                    <p> <?php echo $this->session->flashdata('user_registered'); ?></p>
                                </div>

                            </div>
                        <?php endif; ?>
                        <?php if ($this->session->flashdata('user_loggedout')) : ?>
                            <div id="card-alert" class="card red">
                                <div class="card-content center white-text" style="margin: 0px 0 0 0;padding: 5px;">
                                    <p><?php echo $this->session->flashdata('user_loggedout'); ?></p>
                                </div>

                            </div>
                        <?php endif; ?>
                        <div class="row margin">
                            <div class="input-field col s12  m12 l12">
                                <input id="email" type="email" name="admin_email" class="validate" required>
                                <label for="email" data-error="wrong" data-success="right">Email</label>
                                <span class="helper-text" data-error="wrong" data-success="right"></span>
                            </div>
                        </div>
                        <div class="row margin">
                            <div class="input-field col s12  m12 l12">
                                <input id="password" type="password" name="admin_password" class="validate" required>
                                <label for="password" data-error="wrong" data-success="right">Password</label>
                                <span class="helper-text" data-error="wrong" data-success="right"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <button type="submit" name="login" class="btn waves-effect submit border-round waves-light col s12">Login</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/search.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js"></script>
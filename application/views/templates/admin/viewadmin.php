<div id="main">
   <div class="row">
   <div id="breadcrumbs-wrapper" data-image="<?php echo base_url(); ?>assets/breadcrumb-bg.jpg">
      <div id="breadcrumbs-wrapper">
         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0">
                     <span<span style="font-weight: bold;color: white;">View admin</span>
                  </h5>
               </div>
               <div class="col s12 m6 l6 right-align-md">
                  <ol class="breadcrumbs mt-0 mb-0">
                     <li class="breadcrumb-item"><a style="color: white;" href="<?php echo base_url(); ?>admin/index">Home</a>
                     </li>

                     <li class="breadcrumb-item"><a style="color: white;" href="<?php echo base_url(); ?>admin/admins">Manage Admin</a>
                     </li>
                     <li style="color: white;" class="breadcrumb-item active">View admin
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12">
         <div class="container">

            <!-- users view start -->
            <div class="section users-view">
               <!-- users view media object start -->
               <div class="card-panel">
                  <?php if ($this->session->flashdata('registered')) : ?>
                     <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong> Admin Are Successfully Registered .
                     </div>
                  <?php endif; ?>
                  <?php if ($this->session->flashdata('updated')) : ?>
                     <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>updated!</strong> Admin has been Successfully updated .
                     </div>
                  <?php endif; ?>
                  <div class="row">
                     <div class="col s12 m7">
                        <div class="display-flex media">
                           <a href="#" class="avatar">
                              <img src="<?php echo base_url(); ?>assets/upload/<?php echo $admin['admin_profile_image']; ?>" alt="Admin- Profile" class="circle" height="64" width="64">
                           </a>
                           <div class="media-body">
                              <h6 class="media-heading">
                                 <span class="capitalize"><?php echo $admin['admin_last_name']; ?></span><br>
                                 <span class="grey-text"><?php echo $admin['admin_email']; ?></span>
                              </h6>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="card">
                  <div class="card-content">
                     <div id="information">
                        <div class="row">
                           <div class="col s6">
                              <table class="striped">
                                 <tbody>
                                    <tr>
                                       <td class="black-text">First Name</td>
                                       <td class="capitalize"><?php echo $admin['admin_first_name']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Email</td>
                                       <td style="text-transform: lowercase!important;"><?php echo $admin['admin_email']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Mobile</td>
                                       <td class="capitalize"><?php echo $admin['admin_mobile']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Registration Date:</td>
                                       <td class="capitalize"><?php echo $admin['admin_registration_date']; ?></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                           <div class="col s6">
                              <table class="striped">
                                 <tbody>
                                    <tr>
                                       <td class="black-text">Last Name</td>
                                       <td class="capitalize"><?php echo $admin['admin_last_name']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Country</td>
                                       <td class="capitalize"><?php echo $admin['country_name']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">City</td>
                                       <td class="capitalize"><?php echo $admin['admin_city']; ?></td>
                                    </tr>
                                    <tr>
                                       <td class="black-text">Status</td>
                                       <td class="capitalize"><?php echo $admin['admin_status']; ?></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>


                        </div>
                      
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
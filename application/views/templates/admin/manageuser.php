<div id="main">
   <div class="row">
   <div id="breadcrumbs-wrapper" data-image="<?php echo base_url(); ?>assets/breadcrumb-bg.jpg">

         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title mt-0 mb-0">
                     <span<span style="font-weight: bold;color: white;">User Management</span>
                  </h5>
               </div>
               <div class="col s12 m6 l6 right-align-md">
                  <ol class="breadcrumbs mt-0 mb-0">
                     <li class="breadcrumb-item"><a style="color: white;" href="<?php echo base_url(); ?>admin/index">Home</a>
                     </li>

                     <li style="color: white;" class="breadcrumb-item active">User Management
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </div>
      <div class="col s12">
         <div class="container">
            <?php if ($this->session->flashdata('delete')) { ?>
               <div class="card-alert card red lighten-5">
                  <div class="card-content red-text">
                     <p>Deleted : admin Has Been deleted</p>
                  </div>
                  <button type="button" class="close red-text" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">×</span>
                  </button>
               </div>
            <?php } ?>
            <section class="users-list-wrapper section">
               <div class="users-list-table">
                  <div class="card">
                     <div class="card-content">
                        <!-- <a class="btn waves-effect submit border-round waves-light mb-4 right" href="<?php echo base_url() ?>admin/adduser"><i class="material-icons">add</i> Add user</a> -->
                        <div class="responsive-table">
                           <table id="users-list-datatable" class="table">
                              <thead>
                                 <tr>
                                    <th></th>
                                    <th>id</th>
                                    <th>first name</th>
                                    <th>last name</th>
                                    <th>email</th>
                                    <th>image</th>
                                    <th>Total courses</th>
                                    <th>Total enroll</th>
                                    <th>status</th>
                                    <th>view</th>
                                    <th></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php foreach ($users as $user) : ?>
                                    <tr>
                                       <td></td>
                                       <td><?php echo $user['user_id']; ?></td>
                                       <td><?php echo $user['first_name']; ?></td>
                                       <td><?php echo $user['last_name']; ?></td>
                                       <td><?php echo $user['user_email']; ?></td>
                                       <td><img src="<?php echo base_url(); ?>assets/uploads/<?php if (isset($supplier['user_image'])) {
                                                                                                echo $supplier['user_image'];
                                                                                             } else {
                                                                                                echo "no_image.jpg";
                                                                                             }  ?>" width="64px"></td>
                                       <td><?php echo $user['user_add_courses_count']; ?></td>
                                       <td>0</td>
                                       <td><?php echo $user['user_status']; ?></td>
                                       <td><a class="tooltipped" data-position="bottom" data-tooltip="View User" href="<?php echo base_url(); ?>admin/view_user/<?php echo $user['user_id']; ?>"><i class="material-icons">remove_red_eye</i></a></td>
                                       <td></td>
                                    </tr>
                                 <?php endforeach; ?>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
</div>
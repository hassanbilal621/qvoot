<!-- BEGIN: SideNav-->

<!-- <aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light sidenav-active-square">
  <div class="brand-sidebar" style="background-color: #a01515">
    <h1 class="logo-wrapper"><a class=" darken-1" href="<?php echo base_url(); ?>users/index"><img class="hide-on-med-and-down" src="<?php echo base_url(); ?>assets/app-assets/images/logo/logo.png" style="width: 200px !important;margin: 12px 0 0px 5px;" alt=" FoodHawk"></span></a><a class="navbar-toggler" href="#"><i class="material-icons" style="color: white;">radio_button_checked</i></a></h1>
  </div>
  <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="accordion">
    <li class="navigation-header">
      <a class="navigation-header-text black-text">Dashboard</a>
      <i class="navigation-header-icon material-icons black-text">more_horiz</i>
    </li>
    <li class="bold"><a class="waves-effect waves-cyan" href="<?php echo base_url(); ?>users/"><i class="material-icons">computer</i><span style="color: #444444;">Dashboard</span></a>
    </li>
    <li class="navigation-header">
      <a class="navigation-header-text black-text"> Order Management</a>
      <i class="navigation-header-icon material-icons black-text">more_horiz</i>
    </li>
    <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>users/orderfood"><i class="material-icons">event_note</i><span style="color: #444444;">Speed Order</span></a>
    </li>

    <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>users/myfavorites"><i class="material-icons">star</i><span style="color: #444444;">My Favorites</span></a>
    </li>
    <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>users/orderhistory"><i class="material-icons">history</i><span style="color: #444444;">Order History</span></a>
    </li>
    <li class="navigation-header">
      <a class="navigation-header-text black-text"> Profile</a>
      <i class="navigation-header-icon material-icons black-text">more_horiz</i>
    </li>
    <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>users/Profile"><i class="material-icons">person_outline</i><span style="color: #444444;">Profile</span></a>
    </li>

    <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>users/notification"><i class="material-icons">notifications_none</i><span style="color: #444444;">Notification</span> <?php if (!$notificationcount) {
                                                                                                                                                                                                              } else { ?><span class="badge new badge pill background float-right mr-2"><?php echo $notificationcount ?></span> <?php } ?></a>
    </li>
    <li class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>users/logout"><i class="material-icons">keyboard_tab</i><span style="color: #444444;">logout</span></a>
    </li>
  </ul>


  <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside> -->
<!-- END: SideNav-->

<section class="dashboard-area">
  <div class="dashboard-sidebar">
    <div class="dashboard-nav-trigger">
      <div class="dashboard-nav-trigger-btn">
        <i class="la la-bars"></i> Dashboard Nav
      </div>
    </div>
    <div class="dashboard-nav-container">
      <div class="humburger-menu">
        <div class="humburger-menu-lines side-menu-close"></div><!-- end humburger-menu-lines -->
      </div>
      <div class="side-menu-wrap">
        <ul class="side-menu-ul">
          <li class="sidenav__item  <?php if ($this->router->fetch_method() == 'dashboard') {
                                      echo 'page-active';
                                    } ?>">
            <a href="<?php echo base_url(); ?>users/dashboard"><i class="la la-dashboard"></i> Dashboard</a></li>
          <li class="sidenav__item  <?php if ($this->router->fetch_method() == 'addcourses') {
                                      echo 'page-active';
                                    } ?>">
            <a href="<?php echo base_url(); ?>users/addcourses"><i class="la la-plus-circle"></i>Add Course</a></li>
          <li class="sidenav__item  <?php if ($this->router->fetch_method() == 'mycourses') {
                                      echo 'page-active';
                                    } ?>">
            <a href="<?php echo base_url(); ?>users/mycourses"><i class="la la-file-video-o"></i>My Courses</a></li>
          <li class="sidenav__item  <?php if ($this->router->fetch_method() == 'courseslist') {
                                      echo 'page-active';
                                    } ?>">
            <a href="<?php echo base_url(); ?>users/courseslist"><i class="la la-file-video-o"></i>Courses List</a></li>

          <li class="sidenav__item  <?php if ($this->router->fetch_method() == 'logout') {
                                      echo 'page-active';
                                    } ?>"><a href="<?php echo base_url(); ?>users/logout"><i class="la la-power-off"></i> Logout</a></li>
        </ul>
      </div>
    </div>
  </div><!-- end dashboard-sidebar -->
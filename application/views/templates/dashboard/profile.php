<div class="dashboard-content-wrap">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-content dashboard-bread-content d-flex align-items-center justify-content-between">
                    <div class="user-bread-content d-flex align-items-center">
                        <div class="bread-img-wrap">
                            <img src="<?php echo base_url(); ?>assets/uploads/<?php if (isset($product['user_image'])) {
                                                                                    echo $product['user_image'];
                                                                                } else {
                                                                                    echo "no_image.jpg";
                                                                                }  ?>" alt="">
                        </div>
                        <div class="section-heading">
                            <h2 class="section__title font-size-30">Hello, <?php echo $user['first_name']; ?></h2>

                        </div>
                    </div>
                    <div class="upload-btn-box">
                        <a href="<?php echo base_url(); ?>users/edit_profile" class="theme-btn">Edit Profile</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-12">
                <div class="section-block"></div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-12">
                <h3 class="widget-title">My Profile</h3>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-8">
                <div class="profile-detail pb-5">
                    <ul class="list-items">
                        <li><span class="profile-name">Registration Date:</span><span class="profile-desc"><?php echo $user['user_registration_date']; ?></span></li>
                        <li><span class="profile-name">First Name:</span><span class="profile-desc"><?php echo $user['first_name']; ?></span></li>
                        <li><span class="profile-name">Last Name:</span><span class="profile-desc"><?php echo $user['last_name']; ?></span></li>
                        <li><span class="profile-name">Email:</span><span class="profile-desc"><?php echo $user['user_email']; ?> (<span style="color: red;">Not Verified</span>)</span></li>
                        <li><span class="profile-name">Gender:</span><span class="profile-desc"><?php echo $user['user_gender']; ?></span></li>
                        <li>
                            <span class="profile-name">Address:</span>
                            <span class="profile-desc"><?php echo $user['user_address']; ?></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="dashboard-content-wrap">
    <div class="container-fluid">



        <div class="row mt-5">
            <div class="col-lg-12">
                <div class="card-box-shared">
                    <?php if ($this->session->flashdata('login_failed')) : ?>
                        <div class="alert alert-danger alert-dismissible">
                            <strong>Wrong!</strong> You entered wrong password.
                        </div>
                    <?php endif; ?>
                    <?php if ($this->session->flashdata('login_success')) : ?>
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Success!</strong> Your Password Successfully updated .
                        </div>
                    <?php endif; ?>
                    <div class="card-box-shared-title">
                        <h3 class="widget-title">Edit Profile</h3>
                    </div>
                    <div class="card-box-shared-body">
                        <div class="section-tab section-tab-2">
                            <ul class="nav nav-tabs" role="tablist" id="review">
                                <li role="presentation">
                                    <a href="#profile" role="tab" data-toggle="tab" class="active" aria-selected="true">
                                        Profile
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#password" role="tab" data-toggle="tab" aria-selected="false">
                                        Password
                                    </a>
                                </li>
                                <!-- <li role="presentation">
                                    <a href="#change-email" role="tab" data-toggle="tab" aria-selected="false">
                                        Change Email
                                    </a>
                                </li> -->
                                <li role="presentation">
                                    <a href="#withdraw" role="tab" data-toggle="tab" aria-selected="false">
                                        Withdraw
                                    </a>
                                </li>
                                <!-- <li role="presentation">
                                    <a href="#account" role="tab" data-toggle="tab" aria-selected="false">
                                        Account
                                    </a>
                                </li> -->
                            </ul>
                        </div>
                        <div class="dashboard-tab-content mt-5">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active show" id="profile">
                                    <div class="user-form">
                                        <div class="user-profile-action-wrap mb-5">
                                            <h3 class="widget-title font-size-18 padding-bottom-40px">Profile Settings</h3>
                                            <div class="user-profile-action d-flex align-items-center">
                                                <div class="user-pro-img">
                                                    <img src="<?php echo base_url(); ?>assets/uploads/<?php if ($user['user_image']) {
                                                                                                            echo $user['user_image'];
                                                                                                        } else {
                                                                                                            echo 'no_image.jpg';
                                                                                                        } ?>" alt="user-image" class="img-fluid radius-round border">
                                                </div>
                                                <div class="upload-btn-box course-photo-btn">
                                                    <?php echo form_open_multipart('users/upload_profile_picture/' . $user['user_id']); ?>
                                                    <input type="file" name="user_file" required>
                                                    <p>Max file size is 5MB, Minimum dimension: 200x200 And Suitable files are .jpg &amp; .png</p>
                                                    <button class="theme-btn mt-3" type="submit">Upload Photo</button>
                                                    <!-- <button class="theme-btn mt-3" type="button">Remove Photo</button> -->
                                                    <?php echo form_close(); ?>


                                                </div>
                                            </div><!-- end user-profile-action -->
                                        </div><!-- end user-profile-action-wrap -->
                                        <div class="contact-form-action">
                                            <?php echo form_open_multipart('users/update_profile/' . $user['user_id']); ?>
                                            <div class="row">
                                                <div class="col-lg-6 col-sm-6">
                                                    <div class="input-box">
                                                        <label class="label-text">First Name<span class="primary-color-2 ml-1">*</span></label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="first_name" value="<?php echo $user['first_name']; ?>">
                                                            <span class="la la-user input-icon"></span>
                                                        </div>
                                                    </div>
                                                </div><!-- end col-lg-6 -->
                                                <div class="col-lg-6 col-sm-6">
                                                    <div class="input-box">
                                                        <label class="label-text">Last Name<span class="primary-color-2 ml-1">*</span></label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" name="last_name" value="<?php echo $user['last_name']; ?>">
                                                            <span class="la la-user input-icon"></span>
                                                        </div>
                                                    </div>
                                                </div><!-- end col-lg-6 -->

                                                <div class="col-lg-6 col-sm-6">
                                                    <div class="input-box">
                                                        <label class="label-text">Email Address<span class="primary-color-2 ml-1">*</span></label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="email" name="user_email" readonly value="<?php echo $user['user_email']; ?>">
                                                            <span class="la la-envelope input-icon"></span>
                                                        </div>
                                                    </div>
                                                </div><!-- end col-lg-6 -->
                                                <div class="col-lg-6 col-sm-6">
                                                    <div class="input-box">
                                                        <label class="label-text">Phone Number<span class="primary-color-2 ml-1">*</span></label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="number" name="user_phone" value="<?php echo $user['user_phone']; ?>">
                                                            <span class="la la-phone input-icon"></span>
                                                        </div>
                                                    </div>
                                                </div><!-- end col-lg-6 -->
                                                <div class="col-lg-12">
                                                    <div class="input-box">
                                                        <label class="label-text">Bio<span class="primary-color-2 ml-1">*</span></label>
                                                        <div class="form-group">
                                                            <textarea class="message-control form-control" name="user_bio"><?php echo $user['user_bio']; ?></textarea>
                                                            <span class="la la-pencil input-icon"></span>
                                                        </div>
                                                    </div>
                                                </div><!-- end col-lg-12 -->
                                                <div class="col-lg-12">
                                                    <div class="btn-box">
                                                        <button class="theme-btn" type="submit">save changes</button>
                                                    </div>
                                                </div><!-- end col-lg-12 -->
                                            </div><!-- end row -->
                                            <?php echo form_close(); ?>
                                        </div>
                                    </div>
                                </div><!-- end tab-pane-->
                                <div role="tabpanel" class="tab-pane fade" id="password">
                                    <div class="user-form padding-bottom-60px">
                                        <div class="user-profile-action-wrap">
                                            <h3 class="widget-title font-size-18 padding-bottom-40px">Change Password</h3>
                                        </div><!-- end user-profile-action-wrap -->
                                        <div class="contact-form-action">
                                            <?php echo form_open_multipart('users/upload_profile_password/' . $user['user_id']); ?>
                                            <div class="row">
                                                <div class="col-lg-4 col-sm-4">
                                                    <div class="input-box">
                                                        <label class="label-text">Old Password<span class="primary-color-2 ml-1">*</span></label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="password" name="oldpassword" placeholder="Old password" required>
                                                            <span class="la la-lock input-icon"></span>
                                                        </div>
                                                    </div>
                                                </div><!-- end col-lg-4 -->
                                                <div class="col-lg-4 col-sm-4">
                                                    <div class="input-box">
                                                        <label class="label-text">New Password<span class="primary-color-2 ml-1">*</span></label>
                                                        <div class="form-group">
                                                            <input class="form-control" type="password" name="newpassword" placeholder="New password" required>
                                                            <span class="la la-lock input-icon"></span>
                                                        </div>
                                                    </div>
                                                </div><!-- end col-lg-4 -->

                                                <div class="col-lg-12">
                                                    <div class="btn-box">
                                                        <button class="theme-btn" type="submit">Change password</button>
                                                    </div>
                                                </div><!-- end col-lg-12 -->
                                            </div><!-- end row -->
                                            <?php echo form_close(); ?>
                                        </div>
                                    </div>

                                </div><!-- end tab-pane-->
                                <div role="tabpanel" class="tab-pane fade" id="change-email">
                                    <div class="user-form">
                                        <div class="user-profile-action-wrap">
                                            <h3 class="widget-title font-size-18 padding-bottom-40px">Change email</h3>
                                        </div><!-- end user-profile-action-wrap -->
                                        <div class="contact-form-action">
                                            <form method="post">
                                                <div class="row">
                                                    <div class="col-lg-4 col-sm-4">
                                                        <div class="input-box">
                                                            <label class="label-text">Old Email<span class="primary-color-2 ml-1">*</span></label>
                                                            <div class="form-group">
                                                                <input class="form-control" type="text" name="text" placeholder="Old email">
                                                                <span class="la la-envelope input-icon"></span>
                                                            </div>
                                                        </div>
                                                    </div><!-- end col-lg-4 -->
                                                    <div class="col-lg-4 col-sm-4">
                                                        <div class="input-box">
                                                            <label class="label-text">New Email<span class="primary-color-2 ml-1">*</span></label>
                                                            <div class="form-group">
                                                                <input class="form-control" type="text" name="text" placeholder="New email">
                                                                <span class="la la-envelope input-icon"></span>
                                                            </div>
                                                        </div>
                                                    </div><!-- end col-lg-4 -->
                                                    <div class="col-lg-4 col-sm-4">
                                                        <div class="input-box">
                                                            <label class="label-text">Confirm New Email<span class="primary-color-2 ml-1">*</span></label>
                                                            <div class="form-group">
                                                                <input class="form-control" type="text" name="text" placeholder="Confirm new email">
                                                                <span class="la la-envelope input-icon"></span>
                                                            </div>
                                                        </div>
                                                    </div><!-- end col-lg-4 -->
                                                    <div class="col-lg-12">
                                                        <div class="btn-box">
                                                            <button class="theme-btn" type="submit">save changes</button>
                                                        </div>
                                                    </div><!-- end col-lg-12 -->
                                                </div><!-- end row -->
                                            </form>
                                        </div>
                                    </div>
                                </div><!-- end tab-pane-->
                                <div role="tabpanel" class="tab-pane fade" id="withdraw">
                                    <div class="user-profile-action-wrap">
                                        <h3 class="widget-title font-size-18 padding-bottom-40px">Select a Withdraw Method</h3>
                                    </div><!-- end user-profile-action-wrap -->
                                    <div class="withdraw-method-wrap">
                                        <div class="row">
                                            <div class="col-lg-2 column-td-half">
                                                <div class="payment-option">
                                                    <label for="radio-1" class="radio-trigger">
                                                        <input type="radio" id="radio-1" name="radio">
                                                        <span class="checkmark"></span>
                                                        <span class="widget-title font-size-18">
                                                            Bank Transfer
                                                            <span class="d-block primary-color-3 font-weight-medium font-size-13 line-height-18">Min withdraw $50.00</span>
                                                        </span>
                                                    </label>
                                                </div>
                                            </div><!-- end col-lg-2 -->
                                            <div class="col-lg-2 column-td-half">
                                                <div class="payment-option">
                                                    <label for="radio-2" class="radio-trigger">
                                                        <input type="radio" id="radio-2" name="radio">
                                                        <span class="checkmark"></span>
                                                        <span class="widget-title font-size-18">
                                                            E-Check
                                                            <span class="d-block primary-color-3 font-weight-medium font-size-13 line-height-18">Min withdraw $50.00</span>
                                                        </span>
                                                    </label>
                                                </div>
                                            </div><!-- end col-lg-2 -->
                                            <div class="col-lg-2 column-td-half">
                                                <div class="payment-option">
                                                    <label for="radio-3" class="radio-trigger">
                                                        <input type="radio" id="radio-3" name="radio">
                                                        <span class="checkmark"></span>
                                                        <span class="widget-title font-size-18">
                                                            Payoneer
                                                            <span class="d-block primary-color-3 font-weight-medium font-size-13 line-height-18">Min withdraw $50.00</span>
                                                        </span>
                                                    </label>
                                                </div>
                                            </div><!-- end col-lg-2 -->
                                            <div class="col-lg-2 column-td-half">
                                                <div class="payment-option">
                                                    <label for="radio-4" class="radio-trigger">
                                                        <input type="radio" id="radio-4" name="radio">
                                                        <span class="checkmark"></span>
                                                        <span class="widget-title font-size-18">
                                                            PayPal
                                                            <span class="d-block primary-color-3 font-weight-medium font-size-13 line-height-18">Min withdraw $50.00</span>
                                                        </span>
                                                    </label>
                                                </div>
                                            </div><!-- end col-lg-2 -->
                                            <div class="col-lg-2 column-td-half">
                                                <div class="payment-option">
                                                    <label for="radio-5" class="radio-trigger">
                                                        <input type="radio" id="radio-5" name="radio">
                                                        <span class="checkmark"></span>
                                                        <span class="widget-title font-size-18">
                                                            Skrill
                                                            <span class="d-block primary-color-3 font-weight-medium font-size-13 line-height-18">Min withdraw $50.00</span>
                                                        </span>
                                                    </label>
                                                </div>
                                            </div><!-- end col-lg-2 -->
                                            <div class="col-lg-2 column-td-half">
                                                <div class="payment-option">
                                                    <label for="radio-6" class="radio-trigger">
                                                        <input type="radio" id="radio-6" name="radio">
                                                        <span class="checkmark"></span>
                                                        <span class="widget-title font-size-18">
                                                            Stripe
                                                            <span class="d-block primary-color-3 font-weight-medium font-size-13 line-height-18">Min withdraw $50.00</span>
                                                        </span>
                                                    </label>
                                                </div>
                                            </div><!-- end col-lg-2 -->
                                        </div><!-- end row -->
                                    </div>
                                    <div class="user-form padding-top-50px">
                                        <div class="user-profile-action-wrap">
                                            <h3 class="widget-title font-size-18 padding-bottom-40px">Account info</h3>
                                        </div><!-- end user-profile-action-wrap -->
                                        <div class="contact-form-action">
                                            <form method="post">
                                                <div class="row">
                                                    <div class="col-lg-4 col-sm-4">
                                                        <div class="input-box">
                                                            <label class="label-text">Account Name<span class="primary-color-2 ml-1">*</span></label>
                                                            <div class="form-group">
                                                                <input class="form-control" type="text" name="text" value="Alex Smith">
                                                                <span class="la la-user input-icon"></span>
                                                            </div>
                                                        </div>
                                                    </div><!-- end col-lg-4 -->
                                                    <div class="col-lg-4 col-sm-4">
                                                        <div class="input-box">
                                                            <label class="label-text">Account Number<span class="primary-color-2 ml-1">*</span></label>
                                                            <div class="form-group">
                                                                <input class="form-control" type="text" name="text" value="3275476222500">
                                                                <span class="la la-pencil input-icon"></span>
                                                            </div>
                                                        </div>
                                                    </div><!-- end col-lg-4 -->
                                                    <div class="col-lg-4 col-sm-4">
                                                        <div class="input-box">
                                                            <label class="label-text">Bank Name<span class="primary-color-2 ml-1">*</span></label>
                                                            <div class="form-group">
                                                                <input class="form-control" type="text" name="text" value="South State Bank">
                                                                <span class="la la-bank input-icon"></span>
                                                            </div>
                                                        </div>
                                                    </div><!-- end col-lg-4 -->
                                                    <div class="col-lg-6 col-sm-6">
                                                        <div class="input-box">
                                                            <label class="label-text">IBAN<span class="primary-color-2 ml-1">*</span></label>
                                                            <div class="form-group">
                                                                <input class="form-control" type="text" name="text" value="3030">
                                                                <span class="la la-pencil input-icon"></span>
                                                            </div>
                                                        </div>
                                                    </div><!-- end col-lg-6 -->
                                                    <div class="col-lg-6 col-sm-6">
                                                        <div class="input-box">
                                                            <label class="label-text">BIC/SWIFT<span class="primary-color-2 ml-1">*</span></label>
                                                            <div class="form-group">
                                                                <input class="form-control" type="text" name="text" value="CDDHDBBL">
                                                                <span class="la la-pencil input-icon"></span>
                                                            </div>
                                                        </div>
                                                    </div><!-- end col-lg-6 -->
                                                    <div class="col-lg-12">
                                                        <!-- <div class="btn-box">
                                                            <button class="theme-btn" type="submit">save withdraw account</button>
                                                        </div> -->
                                                    </div><!-- end col-lg-12 -->
                                                </div><!-- end row -->
                                            </form>
                                        </div>
                                    </div>
                                </div><!-- end tab-pane-->
                                <div role="tabpanel" class="tab-pane fade" id="account">
                                    <div class="user-profile-action-wrap">
                                        <h3 class="widget-title font-size-18 padding-bottom-40px">My Account</h3>
                                    </div><!-- end user-profile-action-wrap -->
                                    <div class="user-account-wrap padding-bottom-40px">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="deactivate-account d-flex align-items-center">
                                                    <div class="payment-option">
                                                        <label for="radio-7" class="radio-trigger mb-0">
                                                            <input type="radio" id="radio-7" name="radio">
                                                            <span class="checkmark"></span>
                                                            <span class="widget-title font-size-18">Deactivate Account</span>
                                                        </label>
                                                    </div>
                                                    <div class="btn-box ml-3">
                                                        <button class="theme-btn line-height-40 font-size-14">Deactivate</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="section-block"></div>
                                    <div class="user-profile-action-wrap padding-top-40px">
                                        <div class="delete-account-wrap">
                                            <h3 class="widget-title font-size-18 pb-2 text-danger">Delete Account Permanently</h3>
                                            <p><span class="text-warning">Warning:</span> Once you delete your account, there is no going back. Please be certain.</p>
                                            <div class="btn-box mt-4">
                                                <button class="theme-btn line-height-40 font-size-14" data-toggle="modal" data-target=".account-delete-modal">Delete My Account</button>
                                            </div>
                                        </div>
                                    </div><!-- end user-profile-action-wrap -->
                                </div><!-- end tab-pane-->
                            </div><!-- end tab-content -->
                        </div><!-- end dashboard-tab-content -->
                    </div>
                </div><!-- end card-box-shared -->
            </div><!-- end col-lg-12 -->
        </div><!-- end row -->

    </div>
</div>
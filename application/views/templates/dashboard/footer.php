<!-- end scroll top -->

<!-- template js files -->
<script src="<?php echo base_url(); ?>assets/qvoot/js/jquery-3.4.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/bootstrap-select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/magnific-popup.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/isotope.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/waypoint.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/jquery.counterup.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/fancybox.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/wow.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/smooth-scrolling.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/progress-bar.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/date-time-picker.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/emojionearea.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/animated-skills.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/jquery.filer.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/tooltipster.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/main.js"></script>

<script src="<?php echo base_url(); ?>assets/timepicker/jquery.datetimepicker.full.js"></script>


<script>
    /*jslint browser:true*/
    /*global jQuery, document*/

    jQuery(document).ready(function() {
        'use strict';

        jQuery('#filter-date, #search-from-date, #search-to-date').datetimepicker();
    });
</script>

</body>

</html>
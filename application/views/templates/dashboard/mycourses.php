<div class="dashboard-content-wrap">
    <div class="container-fluid">
        
        <div class="row mt-5">
            <div class="col-lg-12">
                <div class="card-box-shared">
                    <div class="card-box-shared-title">
                        <h3 class="widget-title">My Courses</h3>
                    </div>
                    <div class="card-box-shared-body">


                        <?php foreach($courses as $course): ?>
                        <div class="card-item card-list-layout">
                            <div class="card-image">
                            
                            <?php foreach($courses_pictures as $coursespic): 
                                if($coursespic['course_id'] == $course['couse_id']){ ?>
                                <a href="" class="card__img"><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $coursespic['course_picture']; ?>" alt=""></a>
                            <?php 
                            break;
                                }
                                endforeach; ?>
                            </div><!-- end card-image -->
                            <div class="card-content">
                                <h3 class="card__title mt-0">
                                    <a href=""><?php echo $course['course_name']; ?></a>
                                </h3>
                                <p class="card__author">
                                    <a href=""><?php echo $course['course_instructor_name']; ?></a>
                                </p>
                                <div class="rating-wrap d-flex mt-2 mb-3">
                                    <ul class="review-stars">
                                        <li><span class="la la-star"></span></li>
                                        <li><span class="la la-star"></span></li>
                                        <li><span class="la la-star"></span></li>
                                        <li><span class="la la-star"></span></li>
                                        <li><span class="la la-star"></span></li>
                                    </ul>
                                    <span class="star-rating-wrap">
                                        <span class="star__rating">5</span>
                                        <span class="star__count">(0)</span>
                                    </span>
                                </div><!-- end rating-wrap -->
                                <div class="card-action">
                                    <ul class="card-duration d-flex">
                                        <li>
                                            <span class="meta__date mr-2">
                                                <span class="status-text">Status:</span>
                                                <span class="badge bg-success text-white">Published</span>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="meta__date mr-2">
                                                <span class="status-text">Duration:</span>
                                                <span class="status-text primary-color-3"><?php echo $course['course_hours']; ?></span>
                                            </span>
                                        </li>
                                        <li>
                                            <span class="meta__date mr-2">
                                                <span class="status-text">Students:</span>
                                                <span class="status-text primary-color-3">0</span>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                                <!-- end card-action -->
                                <div class="card-price-wrap d-flex align-items-center">
                                    <p class="card__price">Price :<?php echo $course['course_price']; ?></p>
                                    <div class="edit-action">
                                        <ul class="edit-list">
                                            <li><a href="<?php echo base_url()?>users/viewcourses/<?php echo $course['couse_id']; ?>" class="theme-btn view-btn"><i class="la la-eye mr-1 font-size-16"></i>View</a></li>
                                            <!-- <li><span class="theme-btn edit-btn"><i class="la la-edit mr-1 font-size-16"></i>Edit</span></li> -->
                                            <!-- <li><span class="theme-btn delete-btn" data-toggle="modal" data-target=".item-delete-modal"><i class="la la-trash mr-1 font-size-16"></i>Delete</span></li> -->
                                        </ul>
                                    </div>
                                </div><!-- end card-price-wrap -->
                            </div><!-- end card-content -->
                        </div><!-- end card-item -->
                        <?php endforeach; ?>
          
                    </div>
                </div>
            </div><!-- end col-lg-12 -->
        </div><!-- end row -->
        
    </div><!-- end container-fluid -->
</div><!-- end dashboard-content-wrap -->
<div class="dashboard-content-wrap">
        <div class="container-fluid">
           
            <div class="row mt-5">
                <div class="col-lg-12">
                    <div class="card-box-shared">
                        <div class="card-box-shared-title">
                            <h3 class="widget-title">My Purchase History</h3>
                        </div>
                        <div class="card-box-shared-body">
                            <div class="statement-table purchase-table table-responsive mb-5">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">Item</th>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li>#090909</li>
                                                    </ul>
                                                </div>
                                            </th>
                                            <td>
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li>
                                                            <a href="course-details.html" class="d-inline-block">
                                                                <img src="images/small-img.jpg" alt="">
                                                            </a>
                                                            <a href="course-details.html" class="d-inline-block primary-color">
                                                                MERN Stack Master Course - Building your own Instagram
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li>$79.00</li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li>July 12, 2019</li>
                                                    </ul>
                                                </div>
                                            </td>
                                             <td>
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li><span class="badge bg-success text-white p-1">Completed</span></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li>#090909</li>
                                                    </ul>
                                                </div>
                                            </th>
                                            <td>
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li>
                                                            <a href="course-details.html" class="d-inline-block">
                                                                <img src="images/small-img.jpg" alt="">
                                                            </a>
                                                            <a href="course-details.html" class="d-inline-block primary-color">
                                                                The Ultimate Text-To-Video Creation Course
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li>$79.00</li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li>July 12, 2019</li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li><span class="badge bg-info text-white p-1">On Hold</span></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li>#090909</li>
                                                    </ul>
                                                </div>
                                            </th>
                                            <td>
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li>
                                                            <a href="course-details.html" class="d-inline-block">
                                                                <img src="images/small-img.jpg" alt="">
                                                            </a>
                                                            <a href="course-details.html" class="d-inline-block primary-color">
                                                                Ultimate Guide for the Next Generation of Affiliates
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li>$79.00</li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li>July 12, 2019</li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li><span class="badge bg-danger text-white p-1">Cancelled</span></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li>#090909</li>
                                                    </ul>
                                                </div>
                                            </th>
                                            <td>
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li>
                                                            <a href="course-details.html" class="d-inline-block">
                                                                <img src="images/small-img.jpg" alt="">
                                                            </a>
                                                            <a href="course-details.html" class="d-inline-block primary-color">
                                                                Complete Ethical Hacking Course: Zero to Hero
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li>$79.00</li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li>July 12, 2019</li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="statement-info">
                                                    <ul class="list-items">
                                                        <li><span class="badge bg-danger text-white p-1">Cancelled</span></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="statement-table purchase-table table-responsive mb-5">
                                <div class="statement-header pb-4">
                                    <h3 class="widget-title font-size-18">Order Details</h3>
                                </div>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Item</th>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Coupon Code</th>
                                        <th scope="col">Quantity</th>
                                        <th scope="col">Paid via</th>
                                        <th scope="col">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li> Introduction Web Design with HTML</li>
                                                </ul>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li>$79.00</li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li>July 12, 2019</li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li>SNUGGLE323</li>
                                                </ul>
                                            </div>
                                        </td>
                                         <td>
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li>1</li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li>PayPal</li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li>$79.00</li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li> Introduction Web Design with HTML</li>
                                                </ul>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li>$79.00</li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li>July 12, 2019</li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li>SNUGGLE323</li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li>1</li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li>PayPal</li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li>$79.00</li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="border-bottom-0 pb-0 pt-0"></th>
                                        <td class="border-bottom-0 pb-0 pt-0"></td>
                                        <td class="border-bottom-0 pb-0 pt-0"></td>
                                        <td class="border-bottom-0 pb-0 pt-0"></td>
                                        <td class="border-bottom-0 pb-0 pt-0"></td>
                                        <td class="border-bottom-0 pb-0 pb-2">
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li class="primary-color font-weight-semi-bold font-size-18">Total:</li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td class="border-bottom-0 pb-0 pb-2">
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li>$79.00</li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="border-bottom-0 pb-0 pt-0"></th>
                                        <td class="border-bottom-0 pb-0 pt-0"></td>
                                        <td class="border-bottom-0 pb-0 pt-0"></td>
                                        <td class="border-bottom-0 pb-0 pt-0"></td>
                                        <td class="border-bottom-0 pb-0 pt-0"></td>
                                        <td class="border-bottom-0 pb-0 pt-0">
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li class="primary-color font-weight-semi-bold font-size-18">Total Paid:</li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td class="border-bottom-0 pb-0 pt-0">
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li>$79.00</li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="statement-table purchase-table refunds-table table-responsive">
                                <div class="statement-header pb-4">
                                    <h3 class="widget-title font-size-18">Refunds Details</h3>
                                </div>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Refunds</th>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Refunded to</th>
                                        <th scope="col">Refund Type</th>
                                        <th scope="col">Credit Note</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li> Introduction Web Design with HTML</li>
                                                </ul>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li>$79.00</li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li>July 12, 2019</li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li>Alex Smith</li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li>1</li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="statement-info">
                                                <ul class="list-items">
                                                    <li>PayPal</li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-lg-12 -->
            </div><!-- end row -->
            
        </div><!-- end container-fluid -->
    </div><!-- end dashboard-content-wrap -->
<div class="dashboard-content-wrap">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-content dashboard-bread-content d-flex align-items-center justify-content-between">
                    <div class="user-bread-content d-flex align-items-center">
                        <div class="bread-img-wrap">
                            <img src="<?php if (!empty($user['profile_image'])) {
                                            echo base_url() . 'assets/uploads/' . $user['profile_image'];
                                        } else {
                                            echo base_url() . 'assets/qvoot/images/team7.jpg';
                                        } ?>" alt="">
                        </div>
                        <div class="section-heading">
                            <h2 class="section__title font-size-30">Hello, <b> <?php echo $user['first_name']; ?></b></h2>
                            <div class="rating-wrap d-flex mt-2">

                            </div>
                        </div>
                    </div>
                    <!-- <div class="upload-btn-box">
                            <form action="#" method="post" enctype="multipart/form-data">
                                <input type="file" name="files[]" class="filer_input" multiple="multiple">
                            </form>
                        </div> -->
                </div>
            </div><!-- end col-lg-12 -->
        </div><!-- end row -->
        <div class="row mt-5">
            <div class="col-lg-12">
                <div class="section-block"></div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-12">
                <h3 class="widget-title">Dashboard</h3>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-4 column-lmd-2-half column-md-2-full">
                <div class="icon-box d-flex align-items-center">
                    <div class="icon-element icon-element-bg-1 flex-shrink-0">
                        <i class="la la-mouse-pointer"></i>
                    </div><!-- end icon-element-->
                    <div class="info-content">
                        <h4 class="info__title mb-2">Enrolled Courses</h4>
                        <span class="info__count">0</span>
                    </div><!-- end info-content -->
                </div>
            </div><!-- end col-lg-4 -->
            <div class="col-lg-4 column-lmd-2-half column-md-2-full">
                <div class="icon-box d-flex align-items-center">
                    <div class="icon-element icon-element-bg-2 flex-shrink-0">
                        <i class="la la-file-text-o"></i>
                    </div><!-- end icon-element-->
                    <div class="info-content">
                        <h4 class="info__title mb-2">Active Courses</h4>
                        <span class="info__count">0</span>
                    </div><!-- end info-content -->
                </div>
            </div><!-- end col-lg-4 -->
            <div class="col-lg-4 column-lmd-2-half column-md-2-full">
                <div class="icon-box d-flex align-items-center">
                    <div class="icon-element icon-element-bg-3 flex-shrink-0">
                        <i class="la la-graduation-cap"></i>
                    </div><!-- end icon-element-->
                    <div class="info-content">
                        <h4 class="info__title mb-2">Completed Courses</h4>
                        <span class="info__count">0</span>
                    </div><!-- end info-content -->
                </div>
            </div><!-- end col-lg-4 -->
            <div class="col-lg-4 column-lmd-2-half column-md-2-full">
                <div class="icon-box d-flex align-items-center">
                    <div class="icon-element icon-element-bg-4 flex-shrink-0">
                        <i class="la la-users"></i>
                    </div><!-- end icon-element-->
                    <div class="info-content">
                        <h4 class="info__title mb-2">Total Students</h4>
                        <span class="info__count">0</span>
                    </div><!-- end info-content -->
                </div>
            </div><!-- end col-lg-4 -->
            <div class="col-lg-4 column-lmd-2-half column-md-2-full">
                <div class="icon-box d-flex align-items-center">
                    <div class="icon-element icon-element-bg-5 flex-shrink-0">
                        <i class="la la-file-video-o"></i>
                    </div><!-- end icon-element-->
                    <div class="info-content">
                        <h4 class="info__title mb-2">Total Courses</h4>
                        <span class="info__count">0</span>
                    </div><!-- end info-content -->
                </div>
            </div><!-- end col-lg-4 -->
            <div class="col-lg-4 column-lmd-2-half column-md-2-full">
                <div class="icon-box d-flex align-items-center">
                    <div class="icon-element icon-element-bg-6 flex-shrink-0">
                        <i class="la la-dollar"></i>
                    </div><!-- end icon-element-->
                    <div class="info-content">
                        <h4 class="info__title mb-2">Total Earnings</h4>
                        <span class="info__count">0.00</span>
                    </div><!-- end info-content -->
                </div>
            </div><!-- end col-lg-4 -->
        </div><!-- end row -->
        <div class="row">
            <div class="col-lg-4 column-lmd-2-half column-md-full">
                <div class="chart-item">
                    <div class="chart-headline margin-bottom-40px">
                        <h3 class="widget-title font-size-18">Total Sales</h3>
                    </div>
                    <canvas id="doughnut-chart"></canvas>
                    <div class="chart-legend text-center margin-top-40px">
                        <ul>
                            <li><span class="legend__bg legend__bg-1"></span>Direct Sales</li>
                            <li><span class="legend__bg legend__bg-2"></span>Referral Sales</li>
                            <li><span class="legend__bg legend__bg-3"></span>Affiliate Sales</li>
                        </ul>
                    </div>
                </div>
            </div><!-- end col-lg-4 -->
            <div class="col-lg-4 column-lmd-2-half column-md-full">
                <div class="chart-item">
                    <div class="chart-headline margin-bottom-35px">
                        <h3 class="widget-title font-size-18">Net Income</h3>
                    </div>
                    <canvas id="bar-chart"></canvas>
                    <div class="chart-legend text-center margin-top-40px">
                        <ul>
                            <li>Sales for this month</li>
                        </ul>
                    </div>
                </div><!-- end chart-item -->
            </div><!-- end col-lg-4 -->
            <div class="col-lg-4 column-lmd-2-half column-md-full">
                <div class="chart-item">
                    <div class="chart-headline margin-bottom-35px">
                        <h3 class="widget-title font-size-18">Earning by Location</h3>
                    </div>
                    <div class="world-map">
                        <div id="visit-by-locate"></div>
                    </div>
                    <div class="progress-bar-wrap mt-4">
                        <div class="progress-item">
                            <p class="skillbar-title">USA</p>
                            <div class="skillbar-box">
                                <div class="skillbar" data-percent="70%">
                                    <div class="skillbar-bar skillbar-bar-bg-1"></div>
                                </div> <!-- End Skill Bar -->
                            </div>
                            <div class="skill-bar-percent">70%</div>
                        </div>
                        <div class="progress-item">
                            <p class="skillbar-title">UK</p>
                            <div class="skillbar-box">
                                <div class="skillbar" data-percent="50%">
                                    <div class="skillbar-bar skillbar-bar-bg-2"></div>
                                </div> <!-- End Skill Bar -->
                            </div>
                            <div class="skill-bar-percent">50%</div>
                        </div>
                        <div class="progress-item">
                            <p class="skillbar-title">China</p>
                            <div class="skillbar-box">
                                <div class="skillbar" data-percent="40%">
                                    <div class="skillbar-bar skillbar-bar-bg-3"></div>
                                </div> <!-- End Skill Bar -->
                            </div>
                            <div class="skill-bar-percent">40%</div>
                        </div>
                    </div>
                </div><!-- end chart-item -->
            </div><!-- end col-lg-4 -->
        </div><!-- end row -->
        <div class="row">
            <div class="col-lg-7 column-lmd-2-half column-md-full">
                <div class="chart-item">
                    <div class="chart-headline margin-bottom-30px d-flex justify-content-between align-items-center">
                        <h3 class="widget-title font-size-18">Earning Statistics</h3>
                        <div class="sort-ordering chart-short-option">
                            <select class="sort-ordering-select">
                                <option value="this-week">This Week</option>
                                <option value="this-month">This Month</option>
                                <option value="last-months">Last 6 Months</option>
                                <option value="this-year">This Year</option>
                            </select>
                        </div>
                    </div>
                    <canvas id="line-chart"></canvas>
                    <div class="chart-legend text-center margin-top-40px">
                        <ul>
                            <li>Earnings for this month</li>
                        </ul>
                    </div>
                </div><!-- end chart-item -->
            </div><!-- end col-lg-7 -->

        </div><!-- end row -->

    </div><!-- end container-fluid -->
</div><!-- end dashboard-content-wrap -->
</section><!-- end dashboard-area -->
<!-- ================================
    END DASHBOARD AREA
================================= -->

<!-- start scroll top -->
<div id="scroll-top">
    <i class="fa fa-angle-up" title="Go top"></i>
</div>
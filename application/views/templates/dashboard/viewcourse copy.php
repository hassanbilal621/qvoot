<div class="dashboard-content-wrap">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card-box-shared">
                    <div class="card-box-shared-title">
                        <h3 class="widget-title">View Course</h3>
                    </div>
                    <div class="card-box-shared-body">
                        <div class="user-form">
                            <div class="contact-form-action">
                                <div class="row">
                                    <div class="col s6">
                                        <table class="striped">
                                            <tbody>
                                                <tr>
                                                    <td class="black-text">Course Name :-</td>
                                                    <td class="capitalize"><?php echo $course['course_name']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="black-text">Course Category :-</td>
                                                    <td><?php echo $course['course_category']; ?></td>
                                                </tr>

                                                <tr>
                                                    <td class="black-text">Start Course Time :-</td>
                                                    <td class="capitalize"><?php echo $course['course_start_date']; ?></td>
                                                </tr>


                                                <tr>
                                                    <td class="black-text">Course Price :-</td>
                                                    <td class="capitalize"><?php echo $course['course_price']; ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col s6">
                                        <table class="striped">
                                            <tbody>
                                                <tr>
                                                    <td class="black-text">Instructor's Name :-</td>
                                                    <td class="capitalize"><?php echo $course['course_instructor_name']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="black-text">Total Class :-</td>
                                                    <td class="capitalize"><?php echo $course['course_total_students']; ?></td>
                                                </tr>


                                              
                                                <tr>
                                                    <td class="black-text">Course language :-</td>
                                                    <td class="capitalize"><?php echo $course['course_language']; ?></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12">
                                        <table class="striped">
                                            <tbody>
                                                <tr>
                                                    <td class="black-text">Course Description :-</td>
                                                    <td class="capitalize"><?php echo $course['course_full_desc']; ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="dashboard-content-wrap">
    <div class="container-fluid">
        <div class="row mt-5">
            <div class="col-lg-12">
                <h3 class="widget-title">Submit Course</h3>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-lg-12">
                <?php if ($this->session->flashdata('submit')) : ?>
                    <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Submit!</strong>Thank you for submitting your course, our team will review it within 48 hours. We will publish it after a successful review. If you still think your post is correct and is not approved, please reach us at support@qvoot.com or you can also chat with us directly through live chat 24/7.
                    </div>
                <?php endif; ?>
                <div class="card-box-shared">

                    <div class="card-box-shared-title">
                        <h3 class="widget-title">Course Details</h3>

                    </div>

                    <div class="card-box-shared-body">
                        <div class="user-form">
                            <div class="contact-form-action">
                                <?php echo form_open_multipart('users/submitcourse'); ?>
                                <div class="row">
                                    <div class="col-lg-6 col-sm-6">
                                        <div class="input-box">
                                            <label class="label-text">Course Name<span class="primary-color-2 ml-1">*</span></label>
                                            <div class="form-group">
                                                <input class="form-control" type="text" name="course_name" placeholder="Course title" required>
                                                <span class="la la-file-text-o input-icon"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-sm-6">
                                        <div class="input-box">
                                            <label class="label-text">Instructor's Name<span class="primary-color-2 ml-1">*</span></label>
                                            <div class="form-group">
                                                <input class="form-control" type="text" name="course_instructor_name" placeholder="Your name" required>
                                                <span class="la la-user input-icon"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-6">
                                        <div class="input-box">
                                            <label class="label-text">Course Category<i class="la la-question ml-1 tip" data-toggle="tooltip" data-placement="top" title="Separated by commas"></i></label>
                                            <div class="sort-ordering user-form-short">
                                                <select class="sort-ordering-select" name="course_category" onchange="getsubcat(this.value)" required>
                                                    <option value="select-a-category" selected="">Select a Category</option>
                                                    <?php foreach ($categories as $category) : ?>
                                                        <option value="<?php echo $category['category_id']; ?>"><?php echo $category['category_name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="input-box">
                                            <label class="label-text">Select Sub Category<span class="primary-color-2 ml-1">*</span></label>
                                            <div class="form-group">
                                                <div class="sort-ordering user-form-short">
                                                    <select id="course_subcat_id" class="sort-ordering-select browser-default" name="course_subcat_id" required>
                                                        <option disabled selected value="">Select Course Category First</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-6">
                                        <div class="input-box">
                                            <label class="label-text">Student Capacity<span class="primary-color-2 ml-1">*</span></label>
                                            <div class="form-group">
                                                <input class="form-control" type="number" name="course_total_students" placeholder="Student Capacity" required>
                                                <span class="la la-play-circle input-icon"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-6">
                                        <div class="input-box">
                                            <label class="label-text">Course Start Date<span class="primary-color-2 ml-1">*</span></label>
                                            <div class="form-group">
                                                <input class="form-control" type="datetime" id="filter-date" name="course_start_date" placeholder="Start Date and Time" required>
                                                <span class="la la-play-circle input-icon"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="input-box">
                                            <label class="label-text">Select Country<span class="primary-color-2 ml-1">*</span></label>
                                            <div class="form-group">
                                                <div class="sort-ordering user-form-short">
                                                    <select class="sort-ordering-select browser-default" name="course_country_id" required>
                                                        <option value="select-a-category" selected="">Select a country</option>
                                                        <?php foreach ($countries as $country) : ?>
                                                            <option value="<?php echo $country['country_name']; ?>"><?php echo $country['country_name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="input-box">
                                            <label class="label-text">Select Level<span class="primary-color-2 ml-1">*</span></label>
                                            <div class="form-group">
                                                <div class="sort-ordering user-form-short">
                                                    <select class="sort-ordering-select browser-default" name="course_level" required>
                                                        <option value="select-a-category" selected="">Select a Level</option>
                                                        <option value="Beginners">Beginners</option>
                                                        <option value="Intermediate">Intermediate</option>
                                                        <option value="Advanced">Advanced</option>
                                                        <option value="Expert">Expert</option>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-6">
                                        <div class="input-box">
                                            <label class="label-text">Certificate<span class="primary-color-2 ml-1">*</span></label>
                                            <div class="form-group">
                                                <div class="sort-ordering user-form-short">
                                                    <select class="sort-ordering-select browser-default" name="course_certificate" required>
                                                        <option value="yes">Yes</option>
                                                        <option value="no" selected="">No</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="input-box">
                                            <label class="label-text">Course Type<span class="primary-color-2 ml-1">*</span></label>
                                            <div class="form-group">
                                                <div class="sort-ordering user-form-short">
                                                    <select class="sort-ordering-select browser-default" name="course_type" required>
                                                        <option value="online">Online</option>
                                                        <option value="offline" selected="">Offline</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="col-lg-6">

                                        <div class="input-box">
                                            <label class="label-text">City Name<span class="primary-color-2 ml-1">*</span></label>
                                            <div class="form-group">
                                                <input class="form-control" type="text" name="course_city_name" placeholder="Type City" required>
                                                <span class="la la-file-text-o input-icon"></span>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-lg-6 ">
                                        <div class="input-box">
                                            <label class="label-text">Course Price<span class="primary-color-2 ml-1">*</span></label>
                                            <div class="form-group">
                                                <div class="row">

                                                    <div class="col-lg-3 col-sm-6">
                                                        <div class="sort-ordering user-form-short">
                                                            <select class=" col sort-ordering-select browser-default" name="course_currency" required>
                                                                <option value="$">$ Doller</option>
                                                                <option value="¥,">¥ Yen</option>
                                                                <option value="€,"> € Euro</option>
                                                                <option value="£">£ Pound</option>

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-9 col-sm-6">
                                                        <input class=" form-control" type="number" name="course_price" placeholder="e.g 79" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-6">
                                        <label class="label-text">Course language<i class="la la-question ml-1 tip" data-toggle="tooltip" data-placement="top" title="Separated by commas"></i></label>
                                        <div class="sort-ordering user-form-short">
                                            <select class="sort-ordering-select" name="course_language" required>
                                                <option value="select-a-category" selected="">Select a language</option>
                                                <?php foreach ($languages as $language) : ?>
                                                    <option value="<?php echo $language['language_name']; ?>"><?php echo $language['language_name']; ?>/<?php echo $language['native_name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="input-box">
                                            <label class="label-text">Are there any course requirement or perquisites <span class="primary-color-2 ml-1">*</span></label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="course_requirement[]" placeholder="Example: Be able to read sheet music" required>
                                                <span class="la la-pencil input-icon"></span>

                                            </div>
                                        </div>
                                        <div id="dynamic_field" class="input-box">

                                        </div>

                                        <a id="add_row" style="color: #15bbd3;cursor: pointer;"><span class="la la-plus input-icon" style="margin: 0 7px 0 5px;"></span>Add an answer</a>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="input-box">
                                            <label class="label-text">Who this course is for <span class="primary-color-2 ml-1">*</span></label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="course_for[]" placeholder="Example: this course for" required>
                                                <span class="la la-pencil input-icon"></span>

                                            </div>
                                        </div>
                                        <div id="dynamic_fields" class="input-box">

                                        </div>

                                        <a id="add_rows" style="color: #15bbd3;cursor: pointer;"><span class="la la-plus input-icon" style="margin: 0 7px 0 5px;"></span>Add course for</a>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="input-box">
                                            <label class="label-text">Course Description<span class="primary-color-2 ml-1">*</span></label>
                                            <div class="form-group">
                                                <textarea class="message-control form-control" name="course_full_desc" placeholder="Write course description here" required></textarea>
                                                <span class="la la-pencil input-icon"></span>
                                            </div>
                                        </div>
                                    </div>




                                    <div class="col-lg-12">
                                        <div class="input-box">
                                            <label class="label-text">Course picture<span class="primary-color-2 ml-1">*</span></label>
                                            <div class="form-group mb-0">
                                                <div class="upload-btn-box course-photo-btn">
                                                    <input type="file" name="files[]" class="filer_input" multiple="multiple" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <button class="theme-btn" style="float:right;" type="submit">submit now</button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var finaltotal = 0;
        var i = 1;
        $('#add_row').click(function() {
            i++;
            $('#dynamic_field').append('<div id="row' + i + '" class="form-group"><input type="text" class="form-control" name="course_requirement[]" value="" placeholder="Example: Be able to read sheet music"required ><a name="remove" id="' + i +
                '" class=" btn_remove"><span class="la la-trash input-icon"></span></a></div>');

        });
        $(document).on('click', '.btn_remove', function() {
            var button_id = $(this).attr("id");

            $('#row' + button_id + '').remove();
        });
    });
</script>
<script>
    $(document).ready(function() {
        var finaltotal = 0;
        var i = 1;
        $('#add_rows').click(function() {
            i++;
            $('#dynamic_fields').append('<div id="row' + i + '" class="form-group"><input type="text" class="form-control" name="course_for[]" value="" placeholder="Example:this course for"required ><a name="remove" id="' + i +
                '" class=" btn_remove"><span class="la la-trash input-icon"></span></a></div>');

        });
        $(document).on('click', '.btn_remove', function() {
            var button_id = $(this).attr("id");

            $('#rows' + button_id + '').remove();
        });
    });
</script>

<script>
    function getsubcat(courseid) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>users/get_subcat/" + courseid,
            data: 'country_id=' + courseid,
            success: function(data) {
                // alert(data);
                $("#course_subcat_id").html(data);
                $("#course_subcat_id").selectpicker('refresh');
            }
        });
    }
</script>




<script>
    function getstates(country_id) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>users/get_states_by_countryid/" + country_id,
            data: 'country_id=' + country_id,
            success: function(data) {
                // alert(data);
                $("#states").html(data);
                $("#states").selectpicker('refresh');
            }
        });
    }
</script>
<script>
    function getcity(state_id) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>users/get_cities_by_stateid/" + state_id,
            data: 'state_id=' + state_id,
            success: function(data) {
                // alert(data);
                $("#city").html(data);
                $("#city").selectpicker('refresh');

            }
        });
    }
</script>
<div class="dashboard-content-wrap">
        <div class="container-fluid">
     
            <div class="row mt-5">
                <div class="col-lg-12">
                    <div class="card-box-shared">
                        <div class="card-box-shared-title">
                            <h3 class="widget-title">My Balance</h3>
                        </div>
                        <div class="card-box-shared-body">
                            <div class="balance-info">
                                <ul class="list-items pb-5">
                                    <li><h3 class="widget-title font-size-18">Current Balance</h3></li>
                                    <li>Your current balance is <span class="primary-color font-weight-semi-bold">$219.20 </span> ready to withdraw</li>
                                    <li><a href="#" class="badge btn-primary p-1 text-white">Make a withdraw</a></li>
                                    <li>You will get paid by <span class="primary-color">Bank Transfer</span>, You can change your <a href="#" class="primary-color-2">withdraw preference </a></li>
                                </ul>
                                 <ul class="list-items pb-5">
                                    <li><h3 class="widget-title font-size-18">Pending withdrawals</h3></li>
                                    <li>No withdrawals pending yet.</li>
                                </ul>
                            </div>
                            <div class="statement-table withdraw-table table-responsive mb-5">
                                <div class="statement-header pb-4">
                                    <h3 class="widget-title font-size-18">Completed Withdrawals</h3>
                                </div>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Withdraw Method</th>
                                        <th scope="col">Created at</th>
                                        <th scope="col">Approved at</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">
                                                <div class="statement-info withdraw-info">
                                                    <ul class="list-items">
                                                        <li>$79.00</li>
                                                        <li>$55.00</li>
                                                    </ul>
                                                </div>
                                            </th>
                                            <td>
                                                <div class="statement-info withdraw-info">
                                                    <ul class="list-items">
                                                        <li>Bank Transfer</li>
                                                        <li>Paypal</li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="statement-info withdraw-info">
                                                    <ul class="list-items">
                                                        <li>July 12, 2019 12:20pm</li>
                                                        <li>July 6, 2019 11:20am</li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="statement-info withdraw-info">
                                                    <ul class="list-items">
                                                        <li>July 12, 2019 1:20pm</li>
                                                        <li>July 6, 2019 12:20am</li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">
                                                <div class="statement-info withdraw-info">
                                                    <ul class="list-items">
                                                        <li>$79.00</li>
                                                        <li>$55.00</li>
                                                    </ul>
                                                </div>
                                            </th>
                                            <td>
                                                <div class="statement-info withdraw-info">
                                                    <ul class="list-items">
                                                        <li>Bank Transfer</li>
                                                        <li>PayPal</li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="statement-info withdraw-info">
                                                    <ul class="list-items">
                                                        <li>July 12, 2019 12:20pm</li>
                                                        <li>July 6, 2019 11:20am</li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="statement-info withdraw-info">
                                                    <ul class="list-items">
                                                        <li>July 12, 2019 1:20pm</li>
                                                        <li>July 6, 2019 12:20am</li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="statement-table withdraw-table table-responsive">
                                <div class="statement-header pb-4">
                                    <h3 class="widget-title font-size-18">Rejected Withdrawals</h3>
                                </div>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Withdraw Method</th>
                                        <th scope="col">Created at</th>
                                        <th scope="col">Approved at</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">
                                            <div class="statement-info withdraw-info">
                                                <ul class="list-items">
                                                    <li>$79.00</li>
                                                    <li>$55.00</li>
                                                </ul>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="statement-info withdraw-info">
                                                <ul class="list-items">
                                                    <li>Bank Transfer</li>
                                                    <li>PayPal</li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="statement-info withdraw-info">
                                                <ul class="list-items">
                                                    <li>July 12, 2019 12:20pm</li>
                                                    <li>July 6, 2019 11:20am</li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="statement-info withdraw-info">
                                                <ul class="list-items">
                                                    <li>July 12, 2019 1:20pm</li>
                                                    <li>July 6, 2019 12:20am</li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            <div class="statement-info withdraw-info">
                                                <ul class="list-items">
                                                    <li>$79.00</li>
                                                    <li>$55.00</li>
                                                </ul>
                                            </div>
                                        </th>
                                        <td>
                                            <div class="statement-info withdraw-info">
                                                <ul class="list-items">
                                                    <li>Bank Transfer</li>
                                                    <li>Paypal</li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="statement-info withdraw-info">
                                                <ul class="list-items">
                                                    <li>July 12, 2019 12:20pm</li>
                                                    <li>July 6, 2019 11:20am</li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="statement-info withdraw-info">
                                                <ul class="list-items">
                                                    <li>July 12, 2019 1:20pm</li>
                                                    <li>July 6, 2019 12:20am</li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-lg-12 -->
            </div><!-- end row -->
            
        </div><!-- end container-fluid -->
    </div><!-- end dashboard-content-wrap -->
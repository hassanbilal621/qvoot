<section class="breadcrumb-area" style="height: 180px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-content">
                    <div class="section-heading">
                        <h2 class="section__title">Blogs</h2>
                    </div>
                    <ul class="breadcrumb__list">
                        <li class="active__list-item"><a href="<?php echo base_url(); ?>users/index">index</a></li>
                        <li>Blogs</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="blog-grid section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 column-td-half">
                <div class="card-item blog-card">
                    <div class="card-image">
                        <a href="blog-single.html" class="card__img"><img src="<?php echo base_url(); ?>assets/qvoot/images/img9.jpg" alt=""></a>
                        <div class="card-badge">
                            <span class="badge-label">24 Jan</span>
                        </div>
                    </div>
                    <div class="card-content">
                        <h3 class="card__title mt-0">
                            <a href="blog-single.html">It’s Great the Government is Tightening Gambling</a>
                        </h3>
                        <div class="card-action">
                            <ul class="card-duration d-flex align-items-center">
                                <li>By<a href="#" class="blog-admin-name">TechyDevs</a></li>
                                <li><span class="blog__panel-comment">4 Comments</span></li>
                                <li><span class="blog__panel-likes">130 Likes</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
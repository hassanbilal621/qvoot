<section class="sign-up section--padding regbg" style="padding: 15px 0px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 mx-auto">
                <div class="card-box-shared" style="background-color: rgba(255, 255, 255, .8);">
                    <?php if ($this->session->flashdata('email_false')) : ?>
                        <div class="alert alert-warning alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Warning!</strong> Your Email is Already Registered. Please Resubmit Your Form with another email Thank You.
                        </div>
                    <?php endif; ?>
                    <?php if ($this->session->flashdata('field_missing')) : ?>
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" id="closeicon" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Danger!</strong> You Are Missing Some Important Felids. Please Resubmit Your Form Thank You.
                        </div>
                    <?php endif; ?>
                    <div class="card-box-shared-title text-center">
                        <a href="<?php echo base_url(); ?>" class="logo"><img src="<?php echo base_url() ?>assets/qvoot.png" alt="logo" width="200px"></a>

                        <h3 class="widget-title font-size-25">Create an Account and Start Learning!</h3>
                    </div>
                    <div class="card-box-shared-body">
                        <div class="contact-form-action">
                            <?php echo form_open('users/registeration') ?>
                            <div class="row">
                                <div class="col-lg-6 ">
                                    <div class="input-box">
                                        <label class="label-text">First Name<span class="primary-color-2 ml-1">*</span></label>
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="first_name" placeholder="First Name" required>
                                            <span class="la la-user input-icon"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 ">
                                    <div class="input-box">
                                        <label class="label-text">Last Name<span class="primary-color-2 ml-1">*</span></label>
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="last_name" placeholder="Last Name" required>
                                            <span class="la la-user input-icon"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="input-box">
                                        <label class="label-text">Email Address<span class="primary-color-2 ml-1">*</span></label>
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="user_email" placeholder="Email address" required>
                                            <span class="la la-envelope input-icon"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="input-box">
                                        <label class="label-text">Password<span class="primary-color-2 ml-1">*</span></label>
                                        <div class="form-group">
                                            <input class="form-control" type="password" name="user_password" placeholder="Password" required>
                                            <span class="la la-lock input-icon"></span>
                                        </div>
                                    </div>
                                </div>

                                <!-- <div class="col-lg-6">
                                    <div class="input-box">
                                        <label class="label-text">Select Country<span class="primary-color-2 ml-1">*</span></label>
                                        <div class="form-group">
                                            <div class="sort-ordering user-form-short">
                                                <select class="sort-ordering-select browser-default" name="user_country_id" onchange="getstates(this.value)" required>
                                                    <option value="select-a-category" selected="">Select a country</option>
                                                    <?php foreach ($countries as $country) : ?>
                                                        <option value="<?php echo $country['country_id']; ?>"><?php echo $country['country_name']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="input-box">
                                        <label class="label-text">Select state<span class="primary-color-2 ml-1">*</span></label>
                                        <div class="form-group">
                                            <div class="sort-ordering user-form-short">
                                                <select class="sort-ordering-select browser-default" name="user_state_id" onchange="getcity(this.value)" id="states" required>
                                                    <option disabled selected value="">Select Country First</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="input-box">
                                        <label class="label-text">Select City<span class="primary-color-2 ml-1">*</span></label>
                                        <div class="form-group">
                                            <div class="sort-ordering user-form-short">
                                                <select class="sort-ordering-select browser-default" name="user_city_id" id="city" required>
                                                    <option disabled selected value="">Select States First</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="input-box">
                                        <label class="label-text">Select Gender<span class="primary-color-2 ml-1">*</span></label>
                                        <div class="form-group">
                                            <div class="sort-ordering user-form-short">
                                                <select class="sort-ordering-select browser-default" name="user_gender" required>
                                                    <option value="male">Male</option>
                                                    <option value="female">Female</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="input-box">
                                        <label class="label-text">Enter Steet Name OR No<span class="primary-color-2 ml-1">*</span></label>
                                        <input class="form-control" type="text" name="user_street_num_name" placeholder="Enter Steet Name OR No" style="padding:10px;" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="input-box">
                                        <label class="label-text">Enter Postal Code<span class="primary-color-2 ml-1">*</span></label>
                                        <input class="form-control" type="number" name="user_postal_code" placeholder="Enter Postal Code" style="padding:10px;" required>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="input-box">
                                        <label class="label-text">Enter Address<span class="primary-color-2 ml-1">*</span></label>
                                        <textarea name="user_address" cols="38" rows="12" style="margin-top: 0px;margin-bottom: 0px;height: 135px;resize: none;padding: 10px;" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12" style="margin-top: 7px;">
                                    <div class="form-group">
                                        <div class="custom-checkbox">
                                            <input type="checkbox" id="chb1" require>
                                            <label for="chb1"><span class="line-height-24 d-block">Yes! I want to get the most out of Qvoot by receiving emails with exclusive deals, personal recommendations and learning tips!</span></label>
                                        </div>
                                        <div class="custom-checkbox">
                                            <input type="checkbox" id="chb2" require>
                                            <label for="chb2">By signing up, you agree to our <a href="<?php echo base_url(); ?>users/term_of_use">Terms of Use</a> and
                                                <a href="<?php echo base_url(); ?>users/privacy_policy">Privacy Policy</a>.
                                            </label>
                                        </div>
                                    </div>
                                </div> -->
                                <br>

                                <div class="col-lg-12 mt-2">
                                    <div class="btn-box">
                                        <button class="theme-btn col-lg-12" type="submit">register account</button>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <p class="mt-4">Already have an account? <a href="<?php echo base_url(); ?>users/login" class="primary-color-2">Log in</a></p>
                                </div>
                            </div>
                           <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<script>
    function getstates(country_id) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>users/get_states_by_countryid/" + country_id,
            data: 'country_id=' + country_id,
            success: function(data) {
                alert(data);
                $("#states").html(data);
            }
        });
    }
</script>
<script>
    function getcity(state_id) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url(); ?>users/get_cities_by_stateid/" + state_id,
            data: 'state_id=' + state_id,
            success: function(data) {
                alert(data);
                $("#city").html(data);
            }
        });
    }
</script>

<script src="<?php echo base_url(); ?>assets/app-assets/vendors/formatter/jquery.formatter.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/jquery-3.4.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/bootstrap-select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/magnific-popup.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/isotope.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/waypoint.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/jquery.counterup.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/fancybox.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/wow.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/smooth-scrolling.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/progress-bar.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/date-time-picker.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/emojionearea.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/animated-skills.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/jquery.filer.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/tooltipster.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/main.js"></script>
</body>

</html>
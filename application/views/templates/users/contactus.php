<section class="breadcrumb-area" style="height: 180px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-content">
                    <div class="section-heading">
                        <h2 class="section__title">Contact US</h2>
                    </div>
                    <ul class="breadcrumb__list">
                        <li class="active__list-item"><a href="<?php echo base_url(); ?>users/index">index</a></li>
                        <li>Contact US</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="contact-area padding-bottom-100px">
    <div class="container">

        <div class="contact-form-wrap pt-5">
            <div class="row">
                <center>

                    <?php if (isset($_GET['success'])) : ?>
                        <div class="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>Success!</strong> Thank you for submiting your inquiry, our team will get to you soon. .
                        </div>
                    <?php endif; ?>

                </center>

            </div>
            <div class="row">
                <div class="col-lg-5">
                    <div class="section-heading">
                        <p class="section__meta">Contact us</p>
                        <h2 class="section__title">Do You Have A Query?</h2>
                        <h2 class="section__title">We’ll Love to Hear from You</h2>
                        <span class="section-divider"></span>
                        <p class="section__desc">
                            Whether you have a query about Qvoot features, trials, demo class, pricing or any other information; our quick response team is ready to answer you fast.
                            Reach us either at our Email : <a href="#">info@qvoot.com</a> or use live chat support for instant help.
                        </p>

                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="contact-form-action">
                        <?php $attributes = ['id' => 'myform1'];
                        echo form_open('users/contact', $attributes); ?>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="input-box">
                                    <label class="label-text">Your Name<span class="primary-color-2 ml-1">*</span></label>
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="contact_us_name" placeholder="Your name">
                                        <span class="la la-user input-icon"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="input-box">
                                    <label class="label-text">Your Email<span class="primary-color-2 ml-1">*</span></label>
                                    <div class="form-group">
                                        <input class="form-control" type="email" name="contact_us_email " placeholder="Your email">
                                        <span class="la la-envelope input-icon"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="input-box">
                                    <label class="label-text">Phone Number<span class="primary-color-2 ml-1">*</span></label>
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="contact_us_phone" placeholder="Phone number">
                                        <span class="la la-phone input-icon"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="input-box">
                                    <label class="label-text">Subject<span class="primary-color-2 ml-1">*</span></label>
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="contact_us_subject" placeholder="Reason for contact">
                                        <span class="la la-book input-icon"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="input-box">
                                    <label class="label-text">Message<span class="primary-color-2 ml-1">*</span></label>
                                    <div class="form-group">
                                        <textarea class="message-control form-control" name="contact_us_massage" placeholder="Write message"></textarea>
                                        <span class="la la-pencil input-icon"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <button class="theme-btn" id="submitbuttonss" type="submit">Send Message</button>
                            </div>
                        </div>
                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $("#submitbuttonss").click(function() {
        $("#myform1").submit();
    });
</script>
 <body>
    <section class="login-area section--padding loginbg" style="padding-top: 74px;">
       <div class="container">
          <div class="row">
             <div class="col-lg-6">
                <div class="card-box-shared" style="background-color: rgba(255, 255, 255, .8);">
                   <?php if ($this->session->flashdata('login_failed')) :
                     ?>
                      <div class="alert alert-danger alert-dismissible">
                         <strong>Warning!</strong> Credentials is invalid. Incorrect username or password.
                      </div>
                   <?php endif; ?>
                   <?php if ($this->session->flashdata('field_missing')) : ?>
                      <div class="alert alert-warning alert-dismissible">
                         <strong>Missing!</strong> You Are Missing Some Important Felids. Please Resubmit Your Form Thank You.
                      </div>
                   <?php endif; ?>
                   <?php if ($this->session->flashdata('user_loggedout')) : ?>
                      <div class="alert alert-danger alert-dismissible">
                         <strong>Log Out!</strong> You Are Logout.
                      </div>
                   <?php endif; ?>
                   <?php if ($this->session->flashdata('registered')) : ?>
                      <div class="alert alert-success alert-dismissible">
                         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                         <strong>Success!</strong> You Are Successfully Registered .
                      </div>
                   <?php endif; ?>
                   <div class="card-box-shared-title text-center">
                      <h3 class="widget-title font-size-25">Login to Your Account!</h3>
                   </div>
                   <div class="card-box-shared-body">
                      <div class="contact-form-action">
                         <?php echo form_open('users/login_user') ?>
                         <div class="row">
                            <div class="col-lg-12">
                               <div class="input-box">
                                  <label class="label-text">Email Address<span class="primary-color-2 ml-1">*</span></label>
                                  <div class="form-group">
                                     <input class="form-control" type="email" name="user_email" placeholder="Email address">
                                     <span class="la la-envelope input-icon"></span>
                                  </div>
                               </div>
                            </div>
                            <div class="col-lg-12">
                               <div class="input-box">
                                  <label class="label-text">Password<span class="primary-color-2 ml-1">*</span></label>
                                  <div class="form-group">
                                     <input class="form-control" type="password" name="user_password" placeholder="Password">
                                     <span class="la la-lock input-icon"></span>
                                  </div>
                               </div>
                            </div>
                            <div class="col-lg-12">
                               <div class="form-group">
                                  <div class="custom-checkbox d-flex justify-content-between">

                                     <a href="<?php echo base_url(); ?>users/forgotpassword" class="primary-color-2"> Forgot my password?</a>
                                     <p>Don't have an account? <a href="<?php echo base_url(); ?>users/register" class="primary-color-2">Register</a></p>
                                  </div>
                               </div>
                            </div>
                            <div class="col-lg-12 ">
                               <div class="btn-box">
                                  <button class="theme-btn col-12" type="submit">login account</button>
                               </div>
                            </div>
                         </div>
                        <?php echo form_close(); ?>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </section>

 </body>

 </html>
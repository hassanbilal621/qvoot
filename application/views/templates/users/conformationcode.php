<section class="breadcrumb-area" style="height: 180px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-content">
                    <div class="section-heading">
                        <h2 class="section__title">Confirmation Code</h2>
                    </div>
                    <ul class="breadcrumb__list">
                        <li class="active__list-item"><a href="<?php echo base_url(); ?>users/login">login</a></li>
                        <li>Conformation Code</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="recover-area section--padding forgetbg" style="padding: 15px 0;">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 mx-auto">
                <div class="card-box-shared" style="background-color: rgba(255, 255, 255, .8);">
                    <div class="card-box-shared-title">
                        <h3 class="widget-title font-size-25 pb-2">Reset Password!</h3>
                        <p class="line-height-26">
                            Enter the email of your account to reset password.
                            Then you will receive a link to email to reset the
                            password.If you have any issue about reset password <a href="users/contactus" class="primary-color-2">contact us</a>
                        </p>
                    </div>
                    <div class="card-box-shared-body">
                        <div class="contact-form-action">
                            <?php echo form_open('users/conform') ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="input-box">
                                        <label class="label-text">Conformation Code <span class="primary-color-2 ml-1">*</span></label>
                                        <div class="form-group">
                                            <input type="hidden" name="user_email" value="<?php echo $useremail; ?>" />
                                            <input class="form-control" type="conformationcode" name="conformationcode" placeholder="Enter Conformation Code">
                                            <span class="la la-envelope input-icon"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <button class="theme-btn" type="submit">Conform</button>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <p><a href="<?php echo base_url(); ?>users/login" class="primary-color-2">Login</a></p>
                                </div>
                                <div class="col-lg-6">
                                    <p class="text-right register-text">Not a member? <a href="<?php echo base_url(); ?>users/register" class="primary-color-2">Register</a></p>

                                </div>
                            </div>
                           <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</body>

</html>
<section class="breadcrumb-area" style="height: 180px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-content">
                    <div class="section-heading">
                        <h2 class="section__title">how It Works</h2>
                    </div>
                    <ul class="breadcrumb__list">
                        <li class="active__list-item"><a href="<?php echo base_url(); ?>users/index">index</a></li>
                        <li>how It Works</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="about-area padding-top-120px padding-bottom-110px">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="about-content-box pt-0">
                    <div class="section-heading">
                        <h2 class="section__title line-height-50" style="display: table;margin: 0px 0 -8px 0;">How Does It Work?</h2>
                        <span class="section-divider"></span>
                        <p class="section__desc mb-4">Qvoot is a platform to develop body, mind, and soul integrated. We believe physical health is not everything. For a healthy living, you need a healthy brain and a decent soul. You need to improve your lifestyle as well. We train you up in all these verticals. Our qualified dietician will train you to improve physical health through a decent diet with compact nutritious value based on your physical health. They will also train you up yoga and meditations along with other training that raises spirituality inside to enhance the consciousness of the soul. The combination of physical health, sharp brainpower and internal spirituality can build a happy life. We endeavor to elevate you on that level.</p>
                    </div>
                    <div class="section-heading">
                        <h2 class="section__title line-height-50" style="display: table;margin: 0px 0 -8px 0;">Our Trainers’ Quality</h2>
                        <span class="section-divider"></span>
                        <p class="section__desc mb-4">Everyone is not our trainers. We select only the qualified, well-experienced, and well-reputed trainers when they pass eligible criteria. So, all the trainees can rely on the Qvoot platform to get a qualified and like-minded trainer for their online or offline training sessions. You will be paid from the Qvoot platform once you are selected. </p>
                    </div>
                    <div class="section-heading">
                        <h2 class="section__title line-height-50" style="display: table;margin: 0px 0 -8px 0;">What Our Trainees Achieve </h2>
                        <span class="section-divider"></span>
                        <p class="section__desc mb-4">Whoever you are, regardless of age and physical type, you can be our trainee. You know health is wealth. Your skill is your future, which develops a futuristic lifestyle. We can enhance your lifestyle by developing your thought-process, wisdom, creativity (art and craft), and the sense of completeness. Apart from skill development training, we train yoga, meditation, and other training that will develop your creative-self. You have to bear a little amount once you participate in the training session. </p>
                    </div>

                    <div class="section-heading">
                        <h2 class="section__title line-height-50" style="display: table;margin: 0px 0 -8px 0;">How Can You Communicate?</h2>
                        <span class="section-divider"></span>
                        <p class="section__desc mb-4">If you are a trainee, you will get the trainers' profile on our Qvoot platform/ website. You can choose one of them for the next training session. You have the opportunity to communicate through the messenger interlinked with the website. Once you register with us, you will get full access to communicate through the Qvoot platform. The registration with us is completely free, safe, and secure from the data breach. You can manage your practice time and schedule, or participate in our pre-scheduled training sessions. Our trainers will inform the nearest location or you can access to our studio. </p>
                    </div>
                    <div class="section-heading">
                        <h2 class="section__title line-height-50" style="display: table;margin: 0px 0 -8px 0;">Payment Process</h2>
                        <span class="section-divider"></span>
                        <p class="section__desc mb-4">You have to pay us online. Our payment gateway is secured by SSL certification. For participating in an online or offline class, you have to pay online through credit or debit cards or PayPal. If you do not want to continue our courses, there is also an easy refund policy. See Qvoot Refund Policy for details. However, nobody has taken a refund still now because they are utterly satisfied with us.</p>
                        <p class="section__desc mb-4">You are now a new member of our Qvoot platform! It is time to experience excellence!</p>

                    </div>

                </div>
            </div>
        </div>
</section>
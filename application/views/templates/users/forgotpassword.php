<section class="breadcrumb-area" style="height: 180px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <?php if ($this->session->flashdata('wrong_email')) : ?>
                        <div class="alert alert-danger alert-dismissible">
                            <strong>Alert!</strong> Your email does not exisit into database.
                        </div>
                    <?php endif; ?>
                <div class="breadcrumb-content">
                    <div class="section-heading">
                        <h2 class="section__title">Forgot Password</h2>
                    </div>
                    <ul class="breadcrumb__list">
                        <li class="active__list-item"><a href="<?php echo base_url(); ?>users/login">login</a></li>
                        <li>Forgot Password</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="recover-area section--padding forgetbg" style="padding: 15px 0;">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 mx-auto">
                <div class="card-box-shared" style="background-color: rgba(255, 255, 255, .8);">
                    <div class="card-box-shared-title">
                        <h3 class="widget-title font-size-25 pb-2">Reset Password!</h3>
                        <p class="line-height-26">
                            Enter the email of your account to reset password.
                            Then you will receive a link to email to reset the
                            password.If you have any issue about reset password <a href="<?php echo base_url(); ?>users/contactus" class="primary-color-2">Contact us</a>
                        </p>
                    </div>
                    <div class="card-box-shared-body">
                        <div class="contact-form-action">
                            <?php echo form_open('users/checkingemail') ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="input-box">
                                        <label class="label-text">Email Address<span class="primary-color-2 ml-1">*</span></label>
                                        <div class="form-group">
                                            <input class="form-control" type="email" name="user_email" placeholder="Enter email address" required>
                                            <span class="la la-envelope input-icon"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <button class="theme-btn" type="submit">reset password</button>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <p><a href="<?php echo base_url(); ?>users/login" class="primary-color-2">Login</a></p>
                                </div>
                                <div class="col-lg-6">
                                    <p class="text-right register-text">Not a member? <a href="<?php echo base_url(); ?>users/register" class="primary-color-2">Register</a></p>

                                </div>
                            </div>
                           <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</body>

</html>
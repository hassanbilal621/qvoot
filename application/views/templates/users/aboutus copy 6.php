<section class="breadcrumb-area" style="height: 180px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-content">
                    <div class="section-heading">
                        <h2 class="section__title">Forgot Password</h2>
                    </div>
                    <ul class="breadcrumb__list">
                        <li class="active__list-item"><a href="<?php echo base_url(); ?>users/index">index</a></li>
                        <li>Forgot Password</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<ul class="cat-dropdown-menu">
                                            <?php foreach ($categories as $category) : ?>
                                                <li>
                                                    <a href="course-grid.html"><?php echo $category['category_name'] ?> <i class="la la-angle-right"></i></a>
                                                    <?php if ($category['category_id'] = '1') { ?>
                                                        <ul class="sub-menu">

                                                            <li><a href="#">Yoga</a></li>
                                                            <li><a href="#">Pilates</a></li>
                                                            <li><a href="#">Martial Arts</a></li>
                                                            <li><a href="#">Dancing</a></li>
                                                        </ul>
                                                    <?php  } ?>
                                                    <?php if ($category['category_id'] = '2') { ?>
                                                        <ul class="sub-menu">
                                                            <li><a href="#">Therapist</a></li>
                                                            <li><a href="#">Dietitian</a></li>
                                                            <li><a href="#">Nutrition</a></li>
                                                        </ul>
                                                    <?php  } ?>
                                                    <?php if ($category['category_id'] = '3') { ?>
                                                        <ul class="sub-menu">
                                                            <li><a href="#">Designer</a></li>
                                                            <li><a href="#">Makeup Artist</a></li>
                                                            <li><a href="#">Cooking</a></li>
                                                        </ul>
                                                    <?php  } ?>
                                                    <?php if ($category['category_id'] = '4') { ?>
                                                        <ul class="sub-menu">
                                                            <li><a href="#">Life coach</a></li>
                                                            <li><a href="#">Leadership</a></li>
                                                            <li><a href="#">Entrepreneurship</a></li>
                                                        </ul>
                                                    <?php  } ?>

                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
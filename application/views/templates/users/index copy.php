<body>



<?php $this->load->view('templates/users/navbar.php'); ?>

<section class="slider-area">
    <div class="single-slide-item single-slide-item-3 slide-bg5">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-7">
                    <div class="hero-heading mr-4">
                        <div class="section-heading">
                            <h2 class="section__title">Take the First Step of Your Journey to Success</h2>
                            <p class="section__desc">
                                Who tells you can’t do it? Build the confidence, skills, and qualifications to take control of your future.
                                Start one of our 1000 high quality courses from the world’s leading experts today!
                            </p>
                        </div>
                        <div class="hero-search-form">
                            <div class="contact-form-action">
                                <form method="post">
                                    <div class="input-box">
                                        <label class="label-text section__meta section__metalight">What do you want to learn?</label>
                                        <div class="form-group mb-0">
                                            <input class="form-control" type="text" name="search" placeholder="Search courses by keywords">
                                            <span class="la la-search search-icon"></span>
                                        </div>
                                    </div><!-- end input-box -->
                                </form>
                            </div><!-- end contact-form-action -->
                        </div>
                    </div>
                </div><!-- col-lg-7 -->
                <div class="col-lg-5">
                    <div class="hero-category">
                        <div class="row">
                            <div class="col-lg-6 column-td-half">
                                <div class="category-item category-item-layout-2 category--item-layout-2">
                                    <a href="#" class="category-content">
                                        <i class="la la-desktop icon-element"></i>
                                        <h3 class="cat__title">Fitness</h3>
                                    </a><!-- end category-content -->
                                </div><!-- end category-item -->
                            </div><!-- col-lg-4 -->
                            <div class="col-lg-6 column-td-half">
                                <div class="category-item category-item-layout-2 category--item-layout-2">
                                    <a href="#" class="category-content">
                                        <i class="la la-briefcase icon-element"></i>
                                        <h3 class="cat__title">Health</h3>
                                    </a><!-- end category-content -->
                                </div><!-- end category-item -->
                            </div><!-- end col-lg-4 -->
                            <div class="col-lg-6 column-td-half">
                                <div class="category-item category-item-layout-2 category--item-layout-2">
                                    <a href="#" class="category-content">
                                        <i class="la la-paint-brush icon-element"></i>
                                        <h3 class="cat__title">LifeStyle</h3>
                                    </a><!-- end category-content -->
                                </div><!-- end category-item -->
                            </div><!-- end col-lg-4 -->
                            <div class="col-lg-6 column-td-half">
                                <div class="category-item category-item-layout-2 category--item-layout-2">
                                    <a href="#" class="category-content">
                                        <i class="la la-laptop icon-element"></i>
                                        <h3 class="cat__title">Personal Growth</h3>
                                    </a><!-- end category-content -->
                                </div><!-- end category-item -->
                            </div><!-- end col-lg-4 -->
                      
                        </div><!-- row -->
                    </div><!-- hero-category -->
                </div><!-- col-lg-7 -->
            </div><!-- row -->
        </div><!-- container -->
    </div><!-- end single-slide-item -->
</section><!-- end slider-area -->
<!--================================
        END SLIDER AREA
=================================-->

<!--======================================
        START COURSE AREA
======================================-->
<section class="course-area padding-top-120px padding-bottom-90px">
    <div class="course-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-heading text-center">
                        <h5 class="section__meta">Learn on your schedule</h5>
                        <h2 class="section__title">Explore Trending Courses</h2>
                        <span class="section-divider"></span>
                    </div><!-- end section-heading -->
                </div><!-- end col-lg-12 -->
            </div><!-- end row -->
            <div class="row margin-top-28px">
                <div class="col-lg-12">
                    <div class="tab-content">
                        <div class="course-carousel owl-dot-and-nav">
                            <div class="card-item card-preview" data-tooltip-content="#tooltip_content_1">
                                <div class="card-image">
                                    <a href="course-details.html" class="card__img"><img src="<?php echo base_url(); ?>assets/qvoot/images/img9.jpg" alt=""></a>
                                </div><!-- end card-image -->
                                <div class="card-content">
                                    <p class="card__label">
                                        <span class="card__label-text">all levels</span>
                                        <a href="#" class="card__collection-icon" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="la la-heart-o"></span></a>
                                    </p>
                                    <h3 class="card__title">
                                        <a href="course-details.html">User Experience Design - Adobe XD UI UX Design</a>
                                    </h3>
                                    <p class="card__author">
                                        <a href="teacher-detail.html">kamran paul</a>
                                    </p>
                                    <div class="rating-wrap d-flex mt-2 mb-3">
                                        <ul class="review-stars">
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star-o"></span></li>
                                        </ul>
                                        <span class="star-rating-wrap">
                                            <span class="star__rating">4.4</span>
                                            <span class="star__count">(20)</span>
                                        </span>
                                    </div><!-- end rating-wrap -->
                                    <div class="card-action">
                                        <ul class="card-duration d-flex justify-content-between align-items-center">
                                            <li><span class="meta__date"><i class="la la-play-circle"></i> 45 Classes</span></li>
                                            <li><span class="meta__date"><i class="la la-clock-o"></i> 3 hours 20 min</span></li>
                                        </ul>
                                    </div><!-- end card-action -->
                                    <div class="card-price-wrap d-flex justify-content-between align-items-center">
                                        <span class="card__price">Free</span>
                                        <a href="#" class="text-btn">Get Enrolled</a>
                                    </div><!-- end card-price-wrap -->
                                </div><!-- end card-content -->
                            </div><!-- end card-item -->
                            <div class="card-item card-preview" data-tooltip-content="#tooltip_content_2">
                                <div class="card-image">
                                    <a href="course-details.html" class="card__img"><img src="<?php echo base_url(); ?>assets/qvoot/images/img11.jpg" alt=""></a>
                                    <div class="card-badge">
                                        <span class="badge-label">bestseller</span>
                                    </div>
                                </div><!-- end card-image -->
                                <div class="card-content">
                                    <p class="card__label">
                                        <span class="card__label-text">all levels</span>
                                        <a href="#" class="card__collection-icon" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="la la-heart-o"></span></a>
                                    </p>
                                    <h3 class="card__title">
                                        <a href="course-details.html">The Complete Digital finance Marketing Course</a>
                                    </h3>
                                    <p class="card__author">
                                        <a href="teacher-detail.html">jose purtila</a>
                                    </p>
                                    <div class="rating-wrap d-flex mt-2 mb-3">
                                        <ul class="review-stars">
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star-o"></span></li>
                                        </ul>
                                        <span class="star-rating-wrap">
                                            <span class="star__rating">4.2</span>
                                            <span class="star__count">(30)</span>
                                        </span>
                                    </div><!-- end rating-wrap -->
                                    <div class="card-action">
                                        <ul class="card-duration d-flex justify-content-between align-items-center">
                                            <li><span class="meta__date"><i class="la la-play-circle"></i> 45 Classes</span></li>
                                            <li><span class="meta__date"><i class="la la-clock-o"></i> 3 hours 20 min</span></li>
                                        </ul>
                                    </div><!-- end card-action -->
                                    <div class="card-price-wrap d-flex justify-content-between align-items-center">
                                        <span class="card__price"><span class="before-price">$189.00</span> $119.00</span>
                                        <a href="#" class="text-btn">Add to cart</a>
                                    </div><!-- end card-price-wrap -->
                                </div><!-- end card-content -->
                            </div><!-- end card-item -->
                            <div class="card-item card-preview" data-tooltip-content="#tooltip_content_3">
                                <div class="card-image">
                                    <a href="course-details.html" class="card__img"><img src="<?php echo base_url(); ?>assets/qvoot/images/img12.jpg" alt=""></a>
                                </div><!-- end card-image -->
                                <div class="card-content">
                                    <p class="card__label">
                                        <span class="card__label-text">all levels</span>
                                        <a href="#" class="card__collection-icon" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="la la-heart-o"></span></a>
                                    </p>
                                    <h3 class="card__title">
                                        <a href="course-details.html">Complete Python Bootcamp: Go from zero to hero</a>
                                    </h3>
                                    <p class="card__author">
                                        <a href="teacher-detail.html">noelle travesy</a>
                                    </p>
                                    <div class="rating-wrap d-flex mt-2 mb-3">
                                        <ul class="review-stars">
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star-o"></span></li>
                                        </ul>
                                        <span class="star-rating-wrap">
                                            <span class="star__rating">4.5</span>
                                            <span class="star__count">(40)</span>
                                        </span>
                                    </div><!-- end rating-wrap -->
                                    <div class="card-action">
                                        <ul class="card-duration d-flex justify-content-between align-items-center">
                                            <li><span class="meta__date"><i class="la la-play-circle"></i> 45 Classes</span></li>
                                            <li><span class="meta__date"><i class="la la-clock-o"></i> 3 hours 20 min</span></li>
                                        </ul>
                                    </div><!-- end card-action -->
                                    <div class="card-price-wrap d-flex justify-content-between align-items-center">
                                        <span class="card__price">$68.00</span>
                                        <a href="#" class="text-btn">Add to cart</a>
                                    </div><!-- end card-price-wrap -->
                                </div><!-- end card-content -->
                            </div><!-- end card-item -->
                            <div class="card-item card-preview" data-tooltip-content="#tooltip_content_4">
                                <div class="card-image">
                                    <a href="course-details.html" class="card__img"><img src="<?php echo base_url(); ?>assets/qvoot/images/img9.jpg" alt=""></a>
                                </div><!-- end card-image -->
                                <div class="card-content">
                                    <p class="card__label">
                                        <span class="card__label-text">all levels</span>
                                        <a href="#" class="card__collection-icon" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="la la-heart-o"></span></a>
                                    </p>
                                    <h3 class="card__title">
                                        <a href="course-details.html">Complete Python Bootcamp: Go from zero to hero</a>
                                    </h3>
                                    <p class="card__author">
                                        <a href="teacher-detail.html">noelle travesy</a>
                                    </p>
                                    <div class="rating-wrap d-flex mt-2 mb-3">
                                        <ul class="review-stars">
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star-o"></span></li>
                                        </ul>
                                        <span class="star-rating-wrap">
                                            <span class="star__rating">4.5</span>
                                            <span class="star__count">(40)</span>
                                        </span>
                                    </div><!-- end rating-wrap -->
                                    <div class="card-action">
                                        <ul class="card-duration d-flex justify-content-between align-items-center">
                                            <li><span class="meta__date"><i class="la la-play-circle"></i> 45 Classes</span></li>
                                            <li><span class="meta__date"><i class="la la-clock-o"></i> 3 hours 20 min</span></li>
                                        </ul>
                                    </div><!-- end card-action -->
                                    <div class="card-price-wrap d-flex justify-content-between align-items-center">
                                        <span class="card__price">$68.00</span>
                                        <a href="#" class="text-btn">Add to cart</a>
                                    </div><!-- end card-price-wrap -->
                                </div><!-- end card-content -->
                            </div><!-- end card-item -->
                            <div class="card-item card-preview" data-tooltip-content="#tooltip_content_5">
                                <div class="card-image">
                                    <a href="course-details.html" class="card__img"><img src="<?php echo base_url(); ?>assets/qvoot/images/img8.jpg" alt=""></a>
                                    <div class="card-badge">
                                        <span class="badge-label">highest rated</span>
                                    </div>
                                </div><!-- end card-image -->
                                <div class="card-content">
                                    <p class="card__label">
                                        <span class="card__label-text">all levels</span>
                                        <a href="#" class="card__collection-icon" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="la la-heart-o"></span></a>
                                    </p>
                                    <h3 class="card__title">
                                        <a href="course-details.html">Complete Python Bootcamp: Go from zero to hero</a>
                                    </h3>
                                    <p class="card__author">
                                        <a href="teacher-detail.html">noelle travesy</a>
                                    </p>
                                    <div class="rating-wrap d-flex mt-2 mb-3">
                                        <ul class="review-stars">
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star"></span></li>
                                            <li><span class="la la-star-o"></span></li>
                                        </ul>
                                        <span class="star-rating-wrap">
                                            <span class="star__rating">4.5</span>
                                            <span class="star__count">(40)</span>
                                        </span>
                                    </div><!-- end rating-wrap -->
                                    <div class="card-action">
                                        <ul class="card-duration d-flex justify-content-between align-items-center">
                                            <li><span class="meta__date"><i class="la la-play-circle"></i> 45 Classes</span></li>
                                            <li><span class="meta__date"><i class="la la-clock-o"></i> 3 hours 20 min</span></li>
                                        </ul>
                                    </div><!-- end card-action -->
                                    <div class="card-price-wrap d-flex justify-content-between align-items-center">
                                        <span class="card__price">$68.00</span>
                                        <a href="#" class="text-btn">Add to cart</a>
                                    </div><!-- end card-price-wrap -->
                                </div><!-- end card-content -->
                            </div><!-- end card-item -->
                        </div><!-- end course-carousel -->
                    </div><!-- end tab-content -->
                </div><!-- end col-lg-12 -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end course-wrapper -->
</section><!-- end courses-area -->
<!--======================================
        END COURSE AREA
======================================-->

<div class="section-block"></div>

<!--======================================
        START CHOOSE AREA
======================================-->
<section class="choose-area section-padding text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-heading">
                    <h5 class="section__meta">why choose us</h5>
                    <h2 class="section__title">Why Study with Qvoot?</h2>
                    <span class="section-divider"></span>
                </div><!-- end section-heading -->
            </div><!-- end col-md-12 -->
        </div><!-- end row -->
        <div class="row margin-top-100px">
            <div class="col-lg-4 column-td-half">
                <div class="post-card post-card-layout-2">
                    <div class="post-card-content">
                        <img src="<?php echo base_url(); ?>assets/qvoot/images/img33.jpg" alt="" class="img-fluid">
                        <h2 class="widget-title mt-4 mb-2">Learn anything</h2>
                        <p>Lorem ipsum dolor sit amet. Dolorum nesciunt non obcaecati perferendis porro sint tempore veniam</p>
                    </div><!-- end post-card-content -->
                </div>
            </div><!-- end col-lg-4 -->
            <div class="col-lg-4 column-td-half">
                <div class="post-card post-card-layout-2">
                    <div class="post-card-content">
                        <img src="<?php echo base_url(); ?>assets/qvoot/images/img34.jpg" alt="" class="img-fluid">
                        <h2 class="widget-title mt-4 mb-2">Flexible learning</h2>
                        <p>Lorem ipsum dolor sit amet. Dolorum nesciunt non obcaecati perferendis porro sint tempore veniam</p>
                    </div><!-- end post-card-content -->
                </div>
            </div><!-- end col-lg-4 -->
            <div class="col-lg-4 column-td-half">
                <div class="post-card post-card-layout-2">
                    <div class="post-card-content">
                        <img src="<?php echo base_url(); ?>assets/qvoot/images/img35.jpg" alt="" class="img-fluid">
                        <h2 class="widget-title mt-4 mb-2">Learn with experts</h2>
                        <p>Lorem ipsum dolor sit amet. Dolorum nesciunt non obcaecati perferendis porro sint tempore veniam</p>
                    </div><!-- end post-card-content -->
                </div>
            </div><!-- end col-lg-4 -->
        </div><!-- end row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="btn-box mt-3 d-flex align-items-center justify-content-center text-left">
                    <div class="btn-box-inner mr-3">
                        <span class="d-block mb-2">Are you instructor?</span>
                        <a href="#" class="theme-btn line-height-40 text-capitalize">Start teaching</a>
                    </div>
                    <div class="btn-box-inner">
                        <span class="d-block mb-2">Are you student?</span>
                        <a href="#" class="theme-btn line-height-40 text-capitalize">Start learning</a>
                    </div>
                </div>
            </div><!-- end col-lg-12 -->
        </div><!-- end row -->
    </div><!-- end container -->
</section>
<!-- ================================
       START CHOOSE AREA
================================= -->

<!--======================================
        START CATEGORY AREA
======================================-->
<section class="category-area section-bg padding-top-120px padding-bottom-120px text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-heading">
                    <h5 class="section__meta">Categories</h5>
                    <h2 class="section__title">Browse Popular Categories</h2>
                    <span class="section-divider"></span>
                </div><!-- end section-heading -->
            </div><!-- end col-lg-6 -->
        </div><!-- end row -->
        <div class="row margin-top-28px">
            <div class="col-lg-4 column-td-half">
                <div class="flip-box-item">
                    <div class="front">
                        <img src="<?php echo base_url(); ?>assets/qvoot/images/img1.jpg" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <h3 class="cat__title">Web Development</h3>
                                <p class="cat__meta">200 courses</p>
                            </div>
                        </div><!-- end category-item -->
                    </div><!-- end front -->
                    <div class="back">
                        <img src="<?php echo base_url(); ?>assets/qvoot/images/img1.jpg" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <a href="#" class="theme-btn">View Course</a>
                            </div>
                        </div><!-- end category-item -->
                    </div><!-- end back -->
                </div><!-- end flip-box-item -->
            </div><!-- end col-lg-4 -->
            <div class="col-lg-4 column-td-half">
                <div class="flip-box-item">
                    <div class="front">
                        <img src="<?php echo base_url(); ?>assets/qvoot/images/img2.jpg" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <h3 class="cat__title">Business</h3>
                                <p class="cat__meta">689 courses</p>
                            </div>
                        </div><!-- end category-item -->
                    </div><!-- end front -->
                    <div class="back">
                        <img src="<?php echo base_url(); ?>assets/qvoot/images/img2.jpg" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <a href="#" class="theme-btn">View Course</a>
                            </div>
                        </div><!-- end category-item -->
                    </div><!-- end back -->
                </div><!-- end flip-box-item -->
            </div><!-- end col-lg-4 -->
            <div class="col-lg-4 column-td-half">
                <div class="flip-box-item">
                    <div class="front">
                        <img src="<?php echo base_url(); ?>assets/qvoot/images/img3.jpg" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <h3 class="cat__title">Technology</h3>
                                <p class="cat__meta">567 courses</p>
                            </div>
                        </div><!-- end category-item -->
                    </div><!-- end front -->
                    <div class="back">
                        <img src="<?php echo base_url(); ?>assets/qvoot/images/img3.jpg" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <a href="#" class="theme-btn">View Course</a>
                            </div>
                        </div><!-- end category-item -->
                    </div><!-- end back -->
                </div><!-- end flip-box-item -->
            </div><!-- end col-lg-4 -->
            <div class="col-lg-3 column-td-half">
                <div class="flip-box-item">
                    <div class="front">
                        <img src="<?php echo base_url(); ?>assets/qvoot/images/img4.jpg" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <h3 class="cat__title">Marketing</h3>
                                <p class="cat__meta">500 courses</p>
                            </div>
                        </div><!-- end category-item -->
                    </div><!-- end front -->
                    <div class="back">
                        <img src="<?php echo base_url(); ?>assets/qvoot/images/img4.jpg" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <a href="#" class="theme-btn">View Course</a>
                            </div>
                        </div><!-- end category-item -->
                    </div><!-- end back -->
                </div><!-- end flip-box-item -->
            </div><!-- end col-lg-3 -->
             <div class="col-lg-3 column-td-half">
                <div class="flip-box-item">
                    <div class="front">
                        <img src="<?php echo base_url(); ?>assets/qvoot/images/img5.jpg" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <h3 class="cat__title">Health</h3>
                                <p class="cat__meta">356 courses</p>
                            </div>
                        </div><!-- end category-item -->
                    </div><!-- end front -->
                    <div class="back">
                        <img src="<?php echo base_url(); ?>assets/qvoot/images/img5.jpg" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <a href="#" class="theme-btn">View Course</a>
                            </div>
                        </div><!-- end category-item -->
                    </div><!-- end back -->
                </div><!-- end flip-box-item -->
            </div><!-- end col-lg-3 -->
            <div class="col-lg-3 column-td-half">
                <div class="flip-box-item">
                    <div class="front">
                        <img src="<?php echo base_url(); ?>assets/qvoot/images/img6.jpg" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <h3 class="cat__title">Architecture</h3>
                                <p class="cat__meta">500 courses</p>
                            </div>
                        </div><!-- end category-item -->
                    </div><!-- end front -->
                    <div class="back">
                        <img src="<?php echo base_url(); ?>assets/qvoot/images/img6.jpg" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <a href="#" class="theme-btn">View Course</a>
                            </div>
                        </div><!-- end category-item -->
                    </div><!-- end back -->
                </div><!-- end flip-box-item -->
            </div><!-- end col-lg-3 -->
            <div class="col-lg-3 column-td-half">
                <div class="flip-box-item">
                    <div class="front">
                        <img src="<?php echo base_url(); ?>assets/qvoot/images/img8.jpg" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <h3 class="cat__title">Data Science</h3>
                                <p class="cat__meta">356 courses</p>
                            </div>
                        </div><!-- end category-item -->
                    </div><!-- end front -->
                    <div class="back">
                        <img src="<?php echo base_url(); ?>assets/qvoot/images/img8.jpg" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <a href="#" class="theme-btn">View Course</a>
                            </div>
                        </div><!-- end category-item -->
                    </div><!-- end back -->
                </div><!-- end flip-box-item -->
            </div><!-- end col-lg-3 -->
        </div><!-- end row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="btn-box mt-4 text-center">
                    <a href="#" class="theme-btn">browse all Category</a>
                </div><!-- end btn-box -->
            </div><!-- end col-lg-12 -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end category-area -->
<!--======================================
        END CATEGORY AREA
======================================-->

<!-- ================================
       START FUNFACT AREA
================================= -->
<section class="funfact-area text-center overflow-hidden padding-top-85px padding-bottom-85px">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 column-td-half">
                <div class="counter-item">
                    <span class="la la-bullhorn count__icon"></span>
                    <h4 class="count__title counter">4520</h4>
                    <p class="count__meta">expert instructors</p>
                </div><!-- end counter-item -->
            </div><!-- end col-lg-3 -->
            <div class="col-lg-3 column-td-half">
                <div class="counter-item">
                    <span class="la la-globe count__icon"></span>
                    <h4 class="count__title counter text-color">5452</h4>
                    <p class="count__meta">foreign followers</p>
                </div><!-- end counter-item -->
            </div><!-- end col-lg-3 -->
            <div class="col-lg-3 column-td-half">
                <div class="counter-item">
                    <span class="la la-users count__icon"></span>
                    <h4 class="count__title counter text-color-2">9720</h4>
                    <p class="count__meta">students enrolled</p>
                </div><!-- end counter-item -->
            </div><!-- end col-lg-3 -->
            <div class="col-lg-3 column-td-half">
                <div class="counter-item">
                    <span class="la la-certificate count__icon"></span>
                    <h4 class="count__title counter text-color-3">20</h4>
                    <p class="count__meta">years of experience</p>
                </div><!-- end counter-item -->
            </div><!-- end col-lg-3 -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end funfact-area -->
<!-- ================================
       START FUNFACT AREA
================================= -->

<!--======================================
        START GET-START AREA
======================================-->
<section class="get-start-area padding-top-120px padding-bottom-120px text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="get-start-content">
                    <div class="section-heading">
                        <h5 class="section__meta section__metalight">start online learning</h5>
                        <h2 class="section__title text-white font-size-40 line-height-55">
                            Why so late? Start one of our 2000 high quality courses <br>
                            from the world's leading experts today!
                        </h2>
                        <span class="section-divider section-divider-light"></span>
                    </div><!-- end section-heading -->
                    <div class="btn-box margin-top-20px">
                        <a href="#" class="theme-btn theme-btn-hover-light">join for free</a>
                    </div>
                </div><!-- end get-start-content -->
            </div><!-- end col-lg-12 -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end get-start-area -->
<!--======================================
        END GET-START AREA
======================================-->

<!--================================
         START TESTIMONIAL AREA
=================================-->
<section class="testimonial-area section-bg padding-top-120px padding-bottom-110px">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="section-heading">
                    <h5 class="section__meta">testimonials</h5>
                    <h2 class="section__title">From the Qvoot community</h2>
                    <span class="section-divider"></span>
                    <p class="section__desc">
                        Donec vitae orci sed dolor rutrum auctor. Duis arcu tortor, suscipit eget, imperdiet nec
                    </p>
                </div><!-- end section-heading -->
                <div class="btn-box">
                    <a href="about.html" class="theme-btn">explore all</a>
                </div>
            </div><!-- end col-lg-4 -->
            <div class="col-lg-8">
                <div class="testimonial-subtitle pb-3">
                    <h3 class="widget-title font-weight-medium">30+ million people are already learning on Qvoot</h3>
                </div>
                <div class="testimonial-carousel-2">
                    <div class="testimonial-item testimonial-item-layout-2">
                        <div class="testimonial__desc">
                            <p class="testimonial__desc-desc">
                                My children and I LOVE Qvoot! The courses are fantastic and the
                                instructors are so fun and knowledgeable.
                                I only wish we found it sooner.
                            </p>
                        </div><!-- end testimonial__desc -->
                        <div class="testimonial-header">
                            <img src="<?php echo base_url(); ?>assets/qvoot/images/testi-img.jpg" alt="small-avatar">
                            <div class="testimonial__name">
                                <h3 class="testimonial__name-title">Kamran Adi</h3>
                                <span class="testimonial__name-meta">student</span>
                                <ul class="review-stars d-inline-block">
                                    <li><span class="la la-star"></span></li>
                                    <li><span class="la la-star"></span></li>
                                    <li><span class="la la-star"></span></li>
                                    <li><span class="la la-star"></span></li>
                                    <li><span class="la la-star-o"></span></li>
                                </ul>
                            </div>
                        </div><!-- end testimonial-header -->
                    </div><!-- end testimonial-item -->
                    <div class="testimonial-item testimonial-item-layout-2">
                        <div class="testimonial__desc">
                            <p class="testimonial__desc-desc">
                                My children and I LOVE Qvoot! The courses are fantastic and the
                                instructors are so fun and knowledgeable.
                                I only wish we found it sooner.
                            </p>
                        </div><!-- end testimonial__desc -->
                        <div class="testimonial-header">
                            <img src="<?php echo base_url(); ?>assets/qvoot/images/testi-img2.jpg" alt="small-avatar">
                            <div class="testimonial__name">
                                <h3 class="testimonial__name-title">Kevin Martin</h3>
                                <span class="testimonial__name-meta">student</span>
                                <ul class="review-stars d-inline-block">
                                    <li><span class="la la-star"></span></li>
                                    <li><span class="la la-star"></span></li>
                                    <li><span class="la la-star"></span></li>
                                    <li><span class="la la-star"></span></li>
                                    <li><span class="la la-star-o"></span></li>
                                </ul>
                            </div>
                        </div><!-- end testimonial-header -->
                    </div><!-- end testimonial-item -->
                    <div class="testimonial-item testimonial-item-layout-2">
                        <div class="testimonial__desc">
                            <p class="testimonial__desc-desc">
                                My children and I LOVE Qvoot! The courses are fantastic and the
                                instructors are so fun and knowledgeable.
                                I only wish we found it sooner.
                            </p>
                        </div><!-- end testimonial__desc -->
                        <div class="testimonial-header">
                            <img src="<?php echo base_url(); ?>assets/qvoot/images/testi-img3.jpg" alt="small-avatar">
                            <div class="testimonial__name">
                                <h3 class="testimonial__name-title">Jane Smith</h3>
                                <span class="testimonial__name-meta">student</span>
                                <ul class="review-stars d-inline-block">
                                    <li><span class="la la-star"></span></li>
                                    <li><span class="la la-star"></span></li>
                                    <li><span class="la la-star"></span></li>
                                    <li><span class="la la-star"></span></li>
                                    <li><span class="la la-star-o"></span></li>
                                </ul>
                            </div>
                        </div><!-- end testimonial-header -->
                    </div><!-- end testimonial-item -->
                    <div class="testimonial-item testimonial-item-layout-2">
                        <div class="testimonial__desc">
                            <p class="testimonial__desc-desc">
                                My children and I LOVE Qvoot! The courses are fantastic and the
                                instructors are so fun and knowledgeable.
                                I only wish we found it sooner.
                            </p>
                        </div><!-- end testimonial__desc -->
                        <div class="testimonial-header">
                            <img src="<?php echo base_url(); ?>assets/qvoot/images/testi-img4.jpg" alt="small-avatar">
                            <div class="testimonial__name">
                                <h3 class="testimonial__name-title">Dan Greene</h3>
                                <span class="testimonial__name-meta">student</span>
                                <ul class="review-stars d-inline-block">
                                    <li><span class="la la-star"></span></li>
                                    <li><span class="la la-star"></span></li>
                                    <li><span class="la la-star"></span></li>
                                    <li><span class="la la-star"></span></li>
                                    <li><span class="la la-star-o"></span></li>
                                </ul>
                            </div>
                        </div><!-- end testimonial-header -->
                    </div><!-- end testimonial-item -->
                </div><!-- end testimonial-carousel-2 -->
            </div><!-- end col-lg-8 -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end testimonial-area -->
<!--================================
        END TESTIMONIAL AREA
=================================-->

<!-- ================================
       START CLIENTLOGO AREA
================================= -->
<section class="clientlogo-area overflow-hidden padding-top-120px padding-bottom-100px text-center">
    <div class="stroke-line">
        <span class="stroke__line"></span>
        <span class="stroke__line"></span>
        <span class="stroke__line"></span>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-heading">
                    <h5 class="section__meta">our partners</h5>
                    <h2 class="section__title">Trusted by companies of all sizes</h2>
                    <span class="section-divider"></span>
                    <p class="section__desc">
                        Get access to high quality learning wherever you are, with online courses,
                        programs <br> and degrees created by leading universities, business schools
                    </p>
                </div><!-- end section-heading -->
            </div><!-- end col-md-12 -->
        </div><!-- end row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="client-logo">
                    <div class="client-logo-item">
                        <a href="#"><img src="<?php echo base_url(); ?>assets/qvoot/images/sponsor-img.png" alt="brand image"></a>
                    </div><!-- end client-logo-item -->
                    <div class="client-logo-item">
                        <a href="#"><img src="<?php echo base_url(); ?>assets/qvoot/images/sponsor-img2.png" alt="brand image"></a>
                    </div><!-- end client-logo-item -->
                    <div class="client-logo-item">
                        <a href="#"><img src="<?php echo base_url(); ?>assets/qvoot/images/sponsor-img3.png" alt="brand image"></a>
                    </div><!-- end client-logo-item -->
                    <div class="client-logo-item">
                        <a href="#"><img src="<?php echo base_url(); ?>assets/qvoot/images/sponsor-img4.png" alt="brand image"></a>
                    </div><!-- end client-logo-item -->
                    <div class="client-logo-item">
                        <a href="#"><img src="<?php echo base_url(); ?>assets/qvoot/images/sponsor-img5.png" alt="brand image"></a>
                    </div><!-- end client-logo-item -->
                </div><!-- end client-logo -->
            </div><!-- end col-lg-12 -->
        </div><!-- end row -->
    </div><!-- end container -->
    <div class="stroke-line2">
        <span class="stroke__line"></span>
        <span class="stroke__line"></span>
        <span class="stroke__line"></span>
    </div>
</section><!-- end clientlogo-area -->
<!-- ================================
       START CLIENTLOGO AREA
================================= -->

<!--======================================
        START CTA AREA
======================================-->
<section class="cta-area section-bg padding-top-120px padding-bottom-90px">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 column-lmd-half">
                <div class="post-card post-card-layout-3">
                    <div class="post-card-content">
                        <div class="post-card-img">
                            <img src="<?php echo base_url(); ?>assets/qvoot/images/img33.jpg" alt="" class="img-fluid">
                        </div>
                        <div class="post-card-inner-content">
                            <h2 class="widget-title mb-2">Become Instructor</h2>
                            <p class="mb-4">Students practice at their own pace, first filling in gaps in their understanding and then accelerating</p>
                            <a href="#" class="theme-btn line-height-40 text-capitalize">Start teaching</a>
                        </div>
                    </div><!-- end post-card-content -->
                </div>
            </div><!-- end col-lg-4 -->
            <div class="col-lg-4 column-lmd-half">
                <div class="post-card post-card-layout-3">
                    <div class="post-card-content">
                        <div class="post-card-img">
                            <img src="<?php echo base_url(); ?>assets/qvoot/images/img34.jpg" alt="" class="img-fluid">
                        </div>
                        <div class="post-card-inner-content">
                            <h2 class="widget-title mb-2">Become a Partner</h2>
                            <p class="mb-4">Created by experts, Coursector library of trusted practice and lessons covers math, science, and more.</p>
                            <a href="#" class="theme-btn line-height-40 text-capitalize">Get Qvoot for business</a>
                        </div>
                    </div><!-- end post-card-content -->
                </div>
            </div><!-- end col-lg-4 -->
             <div class="col-lg-4 column-lmd-half">
                <div class="post-card post-card-layout-3">
                    <div class="post-card-content">
                        <div class="post-card-img">
                            <img src="<?php echo base_url(); ?>assets/qvoot/images/img35.jpg" alt="" class="img-fluid">
                        </div>
                       <div class="post-card-inner-content">
                           <h2 class="widget-title mb-2">Become a Learner</h2>
                           <p class="mb-4">Created by experts, Coursector library of trusted practice and lessons covers math, science, and more.</p>
                           <a href="#" class="theme-btn line-height-40 text-capitalize">Start learning</a>
                       </div>
                    </div><!-- end post-card-content -->
                </div>
            </div><!-- end col-lg-4 -->
        </div><!-- end row -->
    </div><!-- end container-fluid -->
</section>
<!-- ================================
       START CTA AREA
================================= -->

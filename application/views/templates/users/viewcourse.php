<section class="breadcrumb-area" style="height: 180px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-content">
                    <div class="section-heading">
                        <h2 class="section__title">View Course</h2>
                    </div>
                    <ul class="breadcrumb__list">
                        <li class="active__list-item"><a href="<?php echo base_url(); ?>users/index">index</a></li>
                        <li>View Course</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="course-area padding-top-120px padding-bottom-120px">
    <div class="course-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <?php echo form_open('users/searchdetail') ?>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12">
                            <div class="input-box">
                                <label class="label-text">What<span class="primary-color-2 ml-1">*</span></label>

                                <div class="form-group">
                                    <input type="text" class="form-control" name="what" placeholder="What are you looking for ?">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12">
                            <div class="input-box">
                                <label class="label-text">Where<span class="primary-color-2 ml-1">*</span></label>

                                <div class="form-group">
                                    <input type="text" class="form-control search-slt" name="where" placeholder="Where are you looking for ?">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12">
                            <div class="input-box">
                                <label class="label-text">When<span class="primary-color-2 ml-1">*</span></label>

                                <div class="form-group">
                                    <input type="datetime" id="filter-date" class="form-control search-slt" name="when" placeholder="When are you looking for ?">

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12">
                            <button type="submit" class="btn col-lg-12 mt-4 theme-btn wrn-btn">Search</button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="filter-bar d-flex justify-content-between align-items-center">
                        <ul class="filter-bar-tab nav nav-tabs align-items-center" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active icon-element" id="grid-view-tab" data-toggle="tab" href="#grid-view" role="tab" aria-controls="grid-view" aria-selected="true">
                                    <span data-toggle="tooltip" data-placement="top" title="Grid View">
                                        <i class="la la-th-large"></i>
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link icon-element" id="list-view-tab" data-toggle="tab" href="#list-view" role="tab" aria-controls="list-view" aria-selected="false">
                                    <span data-toggle="tooltip" data-placement="top" title="List View">
                                        <i class="la la-th-list"></i>
                                    </span>
                                </a>
                            </li>
                            <?php $countcourse =  count($courses); ?>
                            <li class="nav-item font-size-15">Showing results <?php echo $countcourse; ?></li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="course-content-wrapper mt-4">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade show active" id="grid-view" aria-labelledby="grid-view-tab">
                                <div class="row">
                                    <?php foreach ($courses as $course) : ?>
                                        <div class="col-lg-4">
                                            <div class="card-item card-preview" data-tooltip-content="#tooltip_content_<?php echo $course['couse_id']; ?>">
                                                <div class="card-image">
                                                    <a href="<?php echo base_url() ?>users/coursedetail/<?php echo $course['couse_id']; ?>" class="card__img">
                                                        <?php foreach ($courses_pictures as $coursespic) :
                                                            if ($coursespic['course_id'] == $course['couse_id']) { ?>
                                                                <img src="<?php echo base_url(); ?>assets/uploads/<?php if (isset($coursespic['course_picture'])) {
                                                                                                                        echo $coursespic['course_picture'];
                                                                                                                    } else {
                                                                                                                        echo "no_image.jpg";
                                                                                                                    }  ?>" alt="<?php echo $course['course_name']; ?>" style="max-height: 250px;min-height: 250px;"></a>
                                            <?php break;
                                                            }
                                                        endforeach; ?>
                                            </a>

                                                </div>
                                                <div class="card-content">
                                                    <p class="card__label">
                                                        <span class="card__label-text"><?php echo $course['category_name']; ?></span>
                                                        <a href="#" class="card__collection-icon" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="la la-heart-o"></span></a>
                                                    </p>
                                                    <h3 class="card__title">
                                                        <a href="<?php echo base_url() ?>users/coursedetail/<?php echo $course['couse_id']; ?>"><?php echo $course['course_name']; ?></a>
                                                    </h3>
                                                    <p class="card__author">
                                                        <a href="teacher-detail.html"><?php echo $course['course_instructor_name']; ?></a>
                                                    </p>
                                                    <div class="rating-wrap d-flex mt-2 mb-3">
                                                        <ul class="review-stars">
                                                            <li><span class="la la-star"></span></li>
                                                            <li><span class="la la-star"></span></li>
                                                            <li><span class="la la-star"></span></li>
                                                            <li><span class="la la-star"></span></li>
                                                            <li><span class="la la-star-o"></span></li>
                                                        </ul>
                                                        <span class="star-rating-wrap">
                                                            <span class="star__rating">4.4</span>
                                                            <span class="star__count">(20)</span>
                                                        </span>
                                                    </div><!-- end rating-wrap -->
                                                    <div class="card-action">
                                                        <ul class="card-duration d-flex justify-content-between align-items-center">
                                                            <li>
                                                                <span class="meta__date">
                                                                    <i class="la la-play-circle"></i> <?php echo $course['course_total_students']; ?> Classes
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="meta__date">
                                                                    <i class="la la-clock-o"></i> <?php echo $course['course_start_date']; ?>
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div><!-- end card-action -->
                                                    <div class="card-price-wrap d-flex justify-content-between align-items-center">
                                                        <span class="card__price">$<?php echo $course['course_price']; ?></span>
                                                        <a href="#" class="text-btn">Add to cart</a>
                                                    </div><!-- end card-price-wrap -->
                                                </div><!-- end card-content -->
                                            </div><!-- end card-item -->
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="list-view" aria-labelledby="list-view-tab">
                                <div class="row">
                                    <?php foreach ($courses as $course) : ?>
                                        <div class="col-lg-6">
                                            <div class="card-item card-list-layout card-preview" data-tooltip-content="#tooltip_content_<?php echo $course['couse_id']; ?>">
                                                <div class="card-image">
                                                    <a href="<?php echo base_url() ?>users/coursedetail/<?php echo $course['couse_id']; ?>" class="card__img">
                                                        <?php foreach ($courses_pictures as $coursespic) :
                                                            if ($coursespic['course_id'] == $course['couse_id']) { ?>
                                                                <img src="<?php echo base_url(); ?>assets/uploads/<?php if (isset($coursespic['course_picture'])) {
                                                                                                                        echo $coursespic['course_picture'];
                                                                                                                    } else {
                                                                                                                        echo "no_image.jpg";
                                                                                                                    }  ?>" alt="<?php echo $course['course_name']; ?>" style="max-height: 250px;min-height: 250px;"></a>
                                            <?php break;
                                                            }
                                                        endforeach; ?>
                                            </a>
                                                </div>
                                                <div class="card-content">
                                                    <p class="card__label">
                                                        <span class="card__label-text"><?php echo $course['category_name']; ?></span>
                                                        <a href="#" class="card__collection-icon" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="la la-heart-o"></span></a>
                                                    </p>
                                                    <h3 class="card__title">
                                                        <a href="<?php echo base_url() ?>users/coursedetail/<?php echo $course['couse_id']; ?>"><?php echo $course['course_name']; ?></a>
                                                    </h3>
                                                    <p class="card__author">
                                                        <a href="teacher-detail.html"><?php echo $course['course_instructor_name']; ?></a>
                                                    </p>
                                                    <div class="rating-wrap d-flex mt-2 mb-3">
                                                        <ul class="review-stars">
                                                            <li><span class="la la-star"></span></li>
                                                            <li><span class="la la-star"></span></li>
                                                            <li><span class="la la-star"></span></li>
                                                            <li><span class="la la-star"></span></li>
                                                            <li><span class="la la-star-o"></span></li>
                                                        </ul>
                                                        <span class="star-rating-wrap">
                                                            <span class="star__rating">4.4</span>
                                                            <span class="star__count">(20)</span>
                                                        </span>
                                                    </div><!-- end rating-wrap -->
                                                    <div class="card-action">
                                                        <ul class="card-duration d-flex justify-content-between align-items-center">
                                                            <li>
                                                                <span class="meta__date">
                                                                    <i class="la la-play-circle"></i> <?php echo $course['course_total_students']; ?> Classes
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="meta__date">
                                                                    <i class="la la-clock-o"></i> <?php echo $course['course_start_date']; ?>
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div><!-- end card-action -->
                                                    <div class="card-price-wrap d-flex justify-content-between align-items-center">
                                                        <span class="card__price">Free</span>
                                                        <a href="#" class="text-btn">Get Enrolled</a>
                                                    </div><!-- end card-price-wrap -->
                                                </div><!-- end card-content -->
                                            </div><!-- end card-item -->
                                        </div>
                                    <?php endforeach; ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="scroll-top">
    <i class="fa fa-angle-up" title="Go top"></i>
</div>
<?php foreach ($courses as $course) : ?>

    <div class="tooltip_templates">
        <div id="tooltip_content_<?php echo $course['couse_id']; ?>">
            <div class="card-item">
                <div class="card-content">
                    <p class="card__author">
                        By <a href="teacher-detail.html"><?php echo $course['course_instructor_name']; ?></a>
                    </p>
                    <h3 class="card__title">
                        <a href="<?php echo base_url() ?>users/coursedetail/<?php echo $course['couse_id']; ?>"><?php echo $course['course_name']; ?></a>
                    </h3>


                    <div class="card-para mb-3">
                        <p class="font-size-14 line-height-24">
                            <?php echo $course['course_full_desc']; ?>
                        </p>
                    </div>

                    <div class="card-action">
                        <ul class="card-duration d-flex justify-content-between align-items-center">
                            <li><span class="meta__date"><i class="la la-play-circle"></i> <?php echo $course['course_total_students']; ?> Classes</span></li>
                            <li><span class="meta__date"><i class="la la-clock-o"></i> <?php echo $course['course_start_date']; ?></span></li>
                        </ul>
                    </div><!-- end card-action -->
                    <div class="btn-box w-100 text-center mb-3">
                        <a href="<?php echo base_url() ?>users/coursedetail/<?php echo $course['couse_id']; ?>" class="theme-btn d-block">Preview this course</a>
                    </div>
                    <div class="card-price-wrap d-flex justify-content-between align-items-center">
                        <span class="card__price">$<?php echo $course['course_price']; ?></span>
                        <a href="#" class="text-btn">Add to cart</a>
                    </div><!-- end card-price-wrap -->
                </div><!-- end card-content -->
            </div><!-- end card-item -->
        </div>
    </div>
<?php endforeach; ?>
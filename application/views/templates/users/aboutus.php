<section class="breadcrumb-area" style="height: 180px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-content">
                    <div class="section-heading">
                        <h2 class="section__title">About US</h2>
                    </div>
                    <ul class="breadcrumb__list">
                        <li class="active__list-item"><a href="<?php echo base_url(); ?>users/index">index</a></li>
                        <li>About US</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="about-area padding-top-120px padding-bottom-110px">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="about-content-box pt-0">
                    <div class="section-heading">
                        <h2 class="section__meta line-height-50" style="display: table;margin: 0px 0 -8px 0;">About US</h2>
                        <span class="section-divider"></span>
                        <p class="section__desc mb-4">
                            Qvoot is a platform that enables users to reach and maintain their goals by providing access to complete fitness, lifestyle, and personal development classes wherever you are. With Qvoot, whether you find yourself in your hometown, traveling for work, visiting friends, or just even on vacation, we have you covered! With us you can find personal training, yoga, meditation and more activities to help you stay progressive, positive and inspired. Qvoot offers the easiest, most convenient, and most flexible solution to search, find, and reserve all sorts of skill development being mentally inspired. Get trained when you want, where you want. At Qvoot, we realize that your interests, location, and availability can change. That is why we believe no one should be tied to any contract. With Qvoot, you have access to a variety of facilities with nothing to hold you back. It’s time to take the leash off with Qvoot.
                        </p>
                    </div>
                    <div class="section-heading">
                        <h2 class="section__meta line-height-50" style="display: table;margin: 0px 0 -8px 0;">Vision</h2>
                        <span class="section-divider"></span>
                        <p class="section__desc mb-4">
                            Healthy mind is always creative. Happy and healthy users who make time for self-development, creative regardless of where they are without any contract and membership fees. Only good health, stress-free creative mind, and spirituality of the soul can give a person true satisfaction. We are aiming to provide you with that much confidence to win true strength from outside and inside.</p>
                    </div>
                    <div class="section-heading">
                        <h2 class="section__meta line-height-50" style="display: table;margin: 0px 0 -8px 0;">Mission</h2>
                        <span class="section-divider"></span>
                        <p class="section__desc mb-4">
                            We seek to provide the highest level of freedom in self-development, through creative lifestyle by providing users with instant access to facilities and activities around the world. Through the development of health, brainpower, and mental capability, we want to establish a futuristic creative world. When depression, stress, and mental stability are slow-poisoning the people of all age groups, we want to revive them through our supreme mission. </p>
                    </div>

                    <div class="section-heading">
                        <h2 class="section__meta line-height-50" style="display: table;margin: 0px 0 -8px 0;">Why Choose Us ?</h2>
                        <span class="section-divider"></span>
                        <ul class="list-items">
                            <li><i class="la la-check-circle"></i>We always select highly skillful trainers having long experience.</li>
                            <li><i class="la la-check-circle"></i>We have qualified therapists who ensure you a healthy living</li>
                            <li><i class="la la-check-circle"></i>We have certified yoga and meditation guides to make you relaxed.</li>
                            <li><i class="la la-check-circle"></i>We have award-winning lifestyle trainer to empower you to enjoy a creative world you want to relish</li>
                            <li><i class="la la-check-circle"></i>We have easy online and studio training facilities.</li>
                            <li><i class="la la-check-circle"></i>We are affordable across the industry.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
</section>
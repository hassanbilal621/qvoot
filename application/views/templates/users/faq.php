<section class="breadcrumb-area" style="height: 180px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-content">
                    <div class="section-heading">
                        <h2 class="section__title">F A Q</h2>
                    </div>
                    <ul class="breadcrumb__list">
                        <li class="active__list-item"><a href="<?php echo base_url(); ?>users/index">index</a></li>
                        <li>F A Q</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="about-area padding-top-120px padding-bottom-110px">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="about-content-box pt-0">
                    <div class="section-heading">
                        <h2 class="section__title line-height-50" style="display: table;margin: 0px 0 -8px 0;">Qvoot FAQ’S
                        </h2>
                        <h2 class="section__title line-height-50" style="display: table;margin: 0px 0 -8px 0;"> How to become a trainer?
                        </h2>
                        <span class="section-divider"></span>
                        <p class="section__desc mb-4">
                            Simply click on ‘Join Us as Trainer’, once you are on the page, fill out all the information that is requested. Make sure to choose a catchy image and upload one of your videos that will leave a good impression. If you still need help, simply just talk with us through live chat support.
                        </p>
                    </div>
                    <div class="section-heading">
                        <h2 class="section__title line-height-50" style="display: table;margin: 0px 0 -8px 0;">My post is not live yet?</h2>
                        <span class="section-divider"></span>
                        <p class="section__desc mb-4">
                            All of your service postings are manually checked by our staff, therefore, if your post is not approved within 48 hours then there must be something wrong with your account, you have to publish your post according to our criteria. If you still think your post is correct and is not approved, please reach us at support@qvoot.com or you can also chat with us directly through live chat 24/7.
                        </p>
                    </div>
                    <div class="section-heading">
                        <h2 class="section__title line-height-50" style="display: table;margin: 0px 0 -8px 0;">Can I offer different courses in different categories? Is there any limit for offering various courses? </h2>
                        <span class="section-divider"></span>
                        <p class="section__desc mb-4">
                            You are always welcome to offer multiple courses at once in different categories on our platform. We do not have any limitations regarding the number of services you provide.
                        </p>
                    </div>

                    <div class="section-heading">
                        <h2 class="section__title line-height-50" style="display: table;margin: 0px 0 -8px 0;">How can I use a smart calendar?</h2>
                        <span class="section-divider"></span>
                        <p class="section__desc mb-4">
                            With our smart calendar, you can easily manage your classes and hours of operation. Our Smart calendar is very much user friendly. All you need to do is just:
                            ~Select the day on which you would like to hold the course.
                            ~Enter your starting and ending time.
                            ~Define the capacity of your online and offline class by entering the number of trainees that can book the class.
                            Can I be a trainee and trainer at the same time?
                            Yes, of course, you can be a trainer and a trainee at the same time. You are always welcome to learn new skills on our platform and as well as teach your skills to trainees.

                        </p>
                    </div>


                    <div class="section-heading">
                        <h2 class="section__title line-height-50" style="display: table;margin: 0px 0 -8px 0;">Can I offer both online and offline courses?</h2>
                        <span class="section-divider"></span>
                        <p class="section__desc mb-4">
                            Yes, you can offer both online and offline courses at the same time. Qvoot can empower you to become one of the most available and accessible trainers through three key elements: Digitalization, individualization, and sustainability.
                        </p>

                    </div>

                    <div class="section-heading">
                        <h2 class="section__title line-height-50" style="display: table;margin: 0px 0 -8px 0;">How can I attend my session?</h2>
                        <span class="section-divider"></span>
                        <p class="section__desc mb-4">
                            Online: Once it is a starting time of your session, your instructor will send you the link for your online session 15 mins before the starting time. Simply, then just grab the link and join your class.
                        </p>
                        <p class="section__desc mb-4">
                            Offline: Once it is booked you will get a confirmation via email. You will get the exact address of the location of your physical session.

                        </p>

                    </div>
                    <div class="section-heading">
                        <h2 class="section__title line-height-50" style="display: table;margin: 0px 0 -8px 0;">Is there any membership/subscription fee?</h2>
                        <span class="section-divider"></span>
                        <p class="section__desc mb-4">
                            There is no membership or any subscription fees, we are literally free to use. We will never charge our trainers monthly or annually which means our trainers have full flexibility.
                        </p>

                    </div>
                    <div class="section-heading">
                        <h2 class="section__title line-height-50" style="display: table;margin: 0px 0 -8px 0;">How to book a course and how to pay?</h2>
                        <span class="section-divider"></span>
                        <p class="section__desc mb-4">
                            With the help of our help functionality, you can search What course? Where? And When you want to attend the course. After choosing the desired class, you can click on the Book Now button, and you will see the payment gateway. After your successful online payment, you will get a confirmation on email. You can also add your favorite course to Wish list and book later.
                        </p>

                    </div>
                    <div class="section-heading">
                        <h2 class="section__title line-height-50" style="display: table;margin: 0px 0 -8px 0;">What is the use of online and offline features?</h2>
                        <span class="section-divider"></span>
                        <p class="section__desc mb-4">
                            If the trainer is offering an online course that means he will take your session online. But, if there is a trainer who is offering an offline course, it means they take physical classes. By joining us you are not only getting online reliability, but you can also increase your offline strength, now this is game-changing.
                        </p>

                    </div>
                    <div class="section-heading">
                        <h2 class="section__title line-height-50" style="display: table;margin: 0px 0 -8px 0;">What payment methods do we accept?</h2>
                        <span class="section-divider"></span>
                        <p class="section__desc mb-4">
                            We wanted to make it as much easier as we can for our users. Therefore, you can do a transaction via PayPal or a Credit Card / Debit Card.
                        </p>

                    </div>

                </div>
            </div>
        </div>
</section>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="author" content="TechyDevs">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Qvoot | Fit, happy and healthy</title>

    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Mukta:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" sizes="16x16" href="<?php echo base_url(); ?>assets/favicon.png">

    <!-- inject:css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/qvoot/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/qvoot/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/qvoot/css/line-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/qvoot/css/animate.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/qvoot/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/qvoot/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/qvoot/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/qvoot/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/qvoot/css/fancybox.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/qvoot/css/jquery.filer.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/qvoot/css/tooltipster.bundle.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/qvoot/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/qvoot/css/mystyle.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/timepicker/jquery.datetimepicker.css"/>

    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/5f385b49b7f44f406e952709/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->

    <!-- end inject -->
</head>

<body>
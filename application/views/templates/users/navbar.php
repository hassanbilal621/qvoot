<header class="header-menu-area">
    <div class="header-top">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="header-widget">
                        <ul class="header-action-list">
                            <li><a href="#"><span class="la la-envelope-o mr-2"></span>support@qvoot.com</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="header-widget d-flex align-items-center justify-content-end">
                        <div class="header-right-info">
                            <ul class="header-social-profile">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                        <div class="header-right-info">

                        </div>
                        <div class="header-right-info">
                            <?php if (!$this->session->userdata('qvoot_user_id')) { ?>
                                <ul class="header-action-list">
                                    <li><a href="<?php echo base_url(); ?>users/login">Login</a></li>
                                    <li><a href="<?php echo base_url(); ?>users/register">Register</a></li>
                                </ul>
                            <?php } else { ?>
                                <ul class="header-action-list">
                                    <li><a href="<?php echo base_url(); ?>users/logout">Logout</a></li>
                                    <li><a href="<?php echo base_url(); ?>users/dashboard">Client Area</a></li>
                                    <li><a href="<?php echo base_url(); ?>users/Profile">Profile</a></li>

                                </ul>
                            <?php } ?>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- end header-top -->
    <div class="header-menu-content">
        <div class="container-fluid">
            <div class="main-menu-content">
                <div class="row align-items-center">
                    <div class="col-lg-2">
                        <div class="logo-box">
                            <a href="<?php echo base_url(); ?>" class="logo"><img src="<?php echo base_url() ?>assets/qvoot.png" alt="logo" width="200px"></a>
                            <div class="menu-toggler">
                                <i class="la la-bars"></i>
                                <i class="la la-times"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="menu-wrapper">
                            <div class="menu-category">
                                <ul>
                                    <li>
                                        <a href="#"><i class="la la-th-large mr-1"></i>Categories</a>
                                        <ul class="cat-dropdown-menu">
                                            <?php foreach ($categories as $category) : ?>
                                                <li>
                                                    <a href="<?php echo base_url() ?>users/coursebycat/<?php echo $category['category_id'] ?>"><?php echo $category['category_name'] ?> <i class="la la-angle-right"></i></a>
                                                    <ul class="sub-menu">
                                                        <?php foreach ($subcategories as $subcategory) :
                                                            if ($category['category_id'] == $subcategory['category_id_sub']) { ?>
                                                                <li><a href="<?php echo base_url() ?>users/coursebycat/<?php echo $subcategory['category_id_sub'] ?>"><?php echo $subcategory['sub_category_name'] ?></a></li>

                                                        <?php }
                                                        endforeach; ?>

                                                    </ul>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="contact-form-action" style="margin-bottom: 0px !important;">
                                <div class="input-box">
                                    <?php echo form_open('users/search_results') ?>

                                    <div class="form-group" style="margin-bottom: 0px !important;">
                                        <input class="form-control" type="text" name="search_courses" placeholder="Search for courses">
                                        <button type="submit" name="action" class="search-icon">
                                            <span class="la la-search"></span>
                                        </button>


                                    </div>
                                   <?php echo form_close() ?>
                                </div>
                            </div>
                            <nav class="main-menu">
                                <ul>
                                    <li class="active">
                                        <a href="<?php echo base_url(); ?>users/index">Home</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>users/howitworks">How it works</a>
                                    </li>

                                    <li>
                                        <a href="<?php echo base_url(); ?>users/aboutus">About us</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>users/faq">FAQ</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url(); ?>users/contactus">Contact Us</a>
                                    </li>
                                    <!-- <li>
                                        <a href="<?php echo base_url(); ?>users/blog">Blog</a>
                                    </li> -->

                                </ul>
                            </nav>
                            <div class="logo-right-button">
                                <a href="<?php echo base_url(); ?>users/addcourses" class="theme-btn">Join us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
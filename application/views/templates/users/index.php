<!-- <?php // $this->load->view('templates/users/slider.php'); 
        ?> -->
<!--  -->
<!--================================
         START SLIDER AREA
=================================-->
<section class="slider-area">
    <div class="hero-slide owl-dot-and-nav">

        <div class="single-slide-item slide-bg1">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-7">
                        <div class="hero-heading mr-4">
                            <div class="section-heading">
                                <h5 class="section__title" style="font-size: 35px;">Qvoot is going to Launch On 25 Sept</h5>
                                <p class="section__desc">
                                    Currently, we are onboarding trainers!
                                    All experienced trainers are welcome! Join us before Launch, and build a Glorious Career!
                                    Want to know in detail? We have 24/7 Live Chat Support.
                                </p>
                            </div>
                            <div class="hero-search-form">
                                <div class="contact-form-action">
                                    
                                </div><!-- end contact-form-action -->
                            </div>
                        </div>
                    </div><!-- col-lg-7 -->
                    <div class="col-lg-5">
                        <div class="hero-category">
                            <div class="row">
                                <div class="col-lg-6 column-td-half">
                                    <div class="category-item category-item-layout-2 category--item-layout-2">
                                        <a href="#" class="category-content">
                                            <i class="la la-user icon-element"></i>
                                            <h3 class="cat__title">Fitness</h3>
                                        </a><!-- end category-content -->
                                    </div><!-- end category-item -->
                                </div><!-- col-lg-4 -->
                                <div class="col-lg-6 column-td-half">
                                    <div class="category-item category-item-layout-2 category--item-layout-2">
                                        <a href="#" class="category-content">
                                            <i class="la la-heartbeat icon-element"></i>
                                            <h3 class="cat__title">Health</h3>
                                        </a><!-- end category-content -->
                                    </div><!-- end category-item -->
                                </div><!-- end col-lg-4 -->
                                <div class="col-lg-6 column-td-half">
                                    <div class="category-item category-item-layout-2 category--item-layout-2">
                                        <a href="#" class="category-content">
                                            <i class="la la-money icon-element"></i>
                                            <h3 class="cat__title">LifeStyle</h3>
                                        </a><!-- end category-content -->
                                    </div><!-- end category-item -->
                                </div><!-- end col-lg-4 -->
                                <div class="col-lg-6 column-td-half">
                                    <div class="category-item category-item-layout-2 category--item-layout-2">
                                        <a href="#" class="category-content">
                                            <i class="la la-line-chart icon-element"></i>
                                            <h3 class="cat__title">Personal Growth</h3>
                                        </a><!-- end category-content -->
                                    </div><!-- end category-item -->
                                </div><!-- end col-lg-4 -->

                            </div><!-- row -->
                        </div><!-- hero-category -->
                    </div><!-- col-lg-7 -->
                </div><!-- row -->
            </div><!-- container -->
        </div><!-- end single-slide-item -->

        <div class="single-slide-item slide-bg3">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-heading text-right">
                            <h2 class="section__title text-white">Learn Anything, <br> Anytime, Anywhere</h2>
                            <p class="section__desc">Qvoot is a unique and creative platform to keep your health, mind, and soul fit <br>through Yoga, meditation, or spiritual training along with skill development
                            </p>
                        </div>
                        <div class="btn-box hero-btn-right d-flex align-items-center justify-content-end">
                            <!-- <a href="#" class="btn-text video-play-btn mr-4" data-fancybox="video" data-src="https://www.youtube.com/watch?v=cRXm1p-CNyk" data-speed="700">
                               <i class="la la-play icon-btn mr-2"></i>Watch Preview
                            </a> -->
                            <a href="<?php echo base_url(); ?>users/addcourses" class="theme-btn theme-btn-hover-light">Join Now!</a>
                        </div>
                    </div><!-- col-lg-12 -->
                </div><!-- row -->
            </div><!-- container -->
        </div><!-- end single-slide-item -->
    </div><!-- end hero-slide -->
</section><!-- end slider-area -->
<!--================================
        END SLIDER AREA
=================================-->

<section class="choose-area section-padding text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-heading">
                    <h5 class="section__meta">why choose us</h5>
                    <h2 class="section__title">Why Study with Qvoot?</h2>
                    <span class="section-divider"></span>
                </div>
            </div>
        </div>
        <div class="row margin-top-100px">
            <div class="col-lg-4 column-td-half">
                <div class="post-card post-card-layout-2">
                    <div class="post-card-content">
                        <img src="<?php echo base_url(); ?>assets/icons/310.jpg" alt="" class="img-fluid">
                        <h2 class="widget-title mt-4 mb-2">Learn anything</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 column-td-half">
                <div class="post-card post-card-layout-2">
                    <div class="post-card-content">
                        <img src="<?php echo base_url(); ?>assets/icons/464.jpg" alt="" class="img-fluid">
                        <h2 class="widget-title mt-4 mb-2">Flexible learning</h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 column-td-half">
                <div class="post-card post-card-layout-2">
                    <div class="post-card-content">
                        <img src="<?php echo base_url(); ?>assets/icons/1337.jpg" alt="" class="img-fluid">
                        <h2 class="widget-title mt-4 mb-2">Learn with experts</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="btn-box mt-3 d-flex align-items-center justify-content-center text-left">
                    <div class="btn-box-inner mr-3">
                        <span class="d-block mb-2">Are you a Trainer?</span>
                        <a href="<?php echo base_url(); ?>users/addcourses" class="theme-btn line-height-40 text-capitalize">Start teaching</a>
                    </div>
                    <div class="btn-box-inner">
                        <span class="d-block mb-2">Are you a Trainee?</span>
                        <a href="<?php echo base_url(); ?>users/courseslist" class="theme-btn line-height-40 text-capitalize">Start learning</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!--======================================
        START BENEFIT AREA
======================================-->
<section class="benefit-area benefit-area2 padding-top-120px padding-bottom-120px overflow-hidden">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="img-box img-box-2">
                    <img src="<?php echo base_url(); ?>assets/qvoot/images/bg1.jpg" alt="">
                    <img src="<?php echo base_url(); ?>assets/qvoot/images/bg2.jpg" alt="">
                    <img src="<?php echo base_url(); ?>assets/qvoot/images/bg3.jpg" alt="">
                    <img src="<?php echo base_url(); ?>assets/qvoot/images/img33.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="benefit-heading padding-top-120px">
                    <div class="section-heading">
                        <h5 class="section__meta">Get start with Qvoot</h5>
                        <h2 class="section__title">Take the next step toward your personal and professional goals with Qvoot</h2>
                        <span class="section-divider"></span>
                        <p class="section__desc">
                            We seek to provide the highest level of freedom in self-development, through creative lifestyle by providing users with instant access to facilities and activities around the world.
                        </p>
                    </div><!-- end section-heading -->
                    <div class="row">
                        <div class="col-lg-4 column-td-half">
                            <div class="info-icon-box">
                                <span class="la la-mouse-pointer icon-element icon-bg-1"></span>
                                <h4 class="widget-title">Unique Courses</h4>
                            </div><!-- end info-icon-box -->
                        </div><!-- end col-lg-4 -->
                        <div class="col-lg-4 column-td-half">
                            <div class="info-icon-box">
                                <span class="la la-bolt icon-element icon-bg-2"></span>
                                <h4 class="widget-title">Live Learning</h4>
                            </div><!-- end info-icon-box -->
                        </div><!-- end col-lg-4 -->
                        <div class="col-lg-4 column-td-half">
                            <div class="info-icon-box">
                                <span class="la la-users icon-element icon-bg-3"></span>
                                <h4 class="widget-title">Expert Trainers</h4>
                            </div><!-- end info-icon-box -->
                        </div><!-- end col-lg-4 -->
                    </div><!-- end row -->
                    <div class="btn-box">
                        <a href="<?php echo base_url(); ?>users/addcourses" class="theme-btn">join Now!</a>
                    </div>
                </div><!-- end benefit-heading -->
            </div><!-- end col-lg-6 -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end benefit-area -->
<!--======================================
        END BENEFIT AREA
======================================-->

<section class="category-area section-bg padding-top-120px padding-bottom-120px text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-heading">
                    <h5 class="section__meta">Categories</h5>
                    <h2 class="section__title">Browse Popular Categories</h2>
                    <span class="section-divider"></span>
                </div>
            </div>
        </div>
        <div class="row margin-top-28px">
            <div class="col-lg-3 column-td-half">
                <div class="flip-box-item">
                    <div class="front">
                        <img src="<?php echo base_url(); ?>assets/icons/7.jpg" style="height: 200px;" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <h3 class="cat__title">Fitness</h3>
                                <p class="cat__meta">500 courses</p>
                            </div>
                        </div>
                    </div>
                    <div class="back">
                        <img src="<?php echo base_url(); ?>assets/icons/7.jpg" style="height: 200px;" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <a href="<?php echo base_url() ?>users/coursebycat/1" class="theme-btn">View Course</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 column-td-half">
                <div class="flip-box-item">
                    <div class="front">
                        <img src="<?php echo base_url(); ?>assets/icons/44.jpg" style="height: 200px;" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <h3 class="cat__title">Health</h3>
                                <p class="cat__meta">356 courses</p>
                            </div>
                        </div>
                    </div>
                    <div class="back">
                        <img src="<?php echo base_url(); ?>assets/icons/44.jpg" style="height: 200px;" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <a href="<?php echo base_url() ?>users/coursebycat/2" class="theme-btn">View Course</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 column-td-half">
                <div class="flip-box-item">
                    <div class="front">
                        <img src="<?php echo base_url(); ?>assets/icons/6.jpg" style="height: 200px;" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <h3 class="cat__title">Life Style</h3>
                                <p class="cat__meta">500 courses</p>
                            </div>
                        </div>
                    </div>
                    <div class="back">
                        <img src="<?php echo base_url(); ?>assets/icons/6.jpg" style="height: 200px;" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <a href="<?php echo base_url() ?>users/coursebycat/3" class="theme-btn">View Course</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 column-td-half">
                <div class="flip-box-item">
                    <div class="front">
                        <img src="<?php echo base_url(); ?>assets/icons/377.jpg" style="height: 200px;" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <h3 class="cat__title">Personal Growth</h3>
                                <p class="cat__meta">356 courses</p>
                            </div>
                        </div>
                    </div>
                    <div class="back">
                        <img src="<?php echo base_url(); ?>assets/icons/377.jpg" style="height: 200px;" alt="">
                        <div class="category-item">
                            <div class="category-content">
                                <a href="<?php echo base_url() ?>users/coursebycat/4" class="theme-btn">View Course</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="course-area padding-top-120px padding-bottom-120px">
    <div class="course-wrapper">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-12">
                    <div class="section-heading">
                        <h5 class="section__meta">Courses</h5>
                        <h2 class="section__title">Recent Courses</h2>
                        <span class="section-divider"></span>
                    </div>
                </div>
            </div>
            <div class="course-content-wrapper mt-4">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade show active" id="grid-view" aria-labelledby="grid-view-tab">
                                <div class="row">
                                    <?php foreach ($courses as $course) : ?>
                                        <div class="col-lg-4">
                                            <div class="card-item card-preview" data-tooltip-content="#tooltip_content_<?php echo $course['couse_id']; ?>">
                                                <div class="card-image">
                                                    <a href="<?php echo base_url() ?>users/coursedetail/<?php echo $course['couse_id']; ?>" class="card__img">
                                                        <?php foreach ($courses_pictures as $coursespic) :
                                                            if ($coursespic['course_id'] == $course['couse_id']) { ?>
                                                                <img src="<?php echo base_url(); ?>assets/uploads/<?php if (isset($coursespic['course_picture'])) {
                                                                                                                        echo $coursespic['course_picture'];
                                                                                                                    } else {
                                                                                                                        echo "no_image.jpg";
                                                                                                                    }  ?>" alt="<?php echo $course['course_name']; ?>" style="max-height: 200px;min-height: 200px;"></a>
                                            <?php break;
                                                            }
                                                        endforeach; ?>
                                            </a>
                                                </div>
                                                <div class="card-content">
                                                    <p class="card__label">
                                                        <span class="card__label-text"><?php echo $course['category_name']; ?></span>
                                                        <a href="#" class="card__collection-icon" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="la la-heart-o"></span></a>
                                                    </p>
                                                    <h3 class="card__title">
                                                        <a href="<?php echo base_url() ?>users/coursedetail/<?php echo $course['couse_id']; ?>"><?php echo $course['course_name']; ?></a>
                                                    </h3>
                                                    <p class="card__author">
                                                        <a href="teacher-detail.html"><?php echo $course['course_instructor_name']; ?></a>
                                                    </p>
                                                    <div class="card-action">
                                                        <ul class="card-duration d-flex justify-content-between align-items-center">
                                                            <li>
                                                                <span class="meta__date">
                                                                    <i class="la la-play-circle"></i> <?php echo $course['course_total_students']; ?> Course Capacity
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="meta__date">
                                                                    <i class="la la-clock-o"></i>Starting Date: <?php $date = date_create($course['course_start_date']);
                                                                                                                echo date_format($date, "d/m/Y"); ?>
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <!-- end card-action -->
                                                    <div class="card-price-wrap d-flex justify-content-between align-items-center">
                                                        <span class="card__price">$<?php echo $course['course_price']; ?></span>
                                                        <a href="<?php echo base_url() ?>users/coursedetail/<?php echo $course['couse_id']; ?>" class="text-btn">View Detail</a>
                                                    </div>
                                                    <!-- end card-price-wrap -->
                                                </div>
                                                <!-- end card-content -->
                                            </div>
                                            <!-- end card-item -->
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="btn-box mt-4 text-center">
                            <a href="<?php echo base_url() ?>users/viewcourse" class="theme-btn">browse all course</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="funfact-area text-center overflow-hidden padding-top-85px padding-bottom-85px">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 column-td-half">
                <div class="counter-item">
                    <span class="la la-bullhorn count__icon"></span>
                    <h4 class="count__title counter">326</h4>
                    <p class="count__meta">expert instructors</p>
                </div>
            </div>
            <div class="col-lg-3 column-td-half">
                <div class="counter-item">
                    <span class="la la-globe count__icon"></span>
                    <h4 class="count__title counter text-color">702</h4>
                    <p class="count__meta">foreign followers</p>
                </div>
            </div>
            <div class="col-lg-3 column-td-half">
                <div class="counter-item">
                    <span class="la la-users count__icon"></span>
                    <h4 class="count__title counter text-color-2">1634</h4>
                    <p class="count__meta">students enrolled</p>
                </div>
            </div>
            <div class="col-lg-3 column-td-half">
                <div class="counter-item">
                    <span class="la la-certificate count__icon"></span>
                    <h4 class="count__title counter text-color-3">2</h4>
                    <p class="count__meta">years of experience</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="get-start-area padding-top-120px padding-bottom-120px text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="get-start-content">
                    <div class="section-heading">
                        <h5 class="section__meta section__metalight">start online learning</h5>
                        <h2 class="section__title text-white font-size-40 line-height-55">
                            Why so late? Start one of our 2000 high quality courses <br>
                            from the world's leading experts today!
                        </h2>
                        <span class="section-divider section-divider-light"></span>
                    </div>
                    <div class="btn-box margin-top-20px">
                        <a href="<?php echo base_url() ?>users/addcourses" class="theme-btn theme-btn-hover-light">join for free</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<!--======================================
        START CHOOSE AREA
======================================-->
<section class="choose-area section-padding text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-heading">
                    <h2 class="section__title">Choose Us!</h2>
                    <span class="section-divider"></span>
                </div><!-- end section-heading -->
            </div><!-- end col-md-12 -->
        </div><!-- end row -->
        <div class="row margin-top-100px">

            <div class="col-lg-6 column-td-half">
                <div class="post-card post-card-layout-2">
                    <div class="post-card-content">
                        <img src="<?php echo base_url(); ?>assets/qvoot/images/img33.jpg" alt="" class="img-fluid">
                        <h2 class="widget-title mt-4 mb-2">Become trainer</h2>
                        <p>Skillful trainers will be appointed to improve a learner�s health, fitness, lifestyle, and help personal development</p>
                        <a href="<?php echo base_url(); ?>users/addcourses" class="theme-btn line-height-40 text-capitalize">Start teaching</a>

                    </div><!-- end post-card-content -->
                </div>
            </div><!-- end col-lg-4 -->
            <div class="col-lg-6 column-td-half">
                <div class="post-card post-card-layout-2">
                    <div class="post-card-content">
                        <img src="<?php echo base_url(); ?>assets/qvoot/images/img35.jpg" alt="" class="img-fluid">
                        <h2 class="widget-title mt-4 mb-2">Become a trainee</h2>
                        <p>Develop your health,skill,lifestyle and enjoy lifekeeping all stresses aside only with our skilled trainers. Join us!</p>
                        <a href="<?php echo base_url(); ?>users/viewcourse" class="theme-btn line-height-40 text-capitalize">Start learning</a>

                    </div><!-- end post-card-content -->
                </div>
            </div><!-- end col-lg-4 -->
        </div><!-- end row -->

    </div><!-- end container -->
</section>
<!-- ================================
     END FOOTER AREA
================================= -->
<section class="footer-area section-bg-2 padding-top-100px padding-bottom-40px">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 column-td-half">
                <div class="footer-widget">
                    <a href="index.html">
                        <img src="<?php echo base_url() ?>assets/qvoot.png" alt="footer logo" width="100px" class="footer__logo">
                    </a>
                    <ul class="list-items footer-address">
                        <li><a href="mailto:support@wbsite.com" class="mail">support@qvoot.com</a></li>
                        <li>Qvoot is the most flexible way to access new classes and improve your skills</li>
                    </ul>
                    <h3 class="widget-title font-size-17 mt-4">We are on</h3>
                    <ul class="social-profile">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div><!-- end footer-widget -->
            </div><!-- end col-lg-3 -->
            <div class="col-lg-2 column-td-half">
                <div class="footer-widget">
                    <h3 class="widget-title">Company</h3>
                    <span class="section-divider"></span>
                    <ul class="list-items">
                        <li><a href="<?php echo base_url(); ?>users/howitworks">How it works</a></li>
                        <li><a href="<?php echo base_url(); ?>users/aboutus">About Us</a></li>
                        <li><a href="<?php echo base_url(); ?>users/addcourses">Become an Instructor</a></li>
                        <li><a href="<?php echo base_url(); ?>users/contactus">Contact Us</a></li>
                        <li><a href="<?php echo base_url(); ?>users/faq">FAQs</a></li>
                        <!--<li><a href="#">blog</a></li>-->
                    </ul>
                </div><!-- end footer-widget -->
            </div><!-- end col-lg-3 -->
            <div class="col-lg-2 column-td-half">
                <div class="footer-widget">
                    <h3 class="widget-title">Categories</h3>
                    <span class="section-divider"></span>
                    <ul class="list-items">
                        <li><a href="<?php echo base_url(); ?>users/coursebycat/1">Fitness</a></li>
                        <li><a href="<?php echo base_url(); ?>users/coursebycat/2">Health</a></li>
                        <li><a href="<?php echo base_url(); ?>users/coursebycat/3">Life Style</a></li>
                        <li><a href="<?php echo base_url(); ?>users/coursebycat/4">Personal Growth</a></li>

                    </ul>
                </div><!-- end footer-widget -->
            </div>
            <div class="col-lg-5 column-td-half">
                <div class="footer-widget">
                    <h3 class="widget-title">Newsletter</h3>
                    <span class="section-divider"></span>
                    <div class="subscriber-form footer-form">
                        <div class="contact-form-action">
                            <form method="post">
                                <div class="input-box">
                                    <label class="form-label text-white">Your email address</label>
                                    <div class="form-group">
                                        <input class="form-control" type="email" name="email11" placeholder="Enter your email">
                                        <span class="la la-envelope-o input-icon"></span>
                                        <button class="theme-btn subscriber-btn line-height-48" type="submit">Subscribe</button>
                                    </div>
                                    <p class="text-color-rgba font-size-14 mt-1">
                                        <i class="la la-lock mr-1"></i>Your information is safe with us! unsubscribe anytime.
                                    </p>
                                </div>
                            </form>
                        </div><!-- end contact-form-action -->
                    </div><!-- end subscriber-form-->
                </div><!-- end footer-widget -->
            </div><!-- end col-lg-4 -->
        </div><!-- end row -->
        <div class="copyright-content">
            <div class="row align-items-center">
                <div class="col-lg-10">
                    <p class="copy__desc">&copy; 2020 Qvoot. All Rights Reserved.</p>
                </div><!-- end col-lg-9 -->
                <div class="col-lg-2">
                    <div class="sort-ordering">
                        <select class="sort-ordering-select">
                            <option value="1">English</option>
                            <option value="2">German</option>
                        </select>
                    </div>
                </div>
            </div><!-- end row -->
        </div><!-- end copyright-content -->
    </div><!-- end container -->
</section><!-- end footer-area -->
<!-- ================================
      END FOOTER AREA
================================= -->




<!-- start scroll top -->
<div id="scroll-top" style="margin: 0 0 50px 0;">
    <i class="fa fa-angle-up" title="Go top"></i>
</div>
<!-- end scroll top -->


<!-- template js files -->
<script src="<?php echo base_url(); ?>assets/qvoot/js/jquery-3.4.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/bootstrap-select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/magnific-popup.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/isotope.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/waypoint.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/jquery.counterup.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/fancybox.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/wow.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/smooth-scrolling.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/progress-bar.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/date-time-picker.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/emojionearea.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/animated-skills.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/jquery.filer.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/tooltipster.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/qvoot/js/main.js"></script>
<script src="<?php echo base_url(); ?>assets/timepicker/jquery.datetimepicker.full.js"></script>


<script>
    /*jslint browser:true*/
    /*global jQuery, document*/

    jQuery(document).ready(function() {
        'use strict';

        jQuery('#filter-date, #search-from-date, #search-to-date').datetimepicker();
    });
</script>

</body>

</html>
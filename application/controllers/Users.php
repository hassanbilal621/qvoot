<?php
class Users extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}
	public function login()
	{
		if ($this->session->userdata('qvoot_user_id')) {
			redirect('users/index');
		}
		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/login.php');
	}




	public function login_user()
	{
		if ($this->session->userdata('qvoot_user_id')) {
			redirect('users/index');
		}
		$this->form_validation->set_rules('user_email', 'user_email', 'required');
		$this->form_validation->set_rules('user_password', 'user_password', 'required');
		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('field_missing', 'field_missing');
			redirect('users/login');
		} else {
			$email = $this->input->post('user_email');
			$password = $this->input->post('user_password');
			$user_id = $this->user_model->login($email, $password);
			if ($user_id) {
				$user_data = array(
					'qvoot_user_id' => $user_id
				);
				$this->session->set_userdata($user_data);
				$this->session->set_flashdata('Success', 'Success');
				redirect('users/index');
			} else {
				$this->session->set_flashdata('login_failed', 'login_failed');
				redirect('users/login');
			}
		}
	}
	public function logout()
	{
		$this->session->unset_userdata('qvoot_user_id');
		$this->session->set_flashdata('user_loggedout', 'user_loggedout');
		redirect('users/login');
	}
	public function forgotpassword()
	{
		if ($this->session->userdata('qvoot_user_id')) {
			redirect('users/index');
		}
		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/forgotpassword.php');
	}



	public function checkingemail()
	{
		if ($this->session->userdata('study_user_id')) {
			redirect('users/login');
		}
		$this->form_validation->set_rules('user_email', 'user_email', 'required');



		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('field_missing', 'field_missing');
			redirect('users/forgotpassword');
		} else {

			$this->db->where('user_email', $this->input->post('user_email'));
			$query = $this->db->get('users');
			if ($query->num_rows() > 0) {

				$user_email = $this->input->post('user_email');
				$data['useremail'] = $user_email;
				$randnum = rand();
				$this->user_model->generatecode($user_email, $randnum);
                
                $this->load->library('email');
                
                $this->email->from('support@qvoot.com', 'Support Team');
                $this->email->to($user_email, 'User');
                $this->email->subject('Qvoot Email Reset Password Code');
                $this->email->message('Here is your reset password code .'. $randnum);
                
                $this->email->send();
				// die;
				$this->load->view('templates/users/header.php');
				$this->load->view('templates/users/conformationcode.php', $data);
			} else {
				$this->session->set_flashdata('wrong_email', 'wrong_email');
				redirect('users/forgotpassword');
			}
		}
	}



	public function conform()
	{

		$this->form_validation->set_rules('conformationcode', 'user_key', 'required');
		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('field_missing', 'field_missing');
			redirect('users/forgotpassword');
		} else {
			$conformationcode = $this->input->post('conformationcode');
			$user_email = $this->input->post('user_email');
			$userdetail  = $this->user_model->userdetail($conformationcode, $user_email);
// 			echo $conformationcode;
// 			echo '<br>';
// 			echo $user_email;
			
// 			print_r($userdetail);
			
// 			die;
			if ($userdetail) {
				$userid = $userdetail['user_id'];
				$data['userid'] = $userdetail['user_id'];
				
		
				
				$data['userdetail'] = $this->user_model->get_user($userid);
				
				// print_r($data['userdetail']);
		
                
                // $email->send();
				$this->load->view('templates/users/header.php');
				$this->load->view('templates/users/newpassword.php', $data);
			} else {
				$this->session->set_flashdata('change_failed', ' You entered a wrong verification code.');
				redirect('users/conform?=dwww');
			}
		}
	}

	function change_password($userid)
	{

		$enc_password = password_hash($this->input->post('newpassword'), PASSWORD_DEFAULT);

		$data = array(
			'user_password' => $enc_password,
		);
		$this->security->xss_clean($data);
		$this->db->where('user_id', $userid);
		$this->db->update('users', $data);
		$this->session->set_flashdata('login_success', 'login_success');

// 		die;
		redirect('users/login');
	}


	function upload_profile_password($userid)
	{


		$userdetails = $this->user_model->get_profile($userid);

		$user_id = $this->user_model->login($userdetails['user_email'], $this->input->post('oldpassword'));
		$enc_password = password_hash($this->input->post('newpassword'), PASSWORD_DEFAULT);

		if ($user_id) {
			$data = array(
				'user_password' => $enc_password,
			);
			$this->security->xss_clean($data);
			$this->db->where('user_id', $userid);
			$this->db->update('users', $data);
			$this->session->set_flashdata('login_success', 'login_success');
		} else {
			$this->session->set_flashdata('login_failed', 'login_failed');
		}

		redirect('users/edit_profile');
	}


	public function register()
	{
		if ($this->session->userdata('qvoot_user_id')) {
			redirect('users/index');
		}
		$data['countries'] = $this->user_model->get_countries();
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/users/register.php', $data);
	}
	public function registeration()
	{
		$this->form_validation->set_rules('first_name', 'first_name', 'required');
		$this->form_validation->set_rules('last_name', 'last_name', 'required');
		$this->form_validation->set_rules('user_email', 'user_email', 'required');
		$this->form_validation->set_rules('user_password', 'user_password', 'required');
		// $this->form_validation->set_rules('user_gender', 'user_gender', 'required');
		// $this->form_validation->set_rules('user_country_id', 'user_country_id', 'required');
		// $this->form_validation->set_rules('user_state_id', 'user_state_id', 'required');
		// $this->form_validation->set_rules('user_city_id', 'user_city_id', 'required');
		// $this->form_validation->set_rules('user_street_num_name', 'user_street_num_name', 'required');
		// $this->form_validation->set_rules('user_postal_code', 'user_postal_code', 'required');
		// $this->form_validation->set_rules('user_address', 'user_address', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('field_missing', 'field_missing');
			redirect('users/register');
		} else {
			$this->form_validation->set_rules('user_email', 'user_email', 'required|valid_email|is_unique[users.user_email]');
			if ($this->form_validation->run() === FALSE) {
				$this->session->set_flashdata('email_false', 'email_false');
				redirect('users/register');
			}
			$enc_password = password_hash($this->input->post('user_password'), PASSWORD_DEFAULT);
			$this->user_model->registeration($enc_password);
			$this->session->set_flashdata('registered', 'registered');
			redirect('users/login');
		}
	}
	public function index()
	{
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_user($user_id);
		$data['courses'] = $this->user_model->get_recent_courses();
		$data['courses_pictures'] = $this->user_model->get_pictures();
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/index.php', $data);
		$this->load->view('templates/users/footer.php');
	}
	public function aboutus()
	{
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_user($user_id);
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/aboutus.php', $data);
		$this->load->view('templates/users/footer.php');
	}
	public function faq()
	{
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_user($user_id);
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/faq.php', $data);
		$this->load->view('templates/users/footer.php');
	}
	public function contactus()
	{
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_user($user_id);
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/contactus.php', $data);
		$this->load->view('templates/users/footer.php');
	}
	public function contact()
	{

		if ($this->session->userdata('study_user_id')) {
			redirect('users/login');
		}
		$this->form_validation->set_rules('contact_us_name', 'contact_us_name', 'required');
		// $this->form_validation->set_rules('contact_us_email', 'contact_us_email', 'required');
		// $this->form_validation->set_rules('contact_us_phone', 'contact_us_phone', 'required');
		// $this->form_validation->set_rules('contact_us_subject', 'contact_us_subject', 'required');
		// $this->form_validation->set_rules('contact_us_massage', 'contact_us_massage', 'required');
		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('field_missing', 'field_missing');
			redirect('users/contactus?fail');
		} else {
			$this->admin_model->contact();
			$this->session->set_flashdata('Thank you for submiting your inquiry, our team will get to you soon. ', 'thank_you_contacting');

			redirect('users/contactus?success=1');
		}
	}
	public function blog()
	{
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_user($user_id);
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/blog.php', $data);
		$this->load->view('templates/users/footer.php');
	}
	public function dashboard()
	{
		if (!$this->session->userdata('qvoot_user_id')) {
			redirect('users/login');
		}
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_user($user_id);
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/dashboard/navbar.php', $data);
		$this->load->view('templates/dashboard/aside.php');
		$this->load->view('templates/dashboard/index.php', $data);
		$this->load->view('templates/dashboard/footer.php');
	}
	public function addcourses()
	{
		if (!$this->session->userdata('qvoot_user_id')) {
			redirect('users/login');
		}

		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$data['languages'] = $this->user_model->get_languages();
		$data['countries'] = $this->user_model->get_countries();

		$data['user'] = $this->user_model->get_profile($user_id);
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/dashboard/navbar.php', $data);
		$this->load->view('templates/dashboard/aside.php');
		$this->load->view('templates/dashboard/addcourses.php', $data);
		$this->load->view('templates/dashboard/footer.php');
	}






	public function submitcourse()
	{
		if (!$this->session->userdata('qvoot_user_id')) {
			redirect('users/login');
		}

		$this->form_validation->set_rules('course_name', 'course_name', 'required');
		$this->form_validation->set_rules('course_full_desc', 'course_full_desc', 'required');
		$this->form_validation->set_rules('course_category', 'course_category', 'required');
		$this->form_validation->set_rules('course_price', 'course_price', 'required');
		$this->form_validation->set_rules('course_type', 'course_type', 'required');
		$this->form_validation->set_rules('course_total_students', 'course_total_students', 'required');
		$this->form_validation->set_rules('course_instructor_name', 'course_instructor_name', 'required');
		$this->form_validation->set_rules('course_language', 'course_language', 'required');
		$this->form_validation->set_rules('course_level', 'course_level', 'required');
		$this->form_validation->set_rules('course_certificate', 'course_certificate', 'required');
		$this->form_validation->set_rules('course_country_id', 'course_country_id', 'required');
		$this->form_validation->set_rules('course_city_name', 'course_city_name', 'required');
		$this->form_validation->set_rules('course_start_date', 'course_start_date', 'required');
		$this->form_validation->set_rules('course_subcat_id', 'course_subcat_id', 'required');
	
		if ($this->form_validation->run() === FALSE) {
			redirect('users/addcourses=?dsd');
		} else {
			$user_id = $this->session->userdata('qvoot_user_id');
			$data['user'] = $this->user_model->get_profile($user_id);
			$cources = $data['user']['user_add_courses_count'];
			$addone = $cources + '1';
			$courseid = $this->user_model->add_course();
			$number = count($_POST["course_requirement"]);
			if ($number >= 1) {
				for ($i = 0; $i < $number; $i++) {
					if (trim($_POST["course_requirement"][$i] != '')) {
						$this->add_course_requirement($courseid, $_POST["course_requirement"][$i]);
					}
				}
			}
			$number = count($_POST["course_for"]);
			if ($number >= 1) {
				for ($i = 0; $i < $number; $i++) {
					if (trim($_POST["course_for"][$i] != '')) {
						$this->add_course_for($courseid, $_POST["course_for"][$i]);
					}
				}
			}
			$this->user_model->add_user_course($addone, $user_id);
			$data1 = [];
			$count = count($_FILES['files']['name']);
			for ($i = 0; $i < $count; $i++) {
				if (!empty($_FILES['files']['name'][$i])) {
					$_FILES['file']['name'] = $_FILES['files']['name'][$i];
					$_FILES['file']['type'] = $_FILES['files']['type'][$i];
					$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
					$_FILES['file']['error'] = $_FILES['files']['error'][$i];
					$_FILES['file']['size'] = $_FILES['files']['size'][$i];
					$config['upload_path'] = 'assets/uploads/';
					$config['allowed_types'] = 'jpg|jpeg|png|gif';
					$config['max_size'] = '5000';
					$config['file_name'] = $_FILES['files']['name'][$i];
					$this->load->library('upload', $config);
					if ($this->upload->do_upload('file')) {
						$uploadData = $this->upload->data();
						$filename = $uploadData['file_name'];
						$data1['totalFiles'][] = $filename;
					}
				}
			}
			foreach ($data1['totalFiles'] as $imagename) {
				$this->images_uploading($courseid, $imagename);
			}
			$this->session->set_flashdata('submit', 'submit');
			redirect('users/addcourses');
		}
	}
	public function add_course_requirement($courseid, $course_requirement)
	{
		$data = array(
			'courseid_requirement' => $courseid,
			'course_requirements' => $course_requirement,
		);

		$this->security->xss_clean($data);
		$this->db->insert('course_requirement', $data);
	}
	public function add_course_for($courseid, $course_for)
	{
		$data = array(
			'courseid_course_for' => $courseid,
			'course_for' => $course_for,
		);

		$this->security->xss_clean($data);
		$this->db->insert('course_for', $data);
	}
	public function courseslist()
	{
		if (!$this->session->userdata('qvoot_user_id')) {
			redirect('users/login');
		}
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$data['languages'] = $this->user_model->get_languages();
		$data['courses'] = $this->user_model->get_courses();
		$data['courses_pictures'] = $this->user_model->get_pictures();
		$data['user'] = $this->user_model->get_profile($user_id);
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/dashboard/navbar.php', $data);
		$this->load->view('templates/dashboard/aside.php');
		$this->load->view('templates/dashboard/courseslist.php', $data);
		$this->load->view('templates/dashboard/footer.php');
	}
	public function mycourses()
	{
		if (!$this->session->userdata('qvoot_user_id')) {
			redirect('users/login');
		}
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$data['user'] = $this->user_model->get_profile($user_id);
		$data['courses'] = $this->user_model->get_user_courses($user_id);
		$data['courses_pictures'] = $this->user_model->get_pictures();
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/dashboard/navbar.php', $data);
		$this->load->view('templates/dashboard/aside.php');
		$this->load->view('templates/dashboard/courseslist.php', $data);
		$this->load->view('templates/dashboard/footer.php');
	}

	public function viewcourses($courseid)
	{
		if (!$this->session->userdata('qvoot_user_id')) {
			redirect('users/login');
		}
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$data['course'] = $this->user_model->get_course($courseid);
		$data['courses_pictures'] = $this->user_model->get_pictures($courseid);
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_user($user_id);
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/dashboard/navbar.php', $data);
		$this->load->view('templates/dashboard/aside.php');
		$this->load->view('templates/dashboard/viewcourse.php', $data);
		$this->load->view('templates/dashboard/footer.php');
	}

	public function coursebycat($cat_id)
	{
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_user($user_id);
		$data['courses'] = $this->user_model->get_coursesbycat($cat_id);
		$data['courses_pictures'] = $this->user_model->get_pictures();

		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/viewcourse.php', $data);
		$this->load->view('templates/users/footer.php');
	}
	public function viewcourse()
	{
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_user($user_id);
		$data['courses'] = $this->user_model->get_courses();
		$data['courses_pictures'] = $this->user_model->get_pictures();

		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/viewcourse.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function search_results()
	{
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_user($user_id);
		$data['courses'] = $this->user_model->get_search_courses();
		$data['courses_pictures'] = $this->user_model->get_pictures();

		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/viewcourse.php', $data);
		$this->load->view('templates/users/footer.php');
	}
	public function searchdetail()
	{
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_user($user_id);
		$data['courses'] = $this->user_model->get_searchdetail_courses();
		$data['courses_pictures'] = $this->user_model->get_pictures();

		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/viewcourse.php', $data);
		$this->load->view('templates/users/footer.php');
	}
	public function coursedetail($courseid)
	{
		$data['course'] = $this->user_model->get_course($courseid);
		if (isset($data['course'])) {

			$data['categories'] = $this->user_model->get_categories();
			$data['subcategories'] = $this->user_model->get_subcategories();

			$data['courses_pictures'] = $this->user_model->get_course_pictures($courseid);
			$data['course_fors'] = $this->user_model->get_course_fors($courseid);
			$data['course_requirements'] = $this->user_model->get_course_requirements($courseid);
			$user_id =	$this->session->userdata('qvoot_user_id');
			$data['user'] = $this->user_model->get_user($user_id);
			$this->load->view('templates/users/header.php', $data);
			$this->load->view('templates/users/navbar.php');
			$this->load->view('templates/users/coursedetail.php', $data);
			$this->load->view('templates/users/footer.php');
		} else {
			redirect('users/error404');
		}
	}
	public function howitworks()
	{
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/howitworks.php', $data);
		$this->load->view('templates/users/footer.php');
	}
	public function cart()
	{
		if (!$this->session->userdata('qvoot_user_id')) {
			redirect('users/login');
		}
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_profile($user_id);
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/dashboard/navbar.php', $data);
		$this->load->view('templates/dashboard/aside.php');
		$this->load->view('templates/dashboard/cart.php', $data);
		$this->load->view('templates/dashboard/footer.php');
	}

	public function profile()
	{
		if (!$this->session->userdata('qvoot_user_id')) {
			redirect('users/login');
		}
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_profile($user_id);
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/dashboard/navbar.php', $data);
		$this->load->view('templates/dashboard/aside.php');
		$this->load->view('templates/dashboard/profile.php', $data);
		$this->load->view('templates/dashboard/footer.php');
	}


	public function edit_profile()
	{
		if (!$this->session->userdata('qvoot_user_id')) {
			redirect('users/login');
		}
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_profile($user_id);
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/dashboard/navbar.php', $data);
		$this->load->view('templates/dashboard/aside.php');
		$this->load->view('templates/dashboard/edit_profile.php', $data);
		$this->load->view('templates/dashboard/footer.php');
	}


	public function purchasehistory()
	{
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_profile($user_id);
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/dashboard/navbar.php', $data);
		$this->load->view('templates/dashboard/aside.php');
		$this->load->view('templates/dashboard/purchasehistory.php', $data);
		$this->load->view('templates/dashboard/footer.php');
	}
	public function endrolledcourses()
	{
		if (!$this->session->userdata('qvoot_user_id')) {
			redirect('users/login');
		}
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_profile($user_id);
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/dashboard/navbar.php', $data);
		$this->load->view('templates/dashboard/aside.php');
		$this->load->view('templates/dashboard/endrolledcourses.php', $data);
		$this->load->view('templates/dashboard/footer.php');
	}
	public function editprofile()
	{
		if (!$this->session->userdata('qvoot_user_id')) {
			redirect('users/login');
		}
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_profile($user_id);
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/dashboard/navbar.php', $data);
		$this->load->view('templates/dashboard/aside.php');
		$this->load->view('templates/dashboard/editprofile.php', $data);
		$this->load->view('templates/dashboard/footer.php');
	}
	function do_upload($pagelink)
	{
		$config = array(
			'upload_path' => "assets/uploads/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "5000",
			'max_width' => "5000"
		);
		$this->load->library('upload', $config);
		if ($this->upload->do_upload('userfile')) {
			$imgdata = array('upload_data' => $this->upload->data());
			$imgname = $imgdata['upload_data']['file_name'];
		} else {
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
			redirect('users/' . $pagelink);
		}
		return $imgname;
	}



	function do_upload_file($imageno)
	{
		$config = array(
			'upload_path' => "assets/uploads/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "6000",
			'max_width' => "6000"
		);
		$this->load->library('upload', $config);
		if ($this->upload->do_upload($imageno)) {
			$imgdata = array('upload_data' => $this->upload->data());
			$imgname = $imgdata['upload_data']['file_name'];
		} else {
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
		}
		return $imgname;
	}


	public function upload_profile_picture($userid)
	{
		// print_r($_FILES);

		// print "</pre>";

		// echo $imagename;
		$imagename = $this->do_upload_file('user_file');

		$data = array(
			'user_image' => $imagename
		);
		$this->security->xss_clean($data);
		$this->db->where('user_id', $userid);
		$this->db->update('users', $data);

		// die;
		redirect('users/edit_profile');
	}

	function update_profile($userid)
	{
		$data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'user_email' => $this->input->post('user_email'),
			'user_phone' => $this->input->post('user_phone'),
			'user_bio' => $this->input->post('user_bio')
		);
		$this->security->xss_clean($data);
		$this->db->where('user_id', $userid);
		$this->db->update('users', $data);

		redirect('users/edit_profile');
	}







	function do_upload1()
	{
		$config = array(
			'upload_path' => "assets/uploads/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "5000",
			'max_width' => "5000"
		);
		$this->load->library('upload', $config);
		if ($this->upload->do_upload('user_file')) {
			$imgdata = array('upload_data' => $this->upload->data());
			$imgname = $imgdata['upload_data']['file_name'];
		} else {
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
		}
		return $imgname;
	}



	public function images_uploading($courseid, $imagename)
	{
		$data = array(
			'course_id' => $courseid,
			'course_picture' => $imagename
		);
		$this->security->xss_clean($data);
		$this->db->insert('course_pictures', $data);
	}
	public function earnings()
	{
		if (!$this->session->userdata('qvoot_user_id')) {
			redirect('users/login');
		}
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_profile($user_id);
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/dashboard/navbar.php', $data);
		$this->load->view('templates/dashboard/aside.php');
		$this->load->view('templates/dashboard/earnings.php', $data);
		$this->load->view('templates/dashboard/footer.php');
	}
	public function withdraw()
	{
		if (!$this->session->userdata('qvoot_user_id')) {
			redirect('users/login');
		}
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_profile($user_id);
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/dashboard/navbar.php', $data);
		$this->load->view('templates/dashboard/aside.php');
		$this->load->view('templates/dashboard/withdraw.php', $data);
		$this->load->view('templates/dashboard/footer.php');
	}
	public function bookmarks()
	{
		if (!$this->session->userdata('qvoot_user_id')) {
			redirect('users/login');
		}
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_profile($user_id);
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/dashboard/navbar.php', $data);
		$this->load->view('templates/dashboard/aside.php');
		$this->load->view('templates/dashboard/bookmarks.php', $data);
		$this->load->view('templates/dashboard/footer.php');
	}
	public function reviews()
	{
		if (!$this->session->userdata('qvoot_user_id')) {
			redirect('users/login');
		}
		$data['categories'] = $this->user_model->get_categories();
		$data['subcategories'] = $this->user_model->get_subcategories();
		$user_id =	$this->session->userdata('qvoot_user_id');
		$data['user'] = $this->user_model->get_profile($user_id);
		$this->load->view('templates/users/header.php', $data);
		$this->load->view('templates/dashboard/navbar.php', $data);
		$this->load->view('templates/dashboard/aside.php');
		$this->load->view('templates/dashboard/reviews.php', $data);
		$this->load->view('templates/dashboard/footer.php');
	}
	public function error404()
	{
		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/404.php');
	}
	public function profileimage()
	{
		if (!$this->session->userdata('qvoot_user_id')) {
			redirect('users/login');
		}
		$imgname = $this->do_upload('profileimage');
		$this->user_model->profileimage($imgname);
		redirect('users/profile');
	}

	public function get_states_by_countryid($country_id)
	{
		$data['states'] = $this->admin_model->get_states_by_countryid($country_id);

		$this->load->view('templates/ajax/getstates.php', $data);
	}

	public function get_subcat($courseid)
	{
		$data['subcategories'] = $this->admin_model->get_subcatbyid($courseid);

		$this->load->view('templates/ajax/subcat.php', $data);
	}


	public function get_cities_by_stateid($state_id)
	{
		$data['cities'] = $this->admin_model->get_cities_by_stateid($state_id);

		$this->load->view('templates/ajax/getcities.php', $data);
	}
}

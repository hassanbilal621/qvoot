<?php
class admin extends CI_Controller
{
	///////////////////////////// function page start/////////////////////////////
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->library('session');
	}

	function do_upload_file($imageno)
	{
		$config = array(
			'upload_path' => "assets/upload/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "5000",
			'max_width' => "5000"
		);

		$this->load->library('upload', $config);
		if ($this->upload->do_upload($imageno)) {
			$imgdata = array('upload_data' => $this->upload->data());
			$imgname = $imgdata['upload_data']['file_name'];
		} else {
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
		}
		return $imgname;
	}

	public function index()
	{
		if (!$this->session->admindata('qvoot_admin_id')) {
			redirect('admin/login');
		}
		$data = 'd';
		$this->load->view('templates/admin/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/index.php', $data);
		$this->load->view('templates/admin/footer.php');
	}

	public function login()
	{
		if ($this->session->admindata('qvoot_admin_id')) {
			redirect('admin/index');
		}
		$this->load->view('templates/admin/header.php');
		$this->load->view('templates/admin/login.php');
	}

	public function login_admin()
	{
		if ($this->session->admindata('qvoot_admin_id')) {
			redirect('admin/index');
		}

		$this->form_validation->set_rules('admin_email', 'admin_email', 'required');
		$this->form_validation->set_rules('admin_password', 'admin_password', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('field_missing', 'field_missing');
			redirect('admin/login');
		} else {
			$email = $this->input->post('admin_email');
			$password = $this->input->post('admin_password');

			$admin_id = $this->user_model->login($email, $password);
			if ($admin_id) {
				$user_data = array(
					'qvoot_admin_id' => $admin_id
				);
				$this->session->set_admindata($user_data);
				$this->session->set_flashdata('Success', 'Success');
				redirect('admin/index');
			} else {
				$this->session->set_flashdata('login_failed', 'Credentials  is invalid. Incorrect username or password.');
				redirect('admin/login');
			}
		}
	}
	public function logout()
	{
		$this->session->unset_admindata('qvoot_admin_id');
		// $this->session->unset_admindata('user_email');
		// delete_cookie('qvoot_admin_id');
		$this->session->set_flashdata('user_loggedout', 'user_loggedout');
		redirect('admin/login');
	}
	public function admins()
	{
		if (!$this->session->admindata('qvoot_admin_id')) {
			redirect('admin/login');
		}
		$data['admins'] = $this->admin_model->get_admins();
		$this->load->view('templates/admin/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/manageadmin.php', $data);
		$this->load->view('templates/admin/footer.php');
	}
	public function contactus()
	{
		if (!$this->session->admindata('qvoot_admin_id')) {
			redirect('admin/login');
		}
		$data['contacts'] = $this->admin_model->get_contacts();
		$this->load->view('templates/admin/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/contactus.php', $data);
		$this->load->view('templates/admin/footer.php');
	}
	public function resolve($contactid)
	{
		if (!$this->session->admindata('qvoot_admin_id')) {
			redirect('admin/login');
		}
		$this->admin_model->resolve($contactid);
		redirect('admin/contactus');
	}

	public function users()
	{
		if (!$this->session->admindata('qvoot_admin_id')) {
			redirect('admin/login');
		}
		$data['users'] = $this->admin_model->get_users();
		$this->load->view('templates/admin/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/manageuser.php', $data);
		$this->load->view('templates/admin/footer.php');
	}
	public function courses()
	{
		if (!$this->session->admindata('qvoot_admin_id')) {
			redirect('admin/login');
		}
		$data['courses'] = $this->user_model->get_all_courses();
		$this->load->view('templates/admin/header.php');
		$this->load->view('templates/admin/navbar.php');
		$this->load->view('templates/admin/aside.php');
		$this->load->view('templates/admin/managecourses.php', $data);
		$this->load->view('templates/admin/footer.php');
	}
	public function view_admin($adminid)
	{
		if (!$this->session->admindata('qvoot_admin_id')) {
			redirect('admin/login');
		}
		$data['admin'] = $this->admin_model->get_admin($adminid);


		if (isset($data['admin'])) {
			$this->load->view('templates/admin/header.php');
			$this->load->view('templates/admin/navbar.php');
			$this->load->view('templates/admin/aside.php');
			$this->load->view('templates/admin/viewadmin.php', $data);
			$this->load->view('templates/admin/footer.php');
		} else {
			redirect('admin/error404');
		}
	}
	public function view_user($userid)
	{
		if (!$this->session->userdata('qvoot_admin_id')) {
			redirect('admin/login');
		}
		$data['user'] = $this->user_model->get_profile($userid);
		$data['courses'] = $this->user_model->get_user_courses($userid);


		if (isset($data['user'])) {
			$this->load->view('templates/admin/header.php');
			$this->load->view('templates/admin/navbar.php');
			$this->load->view('templates/admin/aside.php');
			$this->load->view('templates/admin/viewuser.php', $data);
			$this->load->view('templates/admin/footer.php');
		} else {
			redirect('admin/error404');
		}
	}

	public function view_course($courseid)
	{
		if (!$this->session->userdata('qvoot_admin_id')) {
			redirect('admin/login');
		}
		$data['course'] = $this->user_model->get_course($courseid);
		$data['courses_pictures'] = $this->user_model->get_course_pictures($courseid);
		// $data['courses_pictures'] = $this->user_model->get_course_pictures($courseid);
		$data['course_fors'] = $this->user_model->get_course_fors($courseid);
		$data['course_requirements'] = $this->user_model->get_course_requirements($courseid);

		if (isset($data['course'])) {
			$this->load->view('templates/admin/header.php');
			$this->load->view('templates/admin/navbar.php');
			$this->load->view('templates/admin/aside.php');
			$this->load->view('templates/admin/viewcourse.php', $data);
			$this->load->view('templates/admin/footer.php');
		} else {
			redirect('admin/error404');
		}
	}
	public function enablecourse($course_id)
	{
		$this->admin_model->enablecourse($course_id);
		redirect('admin/courses');
	}
	public function disablecourse($course_id)
	{
		$this->admin_model->disablecourse($course_id);
		redirect('admin/courses');
	}
}
